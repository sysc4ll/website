---
title: "Benjamin Bayart : Qu'est ce qu'internet ?"
---

{{< newtabref href="https://fr.wikipedia.org/wiki/Benjamin_Bayart" title="Benjamin Bayart" >}} nous explique dans des conférences ce qu'est internet :

{{< newtabref href="https://www.youtube.com/watch?v=pwT2egqlke4" title="Benjamin Bayart : Qu'est ce qu'internet - Partie 1 : Le réseau" >}} \
{{< newtabref href="https://www.youtube.com/watch?v=-O65Eb2D2l0" title="Benjamin Bayart : Qu'est ce qu'internet - Partie 2 : Les applications" >}} \
{{< newtabref href="https://www.youtube.com/watch?v=ctRv5dnJOcY" title="Benjamin Bayart : Qu'est ce qu'internet - Partie 3 : Les enjeux politiques et sociétaux" >}}

