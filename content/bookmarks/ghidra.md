---
title: "Ghidra"
---

"A software reverse engineering (SRE) suite of tools developed by NSA's Research Directorate in support of the Cybersecurity mission" :
{{< newtabref href="https://ghidra-sre.org/" title="https://ghidra-sre.org" >}}

