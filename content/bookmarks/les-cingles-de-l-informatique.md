---
title: "Les cingles de l'informatique"
---

Ce documentaire raconte le développement de l'ordinateur personnel aux États-Unis depuis la Seconde Guerre mondiale jusqu'en 1995 :

{{< newtabref href="https://www.youtube.com/watch?v=dOakyAhqiVY" title="Les cingles de l'informatique - 1ere Partie - L'amateurisme des debuts" >}} \
{{< newtabref href="https://www.youtube.com/watch?v=zywPwbQqshY" title="Les cingles de l'informatique - 2eme Partie - L'entree en scene d'IBM" >}} \
{{< newtabref href="https://www.youtube.com/watch?v=ySM9m9o4pa8" title="Les cingles de l'informatique - 3eme Partie - L'informatique pour tous" >}}

