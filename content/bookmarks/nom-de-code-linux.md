---
title: "Nom de code Linux"
---

Film documentaire de Hannu Puttonen datant de 2002 qui retrace l'histoire des mouvements GNU, Linux, open source et des logiciels libres
et dans lequel plusieurs personnalités de l'informatique sont interviewées, comme Linus Torvalds, Alan Cox, Richard Stallman, Theodore Ts'o ou Eric S. Raymond :
{{< newtabref href="https://www.youtube.com/watch?v=79_IMeks4wY" title="Nom de code Linux" >}}

