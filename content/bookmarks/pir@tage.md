---
title: "pir@tage"
---

"Le documentaire Pirat@ge retrace l'histoire d'Internet gràce aux témoignages de ceux qui l'ont construit, les hackers.
Il se place au coeur des préoccupations de cette génération Y, dont il analyse les modes de communication en réseau, de consommation de biens culturels et de leur partage." :
{{< newtabref href="https://www.youtube.com/watch?v=z_xj8C3lux4" title="pir@tage" >}}

