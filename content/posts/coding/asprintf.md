---
title: "Allouer une string avec des spécificateurs de format"
slug: "asprintf"
description: "Article sur comment allouer une string avec des spécificateurs de format."
tags: ["c"]
categories: ["coding"]
layout: "post-without-toc"
---

# Allouer une string avec des spécificateurs de format

Je met ici pour mémo une façon de créer une fonction du genre :

```c {linenos=false}
char *s = strdup("Hello... Je suis %s.", name);
```

Voici la façon d'y parvenir :

```c
int size;
char *buffer;
 
size = snprintf(NULL, 0, "Hello... Je suis %s.", "julien");
buffer = malloc((size + 1) * sizeof(*buffer));
sprintf(buffer, "Hello... Je suis %s.", "julien");
 
puts(buffer);
 
free(buffer);
```

Il est tout à fait possible d'en faire une fonction.
D'ailleurs cette fonction existe déjà sur certains système d'exploitation.
Sur GNU/Linux cette fonction se nomme **asprintf**.

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
 
int vasprintf(char **buffer, const char *format, va_list args)
{
    int len = 0;
    va_list wargs;
 
    va_copy(wargs, args);
 
    len = vsnprintf(NULL, len, format, wargs);
 
    va_end(wargs);
 
    if (len < 0) return -1;
 
    // allocation avec count + 1 pour '\0'
    *buffer = (char *)malloc(len + 1);
 
    if (*buffer == NULL) return -1;
 
    len = vsprintf(*buffer, format, args);
 
    return len;
}
 
int asprintf(char **buffer, const char *format, ...)
{
    va_list args;
    int len;
 
    va_start(args, format);
    len = vasprintf(buffer, format, args);
    va_end(args);
 
    return len;
}
```

Exemple d'utilisation :

```c
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
 
int vasprintf(char **buffer, const char *format, va_list args)
{
    int len = 0;
    va_list wargs;
 
    va_copy(wargs, args);
 
    len = vsnprintf(NULL, len, format, wargs);
 
    va_end(wargs);
 
    if (len < 0) return -1;
 
    // allocation avec count + 1 pour '\0'
    *buffer = (char *)malloc(len + 1);
 
    if (*buffer == NULL) return -1;
 
    len = vsprintf(*buffer, format, args);
 
    return len;
}
 
int asprintf(char **buffer, const char *format, ...)
{
    va_list args;
    int len;
 
    va_start(args, format);
    len = vasprintf(buffer, format, args);
    va_end(args);
 
    return len;
}

int main(void)
{
    int ret;
    char *buffer;

    ret = asprintf(&buffer, "Hello... Je suis %s.", "julien");
 
    if (ret != -1) puts(buffer);
 
    free(buffer);
 
    return 0;
}
```

Ce qui donne :

```sh
$ ./a.out 
Hello... Je suis julien.
```
