---
title: "bitwise operation en C"
slug: "c-bitwise-operation"
description: "Éxemple d'opération bit à bit"
tags: ["c"]
categories: ["coding"]
layout: "post-without-toc"
---

# Éxemple d'opération bit à bit

```c
#include <stdio.h>
#include <stdint.h>
#include <err.h>
#include <stdlib.h>

/*
 *   7   6     5   4   3     2   1   0
 * +---|---+ +---|---|---+ +---|---|---+
 * |  MOD  | |    REG    | |    R/M    |
 * +---|---+ +---|---|---+ +---|---|---+
 */

/*
 * 0x10 = 00.010.000 (mod=0, reg=2, r/m=0)
 * 0x11 = 00.010.001 (mod=0, reg=2, r/m=1)
 */

/*
 * byte = 0x10
 *
 * modrm_byte rm = byte & 7
 *
 * 0x10 = 00.010.000
 *   7  = 00.000.111
 *
 *   00.010.000
 * & 00.000.111
 * = 00.000.000
 *
 * modrm_byte reg = (byte >> 3) & 7;
 *
 * 0x10 = 00.010.000 >> 3 = 00.000.010
 * 
 *   00.000.010
 * & 00.000.111
 * = 00.000.010
 *
 * modrm_byte mod = (byte >> 6) & 3;
 *
 * 0x10 = 00.010.000 >> 6 = 00.000.000
 *
 *   00.000.000
 * & 00.000.011
 * = 00.000.000
 */

/*
 * byte = 0x11
 *
 * modrm_byte rm = byte & 7
 *
 * 0x11 = 00.010.001
 *   7  = 00.000.111
 *
 *   00.010.001
 * & 00.000.111
 * = 00.000.001
 * = 1
 *
 * modrm_byte reg = (byte >> 3) & 7;
 *
 * 0x11 = 00.010.001 >> 3 = 00.000.010
 * 
 *   00.000.010
 * & 00.000.111
 * = 00.000.010
 * = 2
 *
 * modrm_byte mod = (byte >> 6) & 3;
 *
 * 0x11 = 00.010.001 >> 6 = 00.000.000
 *
 *   00.000.000
 * & 00.000.011
 * = 00.000.000
 * = 0
 */

int main(int argc, char *argv[])
{
    unsigned byte;
    uint8_t modrm_byte;

    if (argc != 2)
        errx(EXIT_FAILURE, "usage: %s <hex digit>\nusage: %s 0x10", argv[0], argv[0]);

    if (sscanf(argv[1], "0x%2x", &byte) != 1)
        errx(EXIT_FAILURE, "sscanf(%s)\n", argv[1]);

    modrm_byte = byte;

    printf("modrm_byte_t rm = 0x%.2x\n",  modrm_byte & 7);
    printf("modrm_byte_t reg = 0x%.2x\n", (modrm_byte >> 3) & 7);
    printf("modrm_byte_t mod = 0x%.2x\n", (modrm_byte >> 6) & 3);

    return 0;
}
```

