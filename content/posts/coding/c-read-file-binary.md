---
title: "read binary file en C"
slug: "c-read-binary-file"
description: "Éxemple de lecture d'un fichier binaire"
tags: ["c"]
categories: ["coding"]
layout: "post-without-toc"
---

# Éxemple de lecture d'un fichier binaire

```c
#include <stdio.h>
#include <stdint.h>
#include <err.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

typedef union modrm_byte_t modrm_byte_t;
union modrm_byte_t
{
    uint8_t byte;

    struct
    {
        uint8_t rm:3;
        uint8_t reg:3;
        uint8_t mod:2;
    };
};

typedef union sib_byte_t sib_byte_t;
union sib_byte_t
{
    uint8_t byte;

    struct
    {
        uint8_t base:3;
        uint8_t index:3;
        uint8_t ss:2;
    };
};

typedef union mdisass_buffer_t mdisass_buffer_t;
union mdisass_buffer_t
{
    union
    {
        uint8_t byte;
        uint8_t prefix;
        uint8_t opcode;
    };

    modrm_byte_t modrm_byte;

    sib_byte_t sib_byte;
};

typedef struct mdisass_data_t mdisass_data_t;
struct mdisass_data_t
{
    size_t size;
    mdisass_buffer_t *buffer;
};

void set_buffer_from_file(mdisass_data_t *data, char *filename);


int main(int argc, char *argv[])
{
    mdisass_data_t *data = NULL;

    if (argc != 2)
        errx(EXIT_FAILURE, "usage: %s ./file.bin", argv[0]);

    if ((data = calloc(sizeof(mdisass_data_t), 1)) == NULL)
    {
        errx(EXIT_FAILURE, "calloc(sizeof(mdisass_data_t), 1");
    }

    set_buffer_from_file(data, argv[1]);

    printf("\n*** START DEBUG ***\n");
    printf("in main()\n");
    printf("data->size: %lu\n", data->size);
    for (size_t i = 0; i < data->size; i++)
        printf("mdisass_buffer[%lu].byte = 0x%.2x ", i, data->buffer[i].byte);
    printf("\n*** END DEBUG ***\n");

    printf("\n*** START DEBUG ***\n");
    data->buffer[0].byte = 0x10;
    printf("mdisass_buffer[0].modrm_byte.byte = 0x%.2x\n", data->buffer[0].modrm_byte.byte);
    printf("mdisass_buffer[0].modrm_byte.rm = 0x%.2x\n", data->buffer[0].modrm_byte.rm);
    printf("mdisass_buffer[0].modrm_byte.reg = 0x%.2x\n", data->buffer[0].modrm_byte.reg);
    printf("mdisass_buffer[0].modrm_byte.mod = 0x%.2x\n", data->buffer[0].modrm_byte.mod);
    printf("*** END DEBUG ***\n");

    free(data->buffer);

    return 0;
}

void set_buffer_from_file(mdisass_data_t *data, char *filename)
{
    const size_t CHUNK_SIZE = 128;
    FILE *f = NULL;
    size_t size = CHUNK_SIZE, read = 0, ret = 0;
    uint8_t *bytes = NULL;

    if ((f = fopen(filename, "rb")) == NULL)
    {
        fclose(f);
        errx(EXIT_FAILURE, "fopen(%s) : %s", filename, strerror(errno));
    }

    if ((bytes = malloc(sizeof(uint8_t) * size)) == NULL)
    {
        fclose(f);
        errx(EXIT_FAILURE, "malloc(sizeof(uint8_t) * %lu) : %s", size, strerror(errno));
    }

    while ((ret = fread(&bytes[read], sizeof(uint8_t), CHUNK_SIZE, f)) != 0)
    {
        read += ret;

        size += CHUNK_SIZE;

        bytes = realloc(bytes, sizeof(uint8_t) * size);

        if (bytes == NULL)
        {
            fclose(f);
            errx(EXIT_FAILURE, "realloc(sizeof(uint8_t) * %lu) : %s", size, strerror(errno));
        }
    }

    if (ferror(f) != 0)
    {
        fclose(f);
        errx(EXIT_FAILURE, "ferror(%s) : %s", filename, strerror(errno));
    }

    fclose(f);

    printf("*** START DEBUG ***\n");
    printf("in set_buffer_from_file()\n");
    printf("read: %lu\n", read);
    for (int i = 0; i < read; i++) printf("0x%.2x ", bytes[i]);
    printf("\n*** END DEBUG ***\n");

    data->size = read;
    data->buffer = (mdisass_buffer_t *)bytes;
}
```

```sh
echo -ne "\x01\x10" > file.bin
```

```sh
./c-read-file-binary ./file.bin
```

```txt
*** START DEBUG ***
in set_buffer_from_file()
read: 2
0x01 0x10 
*** END DEBUG ***

*** START DEBUG ***
in main()
data->size: 2
mdisass_buffer[0].byte = 0x01 mdisass_buffer[1].byte = 0x10 
*** END DEBUG ***

*** START DEBUG ***
mdisass_buffer[0].modrm_byte.byte = 0x10
mdisass_buffer[0].modrm_byte.rm = 0x00
mdisass_buffer[0].modrm_byte.reg = 0x02
mdisass_buffer[0].modrm_byte.mod = 0x00
*** END DEBUG ***
```

