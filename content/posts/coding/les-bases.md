---
title: "Les systèmes décimal, octal, hexadécimal, et binaire"
slug: "les-bases"
description: "Article sur les systèmes décimal, octal, hexadécimal, et binaire"
tags: [""]
categories: ["coding"]
layout: "post-with-toc"
---

# Les systèmes décimal, octal, hexadécimal, et binaire

Dans cet article, nous allons voir les systèmes de numérotations décimal, octal, hexadécimal et binaire.

Nous verrons aussi comment convertir.

## Les bases en arithmétique

On appel "base" le nombre servant à définir un système de numérotation.

Donc, pour le système décimal, c'est la base dix, alors que pour le système octal c'est la base huit, base seize pour le système hexadécimal et base deux pour le système binaire.

Toutes les bases numériques suivent la relation suivante :

<strong>
<p>i=n<p>
<p>&nbsp;∑ (b<sub>i</sub> a<sup>i</sup>) = b<sub>i</sub>a<sup>n</sup> + ... + b<sub>5</sub>a<sup>5</sup> + b<sub>4</sub>a<sup>4</sup> + b<sub>3</sub>a<sup>3</sup> + b<sub>2</sub>a<sup>2</sup> + b<sub>1</sub>a<sup>1</sup> + b<sub>0</sub>a<sup>0</sup></p>
<p>i=0</p>
</strong>

ou : b<sub>i</sub> : chiffre de la base de rang i.\
et : a<sup>i</sup> : puissance de la base a d'exposant de rang i.

Exemple en base 10 :

1983 = (1 * 10<sup>3</sup>) + (9 * 10<sup>2</sup>) + (8 * 10<sup>1</sup>) + (3 * 10<sup>0</sup>)

## La base dix : Le système décimal

La base dix utilise 10 symboles : **0, 1, 2, 3, 4, 5, 6, 7, 8, 9**

Les nombres écrits dans le système décimal vérifie la relation suivante :

5473<sub>10</sub> = (5 * 10<sup>3</sup>) + (4 * 10<sup>2</sup>) + (7 * 10<sup>1</sup>) + (3 * 10<sup>0</sup>)

Chaque chiffre du nombre est à multiplier par une puissance de 10.

L'exposant de cette puissance est nul pour le chiffre situé le plus à droite et s'accroît d'une unité pour chaque passage à un chiffre vers la gauche.

5473<sub>10</sub> = (5 * 10<sup>3</sup>) + (4 * 10<sup>2</sup>) + (7 * 10<sup>1</sup>) + (3 * 10<sup>0</sup>)

## La base deux : Le système binaire

La base deux utilise seulement deux symboles : **0 et 1**

Les nombres écrits dans le système binaire vérifie la relation suivante :

(10110)<sub>2</sub> = (1 * 2<sup>4</sup>) + (0 * 2<sup>3</sup>) + (1 * 2<sup>2</sup>) + (1 * 2<sup>1</sup>) + (0 × 2<sup>0</sup>)

Chaque chiffre du nombre est à multiplier par une puissance de 2.

Comme en base dix, l'exposant de cette puissance est nul pour le chiffre situé le plus à droite et s'accroît d'une unité pour chaque passage à un chiffre vers la gauche.

## La base seize : Le système hexadécimal

La base seize utilise seize symboles : **0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F**

Chaque chiffre du nombre est à multiplier par une puissance de 16.

Comme en base dix, l'exposant de cette puissance est nul pour le chiffre situé le plus à droite et s'accroît d'une unité pour chaque passage à un chiffre vers la gauche.

(5AF)<sub>16</sub> = (5 * 16<sup>2</sup>) + (A * 16<sup>1</sup>) + (F * 16<sup>0</sup>)

## La base huit : Le système octal

La base huit utilise huit symboles : **0, 1, 2, 3, 4, 5, 6, 7, 8**

Chaque chiffre du nombre est à multiplier par une puissance de 8.

Comme en base dix, l'exposant de cette puissance est nul pour le chiffre situé le plus à droite et s'accroît d'une unité pour chaque passage à un chiffre vers la gauche.

(745)<sub>8</sub> = (7 * 8<sup>2</sup>) + (4 * 8<sup>1</sup>) + (5 * 8<sup>0</sup>)

## Les convertions d'une base à décimal

Regardons maintenant comment convertir les nombres d'une base deux, huit, ou seize en décimal.

### Convertion binaire à décimal

(11010)<sub>2</sub>

(1 * 2<sup>4</sup>) + (1 * 2<sup>3</sup>) + (0 * 2<sup>2</sup>) + (1 * 2<sup>1</sup>) + (0 * 2<sup>0</sup>)

16 + 8 + 0 + 2 + 0

26

### Convertion octal à décimal

(1523)<sub>8</sub>

(1 * 8<sup>3</sup>) + (5 * 8<sup>2</sup>) + (2 * 8<sup>1</sup>) + (3 * 8<sup>0</sup>)

512 + 320 + 16 + 3

851

### Convertion hexadécimal à décimal

(B65F)<sub>16</sub>

(11 * 16<sup>3</sup>) + (6 * 16<sup>2</sup>) + (5 * 16<sup>1</sup>) + (15 * 16<sup>0</sup>)

45056 + 1536 + 80 + 15

46687

## Les convertions de décimal à une base

Regardons maintenant comment convertir les nombres d'une base dix à une base binaire, octal et hexadécimal.

### Convertion décimal à binaire

823<sub>10</sub>

823 / 2 = 411 Reste 1 (1)<sub>2</sub>

411 / 2 = 205 Reste 1 (11)<sub>2</sub>

205 / 2 = 102 Reste 1 (111)<sub>2</sub>

102 / 2 = 51 Reste 0 (0111)<sub>2</sub>

51 / 2 = 25 Reste 1 (10111)<sub>2</sub>

25 / 2 = 12 Reste 1 (110111)<sub>2</sub>

12 / 2 = 6 Reste 0 (0110111)<sub>2</sub>

6 / 2 = 3 Reste 0 (00110111)<sub>2</sub>

3 / 2 = 1 Reste 1 (100110111)<sub>2</sub>

1 / 2 = 0 Reste 1 (1100110111)<sub>2</sub>

1100110111<sub>2</sub>

### Convertion décimal à octal

823<sub>10</sub>

823 / 8 = 102 Reste 7 (7)<sub>8</sub>

102 / 8 = 12 Reste 6 (67)<sub>8</sub>

12 / 8 = 1 Reste 4 (467)<sub>8</sub>

1 / 8 = 0 Reste 1 (1467)<sub>8</sub>

1467<sub>8</sub>

### Convertion décimal à hexadécimal

823<sub>10</sub>

823 / 16 = 51 Reste 7 (7)<sub>16</sub>

51 / 16 = 3 Reste 3 (37)<sub>16</sub>

3 / 16 = 0 Reste 3 (337)<sub>16</sub>

337<sub>16</sub>

