---
title: "Calculer la taille d'une chaîne de cracatères sans utiliser string.h"
slug: "my-strlen"
description: "Comment calculer la taille d'une chaîne de cracatères sans utiliser string.h"
tags: ["c"]
categories: ["coding"]
layout: "post-without-toc"
---

# my-strlen

```c
#include <stdio.h>

/*
 * avec indice
 */
size_t my_strlen_indice(char *s)
{
    int i;

    for (i = 0; s[i] != '\0'; i++);

    return i;
}

/*
 * avec pointeur
 */
size_t my_strlen_ptr(char *s)
{
    char *ptr = s;
    int i;

    for (i = 0; *ptr != 0; i++, ptr++);

    return i;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <string>\n", argv[0]);
        return 1;
    }

    printf("my_strlen_indice : %ld\n", my_strlen_indice(argv[1]));

    printf("my_strlen_ptr : %ld\n", my_strlen_ptr(argv[1]));

    return 0;
}
```
