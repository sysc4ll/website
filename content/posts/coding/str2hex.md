---
title: "Transformer une chaîne de caractères contenant des bytes en un tableau de bytes"
slug: "str2hex"
description: "Comment transformer une chaîne de caractères contenant des bytes en un tableau de bytes"
tags: ["c"]
categories: ["coding"]
layout: "post-without-toc"
---

# Comment transformer une chaîne de caractères contenant des bytes en un tableau de bytes

Voici un code en langage C servant à convertir une chaîne de caractères représentant des bytes en un array de ces bytes :

```c
unsigned char *
str2hex(char *str)
{
    char *src = str;
    size_t length = strlen(str);
    unsigned char *buf;
    unsigned char *dst;
    unsigned int value;
  
    for(size_t i = 0; i < length; i++)
    {
        if(!isxdigit(src[i]))
        {
            fprintf(stderr, "erreur: la chaîne de caractères n'est pas sous forme hexadécimal\n");
            exit(EXIT_FAILURE);
        }
    }
  
    if((length % 2) != 0)
    {
        fprintf(stderr, "erreur: la longueur de la string n'est pas multiple de 2\n");
        exit(EXIT_FAILURE);
    }
  
    buf = calloc(length / 2 + 1, sizeof(unsigned char));
  
    dst = buf;
  
    while(*src && (sscanf(src, "%2x", &value) == 1))
    {
        *dst = (unsigned char)value;
        src += 2;
        dst++;
    }
  
    return buf;
}
```

Attention à bien libérer **buf** avec la fonction **free()**.
