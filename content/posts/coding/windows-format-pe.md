---
title: "le format Portable Executable sur windows"
slug: "windows-format-pe"
description: "Article sur le format PE, le format Portable Executable sur windows"
tags: ["c", "windows"]
categories: ["hacking", "coding"]
layout: "post-with-toc"
---

# le format Portable Executable sur windows

Le **format PE** pour **Portable Executable** est le format utilisé sur Micorsoft Windows pour organiser les fichiers exécutables ainsi que les fichiers objets.\
Donc, les fichiers **.exe** ou **.dll** pour ne citer qu'eux sont créés selon ce format de fichier.

## Introduction

Nous allons voir comment analyser ces types de fichiers pour obtenir toutes les informations utiles qui nous sont disponibles.

Voici un schéma représentant **la structure d'un fichier au format PE** :

```txt
+--------------------+
|     Section n      |
+--------------------+
|        ...         |
+--------------------+
|     Section 2	     |
+--------------------+
|     Section 1      |
+--------------------+
|   Tableau de       | tableau de
|   section header   | IMAGE_SECTION_HEADER
+--------------------+-----------------------
| PE Optional header | IMAGE_OPTIONAL_HEADER
+--------------------+
|   PE File header   | IMAGE_FILE_HEADER        IMAGE_NT_HEADERS
+--------------------+                         /
|    PE Signature    | PE\0\0                 /
+--------------------+-----------------------/
|                    |
+--------------------+
|   MS-DOS header    | IMAGE_DOS_HEADER
+--------------------+ offset 0
```

L'en-tête MS-DOS : **IMAGE_DOS_HEADER** est là pour **permettre aux fichiers PE de rester compatible avec MS-DOS**, nous allons nous occuper seulement de 2 des champs de cette structure.

* Le champ **e_magic** qui doit être à **IMAGE_DOS_SIGNATURE** ou encore **0x5A4D** qui signifie **MZ** et correspond aux initiales de Mark Zbikowski, l'un des développeurs de MS-DOS.
Ce champ sert à vérifier que le fichier est bien valide.

* Le champ **e_lfanew** qui est l'offset vers la structure **IMAGE_NT_HEADERS** qui regroupe donc : **"PE Signature", "PE File header" et "PE Optional header"**.

## La structure IMAGE_NT_HEADERS

Nous allons commencer par chercher puis afficher les informations disponibles dans la structures **IMAGE_FILE_HEADER**.

Il nous faut lire **IMAGE_DOS_HEADER**, puis aller à l'offset **e_lfanew** et enfin lire complètement **IMAGE_NT_HEADERS**.

{{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/-/blob/main/DumpImageFileHeader/DumpImageFileHeader.c" title="DumpImageFileHeader.c" >}}

```text
PS Z:\win\le-format-pe\Debug> .\DumpImageFileHeader.exe C:\Windows\System32\kernel32.dll
[*] Machine: 014C (x86)

[*] NumberOfSections: 6

[*] TimeDateStamp: 2640946508
    Mon Sep  8 05:15:08 2053

[*] PointerToSymbolTable: 00000000

[*] NumberOfSymbols: 0

[*] SizeOfOptionalHeader: 224

[*] Characteristics: 2102
    EXECUTABLE_IMAGE
    32BIT_MACHINE
    DLL
PS Z:\win\le-format-pe\Debug>
```

## Les structures IMAGE_SECTION_HEADER

Maintenant, occupons-nous de lister les informations concernant chaque section.
Le tableau des sections ce trouve juste après **IMAGE_NT_HEADERS**.
Dans le code précédent nous avons obtenu le nombre de sections disponibles, nous savons donc déjà combien de **IMAGE_SECTION_HEADER** il faut lire.

{{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/-/blob/main/DumpSectionsInfos/DumpSectionsInfos.c" title="DumpSectionsInfos.c" >}}

```text
PS Z:\win\le-format-pe\Debug> .\DumpSectionsInfos.exe C:\Windows\System32\kernel32.dll
[*] Name: .text

[*] VirtualSize: 415237

[*] VirtualAddress: 00010000

[*] SizeOfRawData: 417792

[*] PointerToRawData: 00001000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: 60000020
    CNT_CODE
    MEM_EXECUTE
    MEM_READ

    -------------------------


[*] Name: .rdata

[*] VirtualSize: 172726

[*] VirtualAddress: 00080000

[*] SizeOfRawData: 176128

[*] PointerToRawData: 00067000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: 40000040
    CNT_INITIALIZED_DATA
    MEM_READ

    -------------------------


[*] Name: .data

[*] VirtualSize: 3264

[*] VirtualAddress: 000B0000

[*] SizeOfRawData: 4096

[*] PointerToRawData: 00092000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: C0000040
    CNT_INITIALIZED_DATA
    MEM_READ
    MEM_WRITE

    -------------------------


[*] Name: .didat

[*] VirtualSize: 84

[*] VirtualAddress: 000C0000

[*] SizeOfRawData: 4096

[*] PointerToRawData: 00093000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: C0000040
    CNT_INITIALIZED_DATA
    MEM_READ
    MEM_WRITE

    -------------------------


[*] Name: .rsrc

[*] VirtualSize: 1312

[*] VirtualAddress: 000D0000

[*] SizeOfRawData: 4096

[*] PointerToRawData: 00094000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: 40000040
    CNT_INITIALIZED_DATA
    MEM_READ

    -------------------------


[*] Name: .reloc

[*] VirtualSize: 18584

[*] VirtualAddress: 000E0000

[*] SizeOfRawData: 20480

[*] PointerToRawData: 00095000

[*] PointerToRelocations: 00000000

[*] PointerToLinenumbers: 00000000

[*] NumberOfRelocations: 0

[*] NumberOfLinenumbers: 0

[*] Characteristics: 42000040
    CNT_INITIALIZED_DATA
    MEM_DISCARDABLE
    MEM_READ

    -------------------------


PS Z:\win\le-format-pe\Debug>
```

Puis, pour dumper une section, il suffit de se placer à **PointerToRawData** de la section choisie et de lire les bytes.

Exemple pour dumper la section ou le segment **.text** :

{{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/-/raw/main/DumpTextSection/DumpTextSection.c" title="DumpTextSection.c" >}}

```txt
PS Z:\win\le-format-pe\Debug> .\DumpTextSection.exe C:\Windows\System32\kernel32.dll
CC CC CC CC CC CC 8B FF 53 33 DB 56 85 D2 74 1F
(bla bla bla)
1D 88 6B 5F C3
PS Z:\win\le-format-pe\Debug>
```

## La structure IMAGE_IMPORT_DESCRIPTOR

Allons-y pour la **table d'import**.
Cette table contient les **fonctions importées**, c'est à dire les fonctions utilisées par un programme (fichier en .exe) ou une dll (fichier en .dll) mais dont le code ne réside pas dans ces derniers.
Elles sont en fait définies dans d'autres dll.

Par exemple, pour utiliser la fonction **ExitProcess** dans un programme, nous devons l'importer de **kernel32.dll**, pour la fonction **MessageBox**, nous l'importons de **user32.dll**.

Il est nécessaire pour lister les fonctions importées de savoir ce qu'est une **RVA** qui signifie **Relative Virtual Address**.

Une **RVA** est enfaite une **adresse relative à une autre : l'adresse de base**.
Par exemple si **ImageBase** est à **0x00400000** et que **AddressOfEntryPoint** est à **0x00001000**, alors l'adresse du point d'entrée de l'exécutable sera à **0x00401000**.

Nous accédons à la table d'importation grâce au champ **DataDirectory** contenu dans **IMAGE_OPTIONAL_HEADER** qui est un tableau de structure **IMAGE_DATA_DIRECTORY** contenant 2 membres :
La **RVA** d'une structure de donnée et sa taille.

La structure de donnée qui nous intéresse, c'est à dire celle des importations,
se nomme **IMAGE_IMPORT_DESCRIPTOR** et est accessible grâce à la constante **IMAGE_DIRECTORY_ENTRY_IMPORT** qui correspond à l'index du tableau **IMAGE_DATA_DIRECTORY**.

Il y a autant de structure **IMAGE_IMPORT_DESCRIPTOR** que de dll depuis lesquelles des fonctions ont été importées. La dernière a tous ses champs mis à 0.

Le champ **Name** de la structure **IMAGE_IMPORT_DESCRIPTOR** contient la **RVA** vers le nom de la dll.
Les champs **OriginalFirstThunk** et **FirstThunk** sont normalement identiques tant que le programme n'est pas chargé en mémoire.
Ils peuvent être : une **RVA** vers une structure appelée **IMAGE_IMPORT_BY_NAME** ou un simple **DWORD**, car une fonction peut être importée par son nom mais aussi par son **ordinal** (son index).

Pour savoir si la fonction est importée par son nom ou par son ordinal, le bit de poid fort du **Thunk** sera égal à 0 ou sinon à 1.
**L'index** ou **l'ordinal** sera le **WORD** de poid faible. Pour tester ce bit, il existe une constante appelée **IMAGE_ORDINAL_FLAG32**.

Si la fonction est importée par son nom, une structure nommée **IMAGE_IMPORT_BY_NAME** est utilisée et contient deux champs :
Le premier **Hint** contient **l'index** ou **l'ordinal** de la fonction dans la table d'exportation de la dll, et le second **Name** est le nom de la fonction.

Si le programme ou la dll est chargé en mémoire, alors **FirstThunk** devient l'adresse effective de la fonction et **OriginalFirstThunk** reste inchangé.

Voici un exemple de code utilisé pour lister les fonctions importées dans un fichier PE :

{{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/-/blob/main/ListImportedFunctions/ListImportedFunctions.c" title="ListImportedFunctions.c" >}}

```txt
PS Z:\win\le-format-pe\Debug> .\ListImportedFunctions.exe C:\Windows\System32\kernel32.dll
[+] api-ms-win-core-rtlsupport-l1-1-0.dll
    6 - RtlUnwind
    0 - RtlCaptureContext
[+] api-ms-win-core-rtlsupport-l1-2-0.dll
    3 - RtlPcToFileHeader
[+] ntdll.dll
    2289 - _aullshr
    1536 - RtlUnhandledExceptionFilter
    642 - NtTerminateProcess
    2457 - wcsncmp
    2458 - wcsncpy
    114 - LdrFindResourceEx_U
    1381 - RtlReadThreadProfilingData
    1361 - RtlQueryThreadProfiling
    954 - RtlEnableThreadProfiling
    932 - RtlDisableThreadProfiling
    1299 - RtlNtStatusToDosErrorNoTeb
    412 - NtMapUserPhysicalPagesScatter
    895 - RtlDecodeSystemPointer
    2367 - bsearch
    820 - RtlComputeImportTableHash
    1001 - RtlFindActivationContextSectionGuid
    853 - RtlCreateActivationContext
    413 - NtMapViewOfSection
    937 - RtlDoesFileExists_U
    1493 - RtlSizeHeap
    1622 - RtlpConvertCultureNamesToLCIDs
    494 - NtQueryInstallUILanguage
    1320 - RtlPublishWnfStateData
    2348 - _wcslwr
    499 - NtQueryLicenseValue
    2361 - _wtol
    2402 - memmove_s
    1040 - RtlGUIDFromString
    2401 - memmove
    1121 - RtlHashUnicodeString
    689 - NtWow64ReadVirtualMemory64
    61 - EtwEventUnregister
    1513 - RtlTimeFieldsToTime
    34 - DbgPrint
    2459 - wcsncpy_s
    1060 - RtlGetDeviceFamilyInfoEnum
    284 - NtCreateFile
    957 - RtlEncodeSystemPointer
    1330 - RtlQueryEnvironmentVariable_U
    57 - EtwEventEnabled
    1517 - RtlTimeToTimeFields
    657 - NtUnmapViewOfSection
    1389 - RtlReleaseActivationContext
    156 - LdrResFindResourceDirectory
    1337 - RtlQueryInformationActivationContext
    1477 - RtlSetThreadPreferredUILanguages
    2435 - swprintf_s
    1129 - RtlImageNtHeaderEx
    931 - RtlDetermineDosPathNameType_U
    1341 - RtlQueryPackageClaims
    1617 - RtlZombifyActivationContext
    1502 - RtlSubAuthorityCountSid
    721 - RtlActivateActivationContext
    1626 - RtlpEnsureBufferSize
    1618 - RtlpApplyLengthFunction
    1322 - RtlQueryActivationContextApplicationSettings
    1043 - RtlGetActiveActivationContext
    943 - RtlDosPathNameToNtPathName_U_WithStatus
    308 - NtCreateSection
    35 - DbgPrintEx
    1079 - RtlGetLengthWithoutLastFullDosOrNtPathElement
    1070 - RtlGetFullPathName_U
    890 - RtlDeactivateActivationContext
    1283 - RtlMultiAppendUnicodeStringBuffer
    742 - RtlAddRefActivationContext
    2376 - isdigit
    2366 - atol
    2438 - tolower
    2439 - toupper
    1543 - RtlUnicodeStringToOemString
    20 - CsrAllocateCaptureBuffer
    859 - RtlCreateEnvironmentEx
    2462 - wcsrchr
    877 - RtlCreateUnicodeString
    923 - RtlDestroyEnvironment
    523 - NtQueryVolumeInformationFile
    858 - RtlCreateEnvironment
    28 - CsrFreeCaptureBuffer
    1034 - RtlFreeOemString
    530 - NtRaiseHardError
    1053 - RtlGetCurrentDirectory_U
    26 - CsrClientCallServer
    59 - EtwEventRegister
    1030 - RtlFreeAnsiString
    977 - RtlEqualUnicodeString
    1539 - RtlUnicodeStringToAnsiString
    985 - RtlExitUserThread
    1353 - RtlQueryProtectedPolicy
    739 - RtlAddIntegrityLabelToBoundaryDescriptor
    552 - NtReplacePartitionUnit
    521 - NtQueryValueKey
    975 - RtlEqualSid
    448 - NtOpenThreadToken
    66 - EtwEventWriteNoRegistration
    1026 - RtlFormatCurrentUserKeyPath
    1149 - RtlInitUnicodeStringEx
    490 - NtQueryInformationToken
    719 - RtlAcquireSRWLockExclusive
    136 - LdrLoadDll
    601 - NtSetInformationThread
    1396 - RtlReleaseSRWLockExclusive
    175 - LdrUnloadDll
    429 - NtOpenKey
    766 - RtlAppendUnicodeToString
    1503 - RtlSubAuthoritySid
    1342 - RtlQueryPackageIdentity
    1612 - RtlWow64LogMessageInEventLogger
    984 - RtlExitUserProcess
    765 - RtlAppendUnicodeStringToString
    126 - LdrGetProcedureAddress
    1170 - RtlInitializeSid
    439 - NtOpenProcessToken
    1356 - RtlQueryRegistryValuesEx
    816 - RtlCompareUnicodeString
    1660 - RtlxAnsiStringToUnicodeSize
    1136 - RtlInitAnsiStringEx
    761 - RtlAnsiStringToUnicodeString
    1224 - RtlIsNameLegalDOS8Dot3
    1056 - RtlGetCurrentProcessorNumberEx
    667 - NtWaitForSingleObject
    282 - NtCreateEvent
    1467 - RtlSetSearchPathMode
    117 - LdrGetDllDirectory
    1266 - RtlLockHeap
    1553 - RtlUnlockHeap
    1117 - RtlGetUserInfoHeap
    2350 - _wcsnicmp
    2423 - strncmp
    2326 - _strnicmp
    809 - RtlCompactHeap
    918 - RtlDeregisterSecureMemoryCacheCallback
    1386 - RtlRegisterSecureMemoryCacheCallback
    426 - NtOpenFile
    371 - NtFsControlFile
    259 - NtClose
    2347 - _wcsicmp
    102 - LdrAddRefDll
    484 - NtQueryInformationFile
    2452 - wcscpy_s
    593 - NtSetInformationFile
    1044 - RtlGetActiveConsoleId
    1298 - RtlNtStatusToDosError
    891 - RtlDeactivateActivationContextUnsafeFast
    723 - RtlActivateActivationContextUnsafeFast
    1038 - RtlFreeUnicodeString
    591 - NtSetInformationDebugObject
    45 - DbgUiGetThreadDebugObject
    46 - DbgUiIssueRemoteBreakin
    617 - NtSetSystemInformation
    1058 - RtlGetCurrentTransaction
    487 - NtQueryInformationProcess
    1442 - RtlSetCurrentTransaction
    1455 - RtlSetLastWin32Error
    1484 - RtlSetUserCallbackExceptionFilter
    1464 - RtlSetProtectedPolicy
    1102 - RtlGetSuiteMask
    1128 - RtlImageNtHeader
    108 - LdrDisableThreadCalloutsForDll
    1148 - RtlInitUnicodeString
    1476 - RtlSetThreadPoolStartFunc
    146 - LdrQueryImageFileExecutionOptions
    2344 - _vsnwprintf
    166 - LdrSetDllManifestProber
    871 - RtlCreateSecurityDescriptor
    1450 - RtlSetGroupSecurityDescriptor
    852 - RtlCreateAcl
    1032 - RtlFreeHeap
    724 - RtlAddAccessAllowedAce
    1179 - RtlIntegerToUnicodeString
    350 - NtEnumerateKey
    1623 - RtlpConvertLCIDsToCultureNames
    1114 - RtlGetUILanguageInfo
    1541 - RtlUnicodeStringToInteger
    21 - CsrAllocateMessagePointer
    62 - EtwEventWrite
    479 - NtQueryEvent
    1243 - RtlLCIDToCultureName
    1676 - TpAllocTimer
    1673 - TpAllocIoCompletion
    1678 - TpAllocWork
    1684 - TpCallbackMayRunLong
    1672 - TpAllocCleanupGroup
    1723 - TpSimpleTryPost
    1700 - TpQueryPoolStackInformation
    1675 - TpAllocPool
    1714 - TpSetPoolMinThreads
    1456 - RtlSetLastWin32ErrorAndNtStatusFromNtStatus
    1715 - TpSetPoolStackInformation
    1677 - TpAllocWait
    1329 - RtlQueryEnvironmentVariable
    1119 - RtlGetVersion
    337 - NtDeleteValueKey
    626 - NtSetValueKey
    2440 - towlower
    1057 - RtlGetCurrentServiceSessionId
    1092 - RtlGetPersistedStateLocation
    32 - CsrVerifyRegion
    788 - RtlCharToInteger
    1135 - RtlInitAnsiString
    1564 - RtlUpcaseUnicodeChar
    1547 - RtlUnicodeToMultiByteSize
    922 - RtlDestroyAtomTable
    358 - NtFindAtom
    481 - NtQueryInformationAtom
    733 - RtlAddAtomToAtomTable
    205 - NtAddAtomEx
    330 - NtDeleteAtom
    855 - RtlCreateAtomTable
    903 - RtlDeleteAtomFromAtomTable
    1272 - RtlLookupAtomInAtomTable
    1324 - RtlQueryAtomInAtomTable
    936 - RtlDnsHostNameToComputerName
    1316 - RtlPrefixString
    363 - NtFlushKey
    2313 - _memicmp
    1662 - RtlxUnicodeStringToAnsiSize
    963 - RtlEnterCriticalSection
    2449 - wcschr
    2464 - wcsstr
    1253 - RtlLeaveCriticalSection
    290 - NtCreateKey
    878 - RtlCreateUnicodeStringFromAsciiz
    2453 - wcscspn
    289 - NtCreateJobSet
    1393 - RtlReleasePrivilege
    595 - NtSetInformationJobObject
    485 - NtQueryInformationJobObject
    288 - NtCreateJobObject
    715 - RtlAcquirePrivilege
    246 - NtAssignProcessToJobObject
    641 - NtTerminateJobObject
    428 - NtOpenJobObject
    1255 - RtlLengthSecurityDescriptor
    585 - NtSetEaFile
    614 - NtSetSecurityObject
    478 - NtQueryEaFile
    510 - NtQuerySecurityObject
    148 - LdrQueryImageFileKeyOption
    139 - LdrOpenImageFileOptionsKey
    1328 - RtlQueryElevationFlags
    598 - NtSetInformationProcess
    1373 - RtlRaiseStatus
    508 - NtQuerySection
    368 - NtFreeVirtualMemory
    691 - NtWriteFile
    353 - NtEnumerateValueKey
    976 - RtlEqualString
    1546 - RtlUnicodeToMultiByteN
    2425 - strncpy_s
    655 - NtUnlockFile
    942 - RtlDosPathNameToNtPathName_U
    531 - NtReadFile
    402 - NtLockFile
    849 - RtlCopyUnicodeString
    1234 - RtlIsTextUnicode
    219 - NtAllocateVirtualMemory
    1082 - RtlGetLongestNtPathLength
    1317 - RtlPrefixUnicodeString
    1284 - RtlMultiByteToUnicodeN
    1285 - RtlMultiByteToUnicodeSize
    944 - RtlDosPathNameToRelativeNtPathName_U
    1394 - RtlReleaseRelativeName
    1454 - RtlSetIoCompletionCallback
    919 - RtlDeregisterWait
    1388 - RtlRegisterWait
    1127 - RtlImageDirectoryEntryToData
    522 - NtQueryVirtualMemory
    857 - RtlCreateBoundaryDescriptor
    464 - NtProtectVirtualMemory
    1109 - RtlGetThreadErrorMode
    294 - NtCreateMailslotFile
    993 - RtlExtendedLargeIntegerDivide
    929 - RtlDestroyQueryDebugBuffer
    1348 - RtlQueryProcessDebugInformation
    869 - RtlCreateQueryDebugBuffer
    474 - NtQueryDirectoryFile
    2418 - strcpy_s
    1002 - RtlFindActivationContextSectionString
    165 - LdrSetDllDirectory
    115 - LdrFindResource_U
    1508 - RtlSwitchedVVI
    393 - NtIsSystemResumeAutomatic
    524 - NtQueryWnfStateData
    454 - NtPowerInformation
    391 - NtInitiatePowerAction
    377 - NtGetDevicePowerState
    620 - NtSetThreadExecutionState
    616 - NtSetSystemEnvironmentValueEx
    515 - NtQuerySystemEnvironmentValueEx
    1143 - RtlInitString
    627 - NtSetVolumeInformationFile
    517 - NtQuerySystemInformationEx
    340 - NtDeviceIoControlFile
    489 - NtQueryInformationThread
    1237 - RtlIsValidHandle
    754 - RtlAllocateHandle
    1378 - RtlReAllocateHeap
    1031 - RtlFreeHandle
    1486 - RtlSetUserValueHeap
    1562 - RtlUnsubscribeWnfStateChangeNotification
    1505 - RtlSubscribeWnfStateChangeNotification
    1367 - RtlQueryWnfStateData
    2415 - strchr
    1445 - RtlSetEnvironmentStrings
    1308 - RtlOemStringToUnicodeString
    2448 - wcscat_s
    752 - RtlAllocateAndInitializeSid
    467 - NtQueryAttributesFile
    1035 - RtlFreeSid
    2428 - strrchr
    480 - NtQueryFullAttributesFile
    1692 - TpCaptureCaller
    2323 - _stricmp
    624 - NtSetTimerResolution
    520 - NtQueryTimerResolution
    1047 - RtlGetAppContainerSidType
    834 - RtlConvertSidToUnicodeString
    1447 - RtlSetEnvironmentVariable
    1046 - RtlGetAppContainerParent
    1426 - RtlRunOnceExecuteOnce
    1156 - RtlInitializeCriticalSection
    1526 - RtlTryAcquirePebLock
    1392 - RtlReleasePebLock
    1088 - RtlGetNtSystemRoot
    665 - NtWaitForMultipleObjects
    258 - NtClearEvent
    1601 - RtlWerpReportException
    1111 - RtlGetThreadPreferredUILanguages
    159 - LdrResSearchResource
    2460 - wcsnlen
    2414 - strcat_s
    2426 - strnlen
    192 - NlsMbCodePageTag
    244 - NtApphelpCacheControl
    1071 - RtlGetFullPathName_UEx
    1832 - ZwClose
    1997 - ZwOpenFile
    2000 - ZwOpenKey
    1922 - ZwEnumerateKey
    2092 - ZwQueryValueKey
    1857 - ZwCreateFile
    2055 - ZwQueryInformationFile
    1881 - ZwCreateSection
    2045 - ZwQueryDirectoryFile
    1297 - RtlNtPathNameToDosPathName
    1084 - RtlGetNativeSystemInformation
    2087 - ZwQuerySystemInformation
    2228 - ZwUnmapViewOfSection
    1984 - ZwMapViewOfSection
    1733 - VerSetConditionMask
    1589 - RtlVerifyVersionInfo
    119 - LdrGetDllHandle
    18 - ApiSetQueryApiSetPresence
    1443 - RtlSetDaclSecurityDescriptor
    516 - NtQuerySystemInformation
    755 - RtlAllocateHeap
    1458 - RtlSetOwnerSecurityDescriptor
    2267 - _CIcos
    2270 - _CIsin
    2276 - _alldiv
    2278 - _allmul
    2283 - _allshl
    2290 - _chkstk
    2296 - _ftol2_sse
    2372 - floor
    2398 - memcmp
    2399 - memcpy
    2403 - memset
[+] KERNELBASE.dll
    38 - BaseGetNamedObjectDirectory
    36 - BaseFormatObjectAttributes
    212 - GetVolumeNameForVolumeMountPointW
    178 - GetRegistryExtensionFlags
    233 - KernelBaseGetGlobalData
    214 - GlobalFree
    236 - LoadStringBaseExW
    7 - AppContainerLookupMoniker
    271 - PackageIdFromFullName
    58 - CompareStringA
    205 - GetUnicodeStringToEightBitStringRoutine
    204 - GetUnicodeStringToEightBitSizeRoutine
    133 - GetNamedPipeAttribute
    29 - AppXReleaseAppXContext
    315 - ReleasePackagedDataForFile
    27 - AppXPostSuccessExtension
    169 - GetPackagedDataForFile
    28 - AppXPreCreationExtension
    32 - AreFileApisANSI
    284 - PrivCopyFileExW
    77 - EnumLanguageGroupLocalesW
    5 - AppContainerFreeMemory
    43 - BasepNotifyTrackingService
    243 - MoveFileWithProgressTransactedW
    40 - BasepAdjustObjectAttributesForPrivateNamespace
    120 - GetEightBitStringToUnicodeStringRoutine
    193 - GetStringTableEntry
    46 - CheckGroupPolicyEnabled
    256 - OpenRegKey
    217 - InternalLcidToName
    248 - NlsIsUserDefaultLocale
    175 - GetPtrCalDataArray
    210 - GetUserOverrideString
    174 - GetPtrCalData
    218 - Internal_EnumCalendarInfo
    220 - Internal_EnumLanguageGroupLocales
    221 - Internal_EnumSystemCodePages
    219 - Internal_EnumDateFormats
    225 - Internal_EnumUILanguages
    222 - Internal_EnumSystemLanguageGroups
    249 - NlsValidateLocale
    224 - Internal_EnumTimeFormats
    132 - GetNamedLocaleHashNode
    211 - GetUserOverrideWord
    131 - GetLocaleInfoHelper
    97 - GetCalendar
    145 - GetPackageFullName
    105 - GetCurrentPackageFullName
    48 - CheckIsMSIXPackage
    51 - ClosePackageInfo
    21 - AppXGetOSMaxVersionTested
    167 - GetPackageTargetPlatformProperty
    203 - GetTargetPlatformContext
    255 - OpenPackageInfoByFullNameForUser
    34 - BaseDllFreeResourceId
    35 - BaseDllMapResourceIdW
    240 - LocalUnlock
    194 - GetStringTypeA
    391 - lstrcmpW
    393 - lstrcmpiW
    330 - SetFileApisToANSI
    397 - lstrlenW
    60 - CreateProcessAsUserA
    239 - LocalReAlloc
    62 - CreateProcessInternalA
    394 - lstrcpynA
    331 - SetFileApisToOEM
    45 - CheckAllowDecryptedRemoteDestinationPolicy
    86 - FatalAppExitW
    213 - GlobalAlloc
    288 - PulseEvent
    250 - NotifyMountMgr
    85 - FatalAppExitA
    79 - EnumSystemLocalesEx
    78 - EnumSystemLanguageGroupsW
    340 - Sleep
    215 - HeapSummary
    171 - GetProcAddressForCaller
    235 - LCIDToLocaleName
    197 - GetSystemDefaultLocaleName
    242 - MapViewOfFileExNuma
    237 - LocalAlloc
    121 - GetEraNameCountedString
    396 - lstrlenA
    80 - EnumUILanguagesW
    207 - GetUserDefaultUILanguage
    61 - CreateProcessAsUserW
    63 - CreateProcessInternalW
    206 - GetUserDefaultLocaleName
    198 - GetSystemDefaultUILanguage
    395 - lstrcpynW
    238 - LocalLock
[+] api-ms-win-core-processthreads-l1-1-0.dll
    87 - TlsAlloc
    55 - ProcessIdToSessionId
    20 - GetExitCodeThread
    60 - ResumeThread
    83 - SuspendThread
    33 - GetProcessVersion
    43 - GetThreadPriority
    77 - SetThreadPriority
    26 - GetProcessId
    70 - SetProcessShutdownParameters
    27 - GetProcessIdOfThread
    85 - TerminateProcess
    58 - QueueUserAPC
    90 - TlsSetValue
    62 - SetProcessAffinityUpdateMode
    44 - GetThreadPriorityBoost
    5 - CreateRemoteThreadEx
    34 - GetStartupInfoW
    19 - GetExitCodeProcess
    52 - OpenProcessToken
    13 - GetCurrentProcessId
    12 - GetCurrentProcess
    84 - SwitchToThread
    78 - SetThreadPriorityBoost
    56 - QueryProcessAffinityUpdateMode
    88 - TlsFree
    0 - CreateProcessA
    89 - TlsGetValue
    3 - CreateProcessW
    81 - SetThreadStackGuarantee
    40 - GetThreadId
    32 - GetProcessTimes
    53 - OpenThread
    4 - CreateRemoteThread
    48 - InitializeProcThreadAttributeList
    61 - SetPriorityClass
    22 - GetPriorityClass
    86 - TerminateThread
    91 - UpdateProcThreadAttribute
    7 - DeleteProcThreadAttributeList
[+] api-ms-win-core-processthreads-l1-1-3.dll
    31 - GetProcessShutdownParameters
    67 - SetProcessInformation
    28 - GetProcessInformation
    74 - SetThreadIdealProcessor
[+] api-ms-win-core-processthreads-l1-1-2.dll
    69 - SetProcessPriorityBoost
    76 - SetThreadInformation
    42 - GetThreadInformation
    36 - GetSystemTimes
    39 - GetThreadIOPendingFlag
    30 - GetProcessPriorityBoost
[+] api-ms-win-core-processthreads-l1-1-1.dll
    72 - SetThreadContext
    47 - GetThreadTimes
    10 - FlushInstructionCache
    29 - GetProcessMitigationPolicy
    68 - SetProcessMitigationPolicy
    50 - IsProcessorFeaturePresent
    75 - SetThreadIdealProcessorEx
    41 - GetThreadIdealProcessorEx
    25 - GetProcessHandleCount
    51 - OpenProcess
    37 - GetThreadContext
[+] api-ms-win-core-registry-l1-1-0.dll
    5 - RegDeleteKeyExW
    16 - RegEnumValueW
    28 - RegOpenCurrentUser
    38 - RegRestoreKeyA
    37 - RegQueryValueExW
    33 - RegQueryInfoKeyW
    24 - RegLoadKeyW
    19 - RegGetValueA
    18 - RegGetKeySecurity
    32 - RegQueryInfoKeyA
    13 - RegEnumKeyExA
    11 - RegDeleteValueW
    27 - RegNotifyChangeKeyValue
    0 - RegCloseKey
    25 - RegLoadMUIStringA
    30 - RegOpenKeyExW
    36 - RegQueryValueExA
    20 - RegGetValueW
    48 - RegUnLoadKeyW
    22 - RegLoadAppKeyW
    4 - RegDeleteKeyExA
    10 - RegDeleteValueA
    2 - RegCreateKeyExA
    15 - RegEnumValueA
    17 - RegFlushKey
    39 - RegRestoreKeyW
    3 - RegCreateKeyExW
    29 - RegOpenKeyExA
    47 - RegUnLoadKeyA
    31 - RegOpenUserClassesRoot
    40 - RegSaveKeyExA
    14 - RegEnumKeyExW
    42 - RegSetKeySecurity
    12 - RegDisablePredefinedCacheEx
    41 - RegSaveKeyExW
    9 - RegDeleteTreeW
    26 - RegLoadMUIStringW
    46 - RegSetValueExW
    45 - RegSetValueExA
    1 - RegCopyTreeW
    23 - RegLoadKeyA
    8 - RegDeleteTreeA
[+] api-ms-win-core-heap-l1-1-0.dll
    2 - HeapAlloc
    0 - GetProcessHeap
    4 - HeapCreate
    7 - HeapLock
    14 - HeapWalk
    13 - HeapValidate
    1 - GetProcessHeaps
    10 - HeapSetInformation
    3 - HeapCompact
    5 - HeapDestroy
    8 - HeapQueryInformation
    12 - HeapUnlock
    9 - HeapReAlloc
    6 - HeapFree
[+] api-ms-win-core-heap-l2-1-0.dll
    3 - LocalFree
[+] api-ms-win-core-memory-l1-1-1.dll
    53 - VirtualLock
    39 - SetProcessWorkingSetSizeEx
    15 - GetSystemFileCacheSize
    29 - QueryMemoryResourceNotification
    35 - ResetWriteWatch
    11 - GetLargePageMinimum
    16 - GetWriteWatch
    7 - CreateMemoryResourceNotification
    40 - SetSystemFileCacheSize
    59 - VirtualUnlock
    5 - CreateFileMappingNumaW
    14 - GetProcessWorkingSetSizeEx
    13 - GetProcessWorkingSetSize
    38 - SetProcessWorkingSetSize
[+] api-ms-win-core-memory-l1-1-0.dll
    6 - CreateFileMappingW
    52 - VirtualFreeEx
    55 - VirtualProtectEx
    41 - UnmapViewOfFile
    18 - MapViewOfFile
    45 - VirtualAlloc
    58 - VirtualQueryEx
    27 - OpenFileMappingW
    51 - VirtualFree
    54 - VirtualProtect
    61 - WriteProcessMemory
    32 - ReadProcessMemory
    21 - MapViewOfFileEx
    57 - VirtualQuery
    48 - VirtualAllocEx
    9 - FlushViewOfFile
[+] api-ms-win-core-memory-l1-1-2.dll
    0 - AllocateUserPhysicalPages
    44 - UnregisterBadMemoryNotification
    49 - VirtualAllocExNuma
    34 - RegisterBadMemoryNotification
    17 - MapUserPhysicalPages
    10 - FreeUserPhysicalPages
    2 - AllocateUserPhysicalPagesNuma
    12 - GetMemoryErrorHandlingCapabilities
[+] api-ms-win-core-handle-l1-1-0.dll
    2 - DuplicateHandle
    3 - GetHandleInformation
    4 - SetHandleInformation
    0 - CloseHandle
[+] api-ms-win-core-synch-l1-1-0.dll
    5 - CreateEventExW
    4 - CreateEventExA
    3 - CreateEventA
    2 - CancelWaitableTimer
    17 - EnterCriticalSection
    29 - LeaveCriticalSection
    39 - ResetEvent
    6 - CreateEventW
    15 - DeleteCriticalSection
    24 - InitializeCriticalSection
    38 - ReleaseSemaphore
    35 - ReleaseMutex
    34 - OpenWaitableTimerW
    41 - SetEvent
    33 - OpenSemaphoreW
    55 - WaitForSingleObjectEx
    54 - WaitForSingleObject
    53 - WaitForMultipleObjectsEx
    48 - SleepEx
    7 - CreateMutexA
    8 - CreateMutexExA
    9 - CreateMutexExW
    10 - CreateMutexW
    11 - CreateSemaphoreExW
    42 - SetWaitableTimer
    13 - CreateWaitableTimerExW
    25 - InitializeCriticalSectionAndSpinCount
    26 - InitializeCriticalSectionEx
    30 - OpenEventA
    31 - OpenEventW
    32 - OpenMutexW
[+] api-ms-win-core-synch-l1-2-1.dll
    12 - CreateSemaphoreW
    52 - WaitForMultipleObjects
[+] api-ms-win-core-synch-l1-2-0.dll
    28 - InitializeSynchronizationBarrier
    21 - InitOnceExecuteOnce
    16 - DeleteSynchronizationBarrier
    44 - SignalObjectAndWait
    18 - EnterSynchronizationBarrier
[+] api-ms-win-core-file-l1-1-0.dll
    75 - QueryDosDeviceW
    54 - GetFullPathNameW
    76 - ReadFile
    74 - LockFileEx
    73 - LockFile
    45 - GetFileAttributesW
    44 - GetFileAttributesExW
    72 - LocalFileTimeToFileTime
    42 - GetFileAttributesA
    61 - GetTempFileNameW
    40 - GetDriveTypeA
    37 - GetDiskFreeSpaceW
    36 - GetDiskFreeSpaceExW
    35 - GetDiskFreeSpaceExA
    34 - GetDiskFreeSpaceA
    31 - FlushFileBuffers
    30 - FindVolumeClose
    29 - FindNextVolumeW
    27 - FindNextFileW
    25 - FindNextFileA
    24 - FindNextChangeNotification
    23 - FindFirstVolumeW
    21 - FindFirstFileW
    19 - FindFirstFileExW
    18 - FindFirstFileExA
    17 - FindFirstFileA
    16 - FindFirstChangeNotificationW
    15 - FindFirstChangeNotificationA
    14 - FindCloseChangeNotification
    13 - FindClose
    12 - FileTimeToLocalFileTime
    11 - DeleteVolumeMountPointW
    10 - DeleteFileW
    9 - DeleteFileA
    8 - DefineDosDeviceW
    7 - CreateFileW
    6 - CreateFileA
    4 - CreateDirectoryW
    3 - CreateDirectoryA
    2 - CompareFileTime
    78 - ReadFileScatter
    79 - RemoveDirectoryA
    80 - RemoveDirectoryW
    81 - SetEndOfFile
    84 - SetFileAttributesA
    85 - SetFileAttributesW
    86 - SetFileInformationByHandle
    88 - SetFilePointer
    89 - SetFilePointerEx
    90 - SetFileTime
    91 - SetFileValidData
    92 - UnlockFile
    93 - UnlockFileEx
    94 - WriteFile
    95 - WriteFileEx
    96 - WriteFileGather
    70 - GetVolumePathNameW
    68 - GetVolumeInformationW
    53 - GetFullPathNameA
    67 - GetVolumeInformationByHandleW
    52 - GetFinalPathNameByHandleW
    51 - GetFinalPathNameByHandleA
    50 - GetFileType
    49 - GetFileTime
    41 - GetDriveTypeW
    77 - ReadFileEx
    48 - GetFileSizeEx
    47 - GetFileSize
    55 - GetLogicalDriveStringsW
    43 - GetFileAttributesExA
    46 - GetFileInformationByHandle
[+] api-ms-win-core-file-l1-2-0.dll
    5 - CreateFile2
    71 - GetVolumePathNamesForVolumeNameW
    65 - GetTempPathW
[+] api-ms-win-core-file-l1-2-2.dll
    66 - GetVolumeInformationA
    64 - GetTempPathA
    22 - FindFirstStreamW
    20 - FindFirstFileNameW
    26 - FindNextFileNameW
    60 - GetTempFileNameA
[+] api-ms-win-core-file-l1-2-4.dll
    63 - GetTempPath2W
    62 - GetTempPath2A
[+] api-ms-win-core-file-l1-2-1.dll
    33 - GetCompressedFileSizeW
    32 - GetCompressedFileSizeA
    87 - SetFileIoOverlappedRange
[+] api-ms-win-core-delayload-l1-1-0.dll
    0 - DelayLoadFailureHook
[+] api-ms-win-core-io-l1-1-0.dll
    5 - GetOverlappedResult
    1 - CancelIoEx
    9 - PostQueuedCompletionStatus
    7 - GetQueuedCompletionStatus
    8 - GetQueuedCompletionStatusEx
    3 - CreateIoCompletionPort
    4 - DeviceIoControl
[+] api-ms-win-core-io-l1-1-1.dll
    0 - CancelIo
    2 - CancelSynchronousIo
[+] api-ms-win-core-job-l1-1-0.dll
    0 - IsProcessInJob
[+] api-ms-win-core-threadpool-legacy-l1-1-0.dll
    2 - CreateTimerQueueTimer
    4 - DeleteTimerQueueEx
    6 - QueueUserWorkItem
    0 - ChangeTimerQueueTimer
    7 - UnregisterWaitEx
    3 - DeleteTimerQueue
    1 - CreateTimerQueue
    5 - DeleteTimerQueueTimer
[+] api-ms-win-core-threadpool-private-l1-1-0.dll
    0 - RegisterWaitForSingleObjectEx
[+] api-ms-win-core-libraryloader-l1-2-3.dll
    4 - EnumResourceNamesA
[+] api-ms-win-core-libraryloader-l1-2-2.dll
    7 - EnumResourceNamesW
[+] api-ms-win-core-libraryloader-l1-2-0.dll
    18 - GetModuleHandleA
    15 - FreeResource
    27 - LoadResource
    17 - GetModuleFileNameW
    19 - GetModuleHandleExA
    5 - EnumResourceNamesExA
    1 - DisableThreadLibraryCalls
    30 - LockResource
    20 - GetModuleHandleExW
    9 - EnumResourceTypesExW
    33 - SizeofResource
    3 - EnumResourceLanguagesExW
    10 - FindResourceExW
    24 - LoadLibraryExA
    12 - FindStringOrdinal
    6 - EnumResourceNamesExW
    21 - GetModuleHandleW
    8 - EnumResourceTypesExA
    25 - LoadLibraryExW
    22 - GetProcAddress
    14 - FreeLibraryAndExitThread
    16 - GetModuleFileNameA
    2 - EnumResourceLanguagesExA
    13 - FreeLibrary
[+] api-ms-win-core-libraryloader-l1-2-1.dll
    11 - FindResourceW
    23 - LoadLibraryA
    26 - LoadLibraryW
[+] api-ms-win-core-libraryloader-l2-1-0.dll
    0 - LoadPackagedLibrary
[+] api-ms-win-core-namedpipe-l1-2-2.dll
    0 - CallNamedPipeW
[+] api-ms-win-core-namedpipe-l1-1-0.dll
    11 - TransactNamedPipe
    12 - WaitNamedPipeW
    1 - ConnectNamedPipe
    5 - GetNamedPipeClientComputerNameW
    3 - CreatePipe
    4 - DisconnectNamedPipe
    9 - PeekNamedPipe
    10 - SetNamedPipeHandleState
    2 - CreateNamedPipeW
[+] api-ms-win-core-namedpipe-l1-2-1.dll
    6 - GetNamedPipeHandleStateW
[+] api-ms-win-core-datetime-l1-1-1.dll
    1 - GetDateFormatEx
    5 - GetTimeFormatEx
[+] api-ms-win-core-datetime-l1-1-0.dll
    6 - GetTimeFormatW
    0 - GetDateFormatA
    4 - GetTimeFormatA
    2 - GetDateFormatW
[+] api-ms-win-core-datetime-l1-1-2.dll
    3 - GetDurationFormatEx
[+] api-ms-win-core-sysinfo-l1-2-0.dll
    16 - GetSystemFirmwareTable
    1 - EnumSystemFirmwareTables
    39 - SetComputerNameExW
    8 - GetNativeSystemInfo
    42 - SetSystemTime
    13 - GetProductInfo
    23 - GetSystemTimePreciseAsFileTime
[+] api-ms-win-core-sysinfo-l1-1-0.dll
    33 - GlobalMemoryStatusEx
    32 - GetWindowsDirectoryW
    5 - GetLocalTime
    31 - GetWindowsDirectoryA
    17 - GetSystemInfo
    29 - GetVersionExA
    6 - GetLogicalProcessorInformation
    28 - GetVersion
    3 - GetComputerNameExW
    26 - GetTickCount
    41 - SetLocalTime
    20 - GetSystemTimeAdjustment
    2 - GetComputerNameExA
    19 - GetSystemTime
    7 - GetLogicalProcessorInformationEx
    22 - GetSystemTimeAsFileTime
    30 - GetVersionExW
[+] api-ms-win-core-sysinfo-l1-2-1.dll
    11 - GetPhysicallyInstalledSystemMemory
    0 - DnsHostnameToComputerNameExW
    37 - SetComputerNameEx2W
[+] api-ms-win-core-sysinfo-l1-2-3.dll
    36 - SetComputerNameA
    40 - SetComputerNameW
    38 - SetComputerNameExA
[+] api-ms-win-core-timezone-l1-1-0.dll
    1 - FileTimeToSystemTime
    9 - SetTimeZoneInformation
    8 - SetDynamicTimeZoneInformation
    5 - GetTimeZoneInformationForYear
    11 - SystemTimeToTzSpecificLocalTime
    10 - SystemTimeToFileTime
    13 - TzSpecificLocalTimeToSystemTime
    4 - GetTimeZoneInformation
    2 - GetDynamicTimeZoneInformation
[+] api-ms-win-core-localization-l1-2-0.dll
    37 - IdnToUnicode
    55 - SetThreadPreferredUILanguages
    7 - FormatMessageA
    14 - GetFileMUIInfo
    26 - GetSystemPreferredUILanguages
    49 - LocaleNameToLCID
    42 - IsValidLanguageGroup
    20 - GetNLSVersion
    4 - EnumSystemLocalesW
    48 - LCMapStringW
    52 - SetLocaleInfoW
    12 - GetCalendarInfoEx
    13 - GetCalendarInfoW
    23 - GetProcessPreferredUILanguages
    21 - GetNLSVersionEx
    44 - IsValidLocaleName
    38 - IsDBCSLeadByte
    43 - IsValidLocale
    53 - SetProcessPreferredUILanguages
    59 - VerLanguageNameW
    31 - GetUserDefaultLCID
    58 - VerLanguageNameA
    11 - GetCPInfoExW
    45 - IsValidNLSVersion
    22 - GetOEMCP
    54 - SetThreadLocale
    24 - GetSystemDefaultLCID
    39 - IsDBCSLeadByteEx
    5 - FindNLSString
    10 - GetCPInfo
    46 - LCMapStringA
    32 - GetUserDefaultLangID
    27 - GetThreadLocale
    56 - SetThreadUILanguage
    18 - GetLocaleInfoEx
    28 - GetThreadPreferredUILanguages
    15 - GetFileMUIPath
    36 - IdnToAscii
    19 - GetLocaleInfoW
    40 - IsNLSDefinedString
    35 - GetUserPreferredUILanguages
    6 - FindNLSStringEx
    30 - GetUILanguageInfo
    17 - GetLocaleInfoA
    25 - GetSystemDefaultLangID
    9 - GetACP
    41 - IsValidCodePage
    2 - EnumSystemLocalesA
    50 - ResolveLocaleName
    8 - FormatMessageW
    47 - LCMapStringEx
    51 - SetCalendarInfoW
    0 - ConvertDefaultLocale
    29 - GetThreadUILanguage
[+] api-ms-win-core-localization-private-l1-1-0.dll
    4 - NlsUpdateLocale
    3 - NlsGetCacheUpdateCount
    2 - NlsCheckPolicy
    5 - NlsUpdateSystemLocale
[+] api-ms-win-core-processsnapshot-l1-1-0.dll
    9 - PssWalkSnapshot
    4 - PssWalkMarkerCreate
    7 - PssWalkMarkerSeekToBeginning
    2 - PssFreeSnapshot
    6 - PssWalkMarkerGetPosition
    5 - PssWalkMarkerFree
    8 - PssWalkMarkerSetPosition
    1 - PssDuplicateSnapshot
    3 - PssQuerySnapshot
    0 - PssCaptureSnapshot
[+] api-ms-win-core-processenvironment-l1-1-0.dll
    12 - GetStdHandle
    2 - FreeEnvironmentStringsA
    9 - GetEnvironmentStringsW
    20 - SetEnvironmentVariableA
    23 - SetStdHandleEx
    17 - SetCurrentDirectoryA
    5 - GetCommandLineW
    19 - SetEnvironmentStringsW
    7 - GetCurrentDirectoryW
    22 - SetStdHandle
    4 - GetCommandLineA
    18 - SetCurrentDirectoryW
    8 - GetEnvironmentStrings
    11 - GetEnvironmentVariableW
    3 - FreeEnvironmentStringsW
    10 - GetEnvironmentVariableA
    0 - ExpandEnvironmentStringsA
    6 - GetCurrentDirectoryA
    21 - SetEnvironmentVariableW
    1 - ExpandEnvironmentStringsW
    16 - SearchPathW
[+] api-ms-win-core-processenvironment-l1-2-0.dll
    15 - SearchPathA
    14 - NeedCurrentDirectoryForExePathW
    13 - NeedCurrentDirectoryForExePathA
[+] api-ms-win-core-string-l1-1-0.dll
    5 - GetStringTypeW
    7 - WideCharToMultiByte
    0 - CompareStringEx
    6 - MultiByteToWideChar
    3 - FoldStringW
    1 - CompareStringOrdinal
    4 - GetStringTypeExW
    2 - CompareStringW
[+] api-ms-win-core-debug-l1-1-0.dll
    5 - IsDebuggerPresent
    7 - OutputDebugStringW
    4 - DebugBreak
    6 - OutputDebugStringA
[+] api-ms-win-core-debug-l1-1-1.dll
    8 - WaitForDebugEvent
    0 - CheckRemoteDebuggerPresent
    3 - DebugActiveProcessStop
    1 - ContinueDebugEvent
    2 - DebugActiveProcess
[+] api-ms-win-core-errorhandling-l1-1-0.dll
    15 - SetUnhandledExceptionFilter
    7 - RaiseException
    12 - SetErrorMode
    5 - GetLastError
    17 - UnhandledExceptionFilter
    4 - GetErrorMode
    13 - SetLastError
[+] api-ms-win-core-errorhandling-l1-1-3.dll
    14 - SetThreadErrorMode
    6 - GetThreadErrorMode
[+] api-ms-win-core-fibers-l1-1-0.dll
    1 - FlsFree
    2 - FlsGetValue
    0 - FlsAlloc
    3 - FlsSetValue
[+] api-ms-win-core-util-l1-1-0.dll
    0 - Beep
[+] api-ms-win-core-profile-l1-1-0.dll
    0 - QueryPerformanceCounter
    1 - QueryPerformanceFrequency
[+] api-ms-win-security-base-l1-1-0.dll
    66 - GetTokenInformation
    35 - CreateWellKnownSid
    44 - EqualSid
    0 - AccessCheck
    22 - AllocateAndInitializeSid
    46 - FreeSid
    40 - DuplicateToken
[+] api-ms-win-security-base-l1-2-0.dll
    28 - CheckTokenMembershipEx
    18 - AddResourceAttributeAce
    49 - GetAppContainerAce
    26 - CheckTokenCapability
    19 - AddScopedPolicyIDAce
    91 - SetCachedSigningLevel
    50 - GetCachedSigningLevel
[+] api-ms-win-core-comm-l1-1-0.dll
    5 - GetCommModemStatus
    0 - ClearCommBreak
    19 - WaitCommEvent
    7 - GetCommProperties
    3 - GetCommConfig
    2 - EscapeCommFunction
    1 - ClearCommError
    8 - GetCommState
    9 - GetCommTimeouts
    11 - PurgeComm
    12 - SetCommBreak
    13 - SetCommConfig
    14 - SetCommMask
    15 - SetCommState
    16 - SetCommTimeouts
    17 - SetupComm
    4 - GetCommMask
    18 - TransmitCommChar
[+] api-ms-win-core-wow64-l1-1-1.dll
    2 - GetSystemWow64DirectoryA
    3 - GetSystemWow64DirectoryW
    6 - IsWow64Process2
    1 - GetSystemWow64Directory2W
[+] api-ms-win-core-wow64-l1-1-0.dll
    10 - Wow64RevertWow64FsRedirection
    7 - Wow64DisableWow64FsRedirection
    5 - IsWow64Process
    8 - Wow64EnableWow64FsRedirection
[+] api-ms-win-core-wow64-l1-1-3.dll
    13 - Wow64SuspendThread
    9 - Wow64GetThreadContext
    11 - Wow64SetThreadContext
[+] api-ms-win-core-realtime-l1-1-0.dll
    4 - QueryIdleProcessorCycleTimeEx
    7 - QueryProcessCycleTime
    8 - QueryThreadCycleTime
    9 - QueryUnbiasedInterruptTime
    3 - QueryIdleProcessorCycleTime
[+] api-ms-win-core-systemtopology-l1-1-1.dll
    3 - GetNumaProximityNodeEx
[+] api-ms-win-core-systemtopology-l1-1-0.dll
    0 - GetNumaHighestNodeNumber
    2 - GetNumaNodeProcessorMaskEx
[+] api-ms-win-core-processtopology-l1-1-0.dll
    1 - GetThreadGroupAffinity
    2 - SetThreadGroupAffinity
    0 - GetProcessGroupAffinity
[+] api-ms-win-core-namespace-l1-1-0.dll
    4 - DeleteBoundaryDescriptor
    5 - OpenPrivateNamespaceW
    3 - CreatePrivateNamespaceW
    0 - AddSIDToBoundaryDescriptor
    1 - ClosePrivateNamespace
    2 - CreateBoundaryDescriptorW
[+] api-ms-win-core-file-l2-1-2.dll
    4 - CreateHardLinkA
    2 - CopyFileW
[+] api-ms-win-core-file-l2-1-0.dll
    11 - ReOpenFile
    8 - MoveFileExW
    9 - MoveFileWithProgressW
    14 - ReplaceFileW
    7 - GetFileInformationByHandleEx
    5 - CreateHardLinkW
    0 - CopyFile2
    6 - CreateSymbolicLinkW
    1 - CopyFileExW
    3 - CreateDirectoryExW
    13 - ReadDirectoryChangesW
[+] api-ms-win-core-file-l2-1-1.dll
    10 - OpenFileById
[+] api-ms-win-core-file-l2-1-3.dll
    12 - ReadDirectoryChangesExW
[+] api-ms-win-core-xstate-l2-1-0.dll
    0 - CopyContext
    7 - LocateXStateFeature
    2 - GetEnabledXStateFeatures
    8 - SetXStateFeaturesMask
    5 - InitializeContext
    4 - GetXStateFeaturesMask
[+] api-ms-win-core-xstate-l2-1-1.dll
    6 - InitializeContext2
[+] api-ms-win-core-xstate-l2-1-2.dll
    3 - GetThreadEnabledXStateFeatures
    1 - EnableProcessOptionalXStateFeatures
[+] api-ms-win-core-localization-l2-1-0.dll
    4 - EnumDateFormatsExW
    3 - EnumDateFormatsExEx
    9 - GetCurrencyFormatEx
    6 - EnumSystemCodePagesW
    11 - GetNumberFormatEx
    1 - EnumCalendarInfoExW
    2 - EnumCalendarInfoW
    7 - EnumTimeFormatsEx
    8 - EnumTimeFormatsW
    5 - EnumDateFormatsW
    0 - EnumCalendarInfoExEx
[+] api-ms-win-core-normalization-l1-1-0.dll
    0 - GetStringScripts
    2 - IsNormalizedString
    4 - VerifyScripts
    3 - NormalizeString
    1 - IdnToNameprepUnicode
[+] api-ms-win-core-fibers-l2-1-0.dll
    6 - SwitchToFiber
    0 - ConvertFiberToThread
    1 - ConvertThreadToFiber
    5 - DeleteFiber
    3 - CreateFiber
[+] api-ms-win-core-fibers-l2-1-1.dll
    4 - CreateFiberEx
    2 - ConvertThreadToFiberEx
[+] api-ms-win-core-sidebyside-l1-1-0.dll
    9 - ReleaseActCtx
    7 - QueryActCtxSettingsW
    6 - GetCurrentActCtx
    10 - ZombifyActCtx
    1 - AddRefActCtx
    8 - QueryActCtxW
    3 - DeactivateActCtx
    5 - FindActCtxSectionStringW
    4 - FindActCtxSectionGuid
    0 - ActivateActCtx
    2 - CreateActCtxW
[+] api-ms-win-core-appcompat-l1-1-0.dll
    2 - BaseCleanupAppcompatCacheSupport
    3 - BaseDumpAppcompatCache
    4 - BaseFlushAppcompatCache
    0 - BaseCheckAppcompatCache
    6 - BaseInitAppcompatCacheSupport
    9 - BaseUpdateAppcompatCache
    1 - BaseCheckAppcompatCacheEx
[+] api-ms-win-core-appcompat-l1-1-1.dll
    5 - BaseFreeAppCompatDataForProcess
    8 - BaseReadAppCompatDataForProcess
[+] api-ms-win-core-windowserrorreporting-l1-1-1.dll
    7 - WerRegisterCustomMetadata
    8 - WerRegisterExcludedMemoryBlock
    14 - WerUnregisterAdditionalProcess
    16 - WerUnregisterCustomMetadata
    17 - WerUnregisterExcludedMemoryBlock
    5 - WerRegisterAdditionalProcess
[+] api-ms-win-core-windowserrorreporting-l1-1-2.dll
    6 - WerRegisterAppLocalDump
    15 - WerUnregisterAppLocalDump
[+] api-ms-win-core-windowserrorreporting-l1-1-0.dll
    1 - GetApplicationRestartSettings
    11 - WerRegisterRuntimeExceptionModule
    9 - WerRegisterFile
    10 - WerRegisterMemoryBlock
    18 - WerUnregisterFile
    19 - WerUnregisterMemoryBlock
    0 - GetApplicationRecoveryCallback
    20 - WerUnregisterRuntimeExceptionModule
[+] api-ms-win-core-windowserrorreporting-l1-1-3.dll
    2 - RegisterApplicationRestart
    3 - UnregisterApplicationRestart
[+] api-ms-win-core-console-l1-1-0.dll
    13 - ReadConsoleInputW
    12 - ReadConsoleInputA
    11 - ReadConsoleA
    19 - WriteConsoleW
    18 - WriteConsoleA
    8 - GetNumberOfConsoleInputEvents
    7 - GetConsoleOutputCP
    6 - GetConsoleMode
    5 - GetConsoleCP
    17 - SetConsoleMode
    16 - SetConsoleCtrlHandler
    0 - AllocConsole
    14 - ReadConsoleW
[+] api-ms-win-core-console-l1-2-0.dll
    1 - AttachConsole
    10 - PeekConsoleInputW
    4 - FreeConsole
    9 - PeekConsoleInputA
[+] api-ms-win-core-console-l1-2-1.dll
    15 - ResizePseudoConsole
    2 - ClosePseudoConsole
    3 - CreatePseudoConsole
[+] api-ms-win-core-console-l2-1-0.dll
    2 - FillConsoleOutputCharacterA
    1 - FillConsoleOutputAttribute
    0 - CreateConsoleScreenBuffer
    33 - WriteConsoleInputW
    32 - WriteConsoleInputA
    31 - SetConsoleWindowInfo
    28 - SetConsoleTextAttribute
    27 - SetConsoleScreenBufferSize
    26 - SetConsoleScreenBufferInfoEx
    25 - SetConsoleOutputCP
    24 - SetConsoleCursorPosition
    23 - SetConsoleCursorInfo
    22 - SetConsoleCP
    21 - SetConsoleActiveScreenBuffer
    20 - ScrollConsoleScreenBufferW
    19 - ScrollConsoleScreenBufferA
    18 - ReadConsoleOutputW
    17 - ReadConsoleOutputCharacterW
    16 - ReadConsoleOutputCharacterA
    15 - ReadConsoleOutputAttribute
    14 - ReadConsoleOutputA
    13 - GetLargestConsoleWindowSize
    10 - GetConsoleScreenBufferInfoEx
    9 - GetConsoleScreenBufferInfo
    34 - WriteConsoleOutputA
    6 - GetConsoleCursorInfo
    5 - GenerateConsoleCtrlEvent
    4 - FlushConsoleInputBuffer
    3 - FillConsoleOutputCharacterW
    35 - WriteConsoleOutputAttribute
    36 - WriteConsoleOutputCharacterA
    37 - WriteConsoleOutputCharacterW
    38 - WriteConsoleOutputW
[+] api-ms-win-core-console-l2-2-0.dll
    11 - GetConsoleTitleA
    12 - GetConsoleTitleW
    30 - SetConsoleTitleW
    8 - GetConsoleOriginalTitleW
    7 - GetConsoleOriginalTitleA
    29 - SetConsoleTitleA
[+] api-ms-win-core-console-l3-2-0.dll
    6 - GetConsoleAliasExesLengthA
    7 - GetConsoleAliasExesLengthW
    8 - GetConsoleAliasExesW
    5 - GetConsoleAliasExesA
    3 - ExpungeConsoleCommandHistoryW
    2 - ExpungeConsoleCommandHistoryA
    11 - GetConsoleAliasesLengthA
    12 - GetConsoleAliasesLengthW
    9 - GetConsoleAliasW
    0 - AddConsoleAliasA
    13 - GetConsoleAliasesW
    14 - GetConsoleCommandHistoryA
    15 - GetConsoleCommandHistoryLengthA
    16 - GetConsoleCommandHistoryLengthW
    17 - GetConsoleCommandHistoryW
    18 - GetConsoleDisplayMode
    19 - GetConsoleFontSize
    20 - GetConsoleHistoryInfo
    21 - GetConsoleProcessList
    22 - GetConsoleSelectionInfo
    23 - GetConsoleWindow
    24 - GetCurrentConsoleFont
    25 - GetCurrentConsoleFontEx
    26 - GetNumberOfConsoleMouseButtons
    27 - SetConsoleDisplayMode
    28 - SetConsoleHistoryInfo
    4 - GetConsoleAliasA
    1 - AddConsoleAliasW
    29 - SetConsoleNumberOfCommandsA
    30 - SetConsoleNumberOfCommandsW
    31 - SetCurrentConsoleFontEx
    10 - GetConsoleAliasesA
[+] api-ms-win-core-psapi-l1-1-0.dll
    19 - K32QueryWorkingSetEx
    17 - K32InitializeProcessForWsWatch
    15 - K32GetWsChanges
    18 - K32QueryWorkingSet
    8 - K32GetMappedFileNameW
    3 - K32EnumProcessModules
    20 - QueryFullProcessImageNameW
    2 - K32EnumPageFilesW
    12 - K32GetPerformanceInfo
    5 - K32EnumProcesses
    11 - K32GetModuleInformation
    6 - K32GetDeviceDriverBaseNameW
    14 - K32GetProcessMemoryInfo
    9 - K32GetModuleBaseNameW
    4 - K32EnumProcessModulesEx
    13 - K32GetProcessImageFileNameW
    0 - K32EmptyWorkingSet
    16 - K32GetWsChangesEx
    10 - K32GetModuleFileNameExW
    7 - K32GetDeviceDriverFileNameW
    1 - K32EnumDeviceDrivers
[+] api-ms-win-core-psapi-ansi-l1-1-0.dll
    6 - K32GetProcessImageFileNameA
    1 - K32GetDeviceDriverBaseNameA
    2 - K32GetDeviceDriverFileNameA
    5 - K32GetModuleFileNameExA
    7 - QueryFullProcessImageNameA
    4 - K32GetModuleBaseNameA
    0 - K32EnumPageFilesA
    3 - K32GetMappedFileNameA
[+] api-ms-win-security-appcontainer-l1-1-0.dll
    0 - GetAppContainerNamedObjectPath
[+] api-ms-win-eventing-provider-l1-1-0.dll
    4 - EventSetInformation
    3 - EventRegister
    9 - EventWriteTransfer
    5 - EventUnregister
[+] api-ms-win-core-delayload-l1-1-1.dll
    1 - ResolveDelayLoadedAPI

PS Z:\win\le-format-pe\Debug>
```

## La structure IMAGE_EXPORT_DIRECTORY

Et pour finir, l'inverse des fonctions importées : les fonctions exportées.
Ce sont les fonctions contenues dans les dll qui peuvent être appelées par tout autre programme ou dll.
Comme pour les fonctions importées, on y accède grâce au tableau **IMAGE_DATA_DIRECTORY** obtenu depuis le champ **DataDirectory** de la structure **IMAGE_OPTIONAL_HEADER**.

Cette fois-ci, on tombe sur la structure de données qui nous intéresse,
c'est à dire: **IMAGE_EXPORT_DIRECTORY** en utilisant la constante **IMAGE_DIRECTORY_ENTRY_EXPORT** comme index pour le tableau **IMAGE_DATA_DIRECTORY**.

Le nombre de fonctions exportées par leur nom est contenu dans le champ **NumberOfNames**, les **RVA** des noms dans **AddressOfNames** et les ordinaux dans **AddressOfNameOrdinals**.
Ces deux champs doivent être parcourus en parallèle. Les adresses des fonctions sont dans **AddressOfFunctions**.

Quand un nom est trouvé, l'ordinal est utilisé comme index dans le tableau **AddressOfFunctions**.
Si la fonction est forwardée, l'adresse correspond à une **RVA** vers la string indiquant d'où elle est forwardée.

Le code : {{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/-/blob/main/ListExportedFunctions/ListExportedFunctions.c" title="ListExportedFunctions.c" >}}

```txt
PS Z:\win\le-format-pe\Debug> .\ListExportedFunctions.exe C:\Windows\System32\kernel32.dll
NumberOfNames: 1629

ordinal / index -- name -- address

4 -- AcquireSRWLockExclusive -- 000968CE (forwarder -> NTDLL.RtlAcquireSRWLockExclusive)
5 -- AcquireSRWLockShared -- 00096904 (forwarder -> NTDLL.RtlAcquireSRWLockShared)
6 -- ActivateActCtx -- 00021AC0
7 -- ActivateActCtxWorker -- 00016E50
8 -- ActivatePackageVirtualizationContext -- 000248F0
9 -- AddAtomA -- 00020190
10 -- AddAtomW -- 00013B60
11 -- AddConsoleAliasA -- 000245B0
12 -- AddConsoleAliasW -- 000245C0
13 -- AddDllDirectory -- 000969AF (forwarder -> api-ms-win-core-libraryloader-l1-1-0.AddDllDirectory)
14 -- AddIntegrityLabelToBoundaryDescriptor -- 00039240
15 -- AddLocalAlternateComputerNameA -- 000548D0
16 -- AddLocalAlternateComputerNameW -- 00054930
17 -- AddRefActCtx -- 000369B0
18 -- AddRefActCtxWorker -- 0001F250
19 -- AddResourceAttributeAce -- 000369C0
20 -- AddSIDToBoundaryDescriptor -- 0001A0E0
21 -- AddScopedPolicyIDAce -- 000369E0
22 -- AddSecureMemoryCacheCallback -- 00034A30
23 -- AddVectoredContinueHandler -- 00096AE8 (forwarder -> NTDLL.RtlAddVectoredContinueHandler)
24 -- AddVectoredExceptionHandler -- 00096B28 (forwarder -> NTDLL.RtlAddVectoredExceptionHandler)
25 -- AdjustCalendarDate -- 00046340
26 -- AllocConsole -- 00024200
27 -- AllocateUserPhysicalPages -- 00036A20
28 -- AllocateUserPhysicalPagesNuma -- 00036A00
29 -- AppPolicyGetClrCompat -- 00096BBB (forwarder -> kernelbase.AppPolicyGetClrCompat)
30 -- AppPolicyGetCreateFileAccess -- 00096BF9 (forwarder -> kernelbase.AppPolicyGetCreateFileAccess)
31 -- AppPolicyGetLifecycleManagement -- 00096C41 (forwarder -> kernelbase.AppPolicyGetLifecycleManagement)
32 -- AppPolicyGetMediaFoundationCodecLoading -- 00096C94 (forwarder -> kernelbase.AppPolicyGetMediaFoundationCodecLoading)
33 -- AppPolicyGetProcessTerminationMethod -- 00096CEC (forwarder -> kernelbase.AppPolicyGetProcessTerminationMethod)
34 -- AppPolicyGetShowDeveloperDiagnostic -- 00096D40 (forwarder -> kernelbase.AppPolicyGetShowDeveloperDiagnostic)
35 -- AppPolicyGetThreadInitializationType -- 00096D94 (forwarder -> kernelbase.AppPolicyGetThreadInitializationType)
36 -- AppPolicyGetWindowingModel -- 00096DDF (forwarder -> kernelbase.AppPolicyGetWindowingModel)
37 -- AppXGetOSMaxVersionTested -- 00096E1F (forwarder -> kernelbase.AppXGetOSMaxVersionTested)
38 -- ApplicationRecoveryFinished -- 0003EB30
39 -- ApplicationRecoveryInProgress -- 0003EB50
40 -- AreFileApisANSI -- 00022A80
41 -- AreShortNamesEnabled -- 000266C0
42 -- AssignProcessToJobObject -- 00055E30
43 -- AttachConsole -- 00024210
44 -- BackupRead -- 00056C80
45 -- BackupSeek -- 00057B80
46 -- BackupWrite -- 00057E10
47 -- BaseCheckAppcompatCache -- 00036A60
48 -- BaseCheckAppcompatCacheEx -- 00036A40
49 -- BaseCheckAppcompatCacheExWorker -- 00044C40
50 -- BaseCheckAppcompatCacheWorker -- 00044C50
51 -- BaseCheckElevation -- 0001D540
52 -- BaseCleanupAppcompatCacheSupport -- 00036A80
53 -- BaseCleanupAppcompatCacheSupportWorker -- 00044C60
54 -- BaseDestroyVDMEnvironment -- 0003BD20
55 -- BaseDllReadWriteIniFile -- 00020A50
56 -- BaseDumpAppcompatCache -- 00036AA0
57 -- BaseDumpAppcompatCacheWorker -- 000235C0
58 -- BaseElevationPostProcessing -- 00019ED0
59 -- BaseFlushAppcompatCache -- 00036AB0
60 -- BaseFlushAppcompatCacheWorker -- 00044C70
61 -- BaseFormatObjectAttributes -- 00023790
62 -- BaseFormatTimeOut -- 00054240
63 -- BaseFreeAppCompatDataForProcessWorker -- 00020320
64 -- BaseGenerateAppCompatData -- 00019F20
65 -- BaseGetNamedObjectDirectory -- 00036AC0
66 -- BaseInitAppcompatCacheSupport -- 00036AE0
67 -- BaseInitAppcompatCacheSupportWorker -- 0001A1C0
68 -- BaseIsAppcompatInfrastructureDisabled -- 00022A40
69 -- BaseIsAppcompatInfrastructureDisabledWorker -- 00022A40
70 -- BaseIsDosApplication -- 0005A490
71 -- BaseQueryModuleData -- 00045550
72 -- BaseReadAppCompatDataForProcessWorker -- 00044CD0
73 -- BaseSetLastNTError -- 00014310
1 -- BaseThreadInitThunk -- 00016720
74 -- BaseUpdateAppcompatCache -- 00036AF0
75 -- BaseUpdateAppcompatCacheWorker -- 00044F80
76 -- BaseUpdateVDMEntry -- 0003BF90
77 -- BaseVerifyUnicodeString -- 00054490
78 -- BaseWriteErrorElevationRequiredEvent -- 00058F50
79 -- Basep8BitStringToDynamicUnicodeString -- 00021800
80 -- BasepAllocateActivationContextActivationBlock -- 000544F0
81 -- BasepAnsiStringToDynamicUnicodeString -- 00054430
82 -- BasepAppContainerEnvironmentExtension -- 00010290
83 -- BasepAppXExtension -- 00035C70
84 -- BasepCheckAppCompat -- 0001D4C0
85 -- BasepCheckWebBladeHashes -- 00019B30
86 -- BasepCheckWinSaferRestrictions -- 00018CF0
87 -- BasepConstructSxsCreateProcessMessage -- 00011D00
88 -- BasepCopyEncryption -- 00034680
89 -- BasepFinishPackageActivation -- 00024B00
90 -- BasepFinishPackageActivationForSxS -- 00024C40
91 -- BasepFreeActivationContextActivationBlock -- 000545F0
92 -- BasepFreeAppCompatData -- 000198F0
93 -- BasepGetAppCompatData -- 00024C90
94 -- BasepGetComputerNameFromNtPath -- 00021EF0
95 -- BasepGetExeArchType -- 0001DBB0
96 -- BasepGetPackageActivationTokenForFilePath -- 00024B30
97 -- BasepGetPackageActivationTokenForSxS -- 00024C70
98 -- BasepGetPackagedAppInfoForFile -- 00024B50
99 -- BasepInitAppCompatData -- 00045340
100 -- BasepIsProcessAllowed -- 000196D0
101 -- BasepMapModuleHandle -- 00054630
102 -- BasepNotifyLoadStringResource -- 00018420
103 -- BasepPostSuccessAppXExtension -- 00035CC0
104 -- BasepProcessInvalidImage -- 00035CE0
105 -- BasepQueryAppCompat -- 00017B90
106 -- BasepQueryModuleChpeSettings -- 000453B0
107 -- BasepReleaseAppXContext -- 000362D0
108 -- BasepReleasePackagedAppInfo -- 00024B70
109 -- BasepReleaseSxsCreateProcessUtilityStruct -- 000120E0
110 -- BasepReportFault -- 0003EDD0
111 -- BasepSetFileEncryptionCompression -- 000226D0
112 -- Beep -- 00033DB0
113 -- BeginUpdateResourceA -- 00044490
114 -- BeginUpdateResourceW -- 000444E0
115 -- BindIoCompletionCallback -- 0005A890
116 -- BuildCommDCBA -- 0003D420
117 -- BuildCommDCBAndTimeoutsA -- 0003D470
118 -- BuildCommDCBAndTimeoutsW -- 0003D4A0
119 -- BuildCommDCBW -- 0003D510
120 -- CallNamedPipeA -- 0005A8C0
121 -- CallNamedPipeW -- 00023F90
122 -- CallbackMayRunLong -- 00036B10
123 -- CancelDeviceWakeupRequest -- 00035C30
124 -- CancelIo -- 00036B40
125 -- CancelIoEx -- 0001FA70
126 -- CancelSynchronousIo -- 00036B60
127 -- CancelThreadpoolIo -- 0009775C (forwarder -> NTDLL.TpCancelAsyncIoOperation)
128 -- CancelTimerQueueTimer -- 0003ED60
129 -- CancelWaitableTimer -- 00023870
130 -- CeipIsOptedIn -- 000977B3 (forwarder -> kernelbase.CeipIsOptedIn)
131 -- ChangeTimerQueueTimer -- 00036B80
132 -- CheckAllowDecryptedRemoteDestinationPolicy -- 00036BA0
133 -- CheckElevation -- 0001D3D0
134 -- CheckElevationEnabled -- 0001F9B0
135 -- CheckForReadOnlyResource -- 0005ABC0
136 -- CheckForReadOnlyResourceFilter -- 00039270
137 -- CheckNameLegalDOS8Dot3A -- 00035AC0
138 -- CheckNameLegalDOS8Dot3W -- 00035B30
139 -- CheckRemoteDebuggerPresent -- 00036BB0
140 -- CheckTokenCapability -- 00036BD0
141 -- CheckTokenMembershipEx -- 00036BF0
142 -- ClearCommBreak -- 00024030
143 -- ClearCommError -- 00024040
144 -- CloseConsoleHandle -- 00062520
145 -- CloseHandle -- 00023830
146 -- ClosePackageInfo -- 0009792F (forwarder -> kernelbase.ClosePackageInfo)
147 -- ClosePrivateNamespace -- 000204B0
148 -- CloseProfileUserMapping -- 0001A1C0
149 -- ClosePseudoConsole -- 00024220
150 -- CloseState -- 00097997 (forwarder -> kernelbase.CloseState)
151 -- CloseThreadpool -- 000979BD (forwarder -> NTDLL.TpReleasePool)
152 -- CloseThreadpoolCleanupGroup -- 000979ED (forwarder -> NTDLL.TpReleaseCleanupGroup)
153 -- CloseThreadpoolCleanupGroupMembers -- 00097A2C (forwarder -> NTDLL.TpReleaseCleanupGroupMembers)
154 -- CloseThreadpoolIo -- 00097A61 (forwarder -> NTDLL.TpReleaseIoCompletion)
155 -- CloseThreadpoolTimer -- 00097A92 (forwarder -> NTDLL.TpReleaseTimer)
156 -- CloseThreadpoolWait -- 00097ABB (forwarder -> NTDLL.TpReleaseWait)
157 -- CloseThreadpoolWork -- 00097AE3 (forwarder -> NTDLL.TpReleaseWork)
158 -- CmdBatNotification -- 000357D0
159 -- CommConfigDialogA -- 00039C70
160 -- CommConfigDialogW -- 00039D00
161 -- CompareCalendarDates -- 00046630
162 -- CompareFileTime -- 00023A40
163 -- CompareStringA -- 0001E8D0
164 -- CompareStringEx -- 000201B0
165 -- CompareStringOrdinal -- 000178D0
166 -- CompareStringW -- 00014570
167 -- ConnectNamedPipe -- 00022C00
168 -- ConsoleMenuControl -- 000625E0
169 -- ContinueDebugEvent -- 00036C10
170 -- ConvertCalDateTimeToSystemTime -- 000466C0
171 -- ConvertDefaultLocale -- 00036C30
172 -- ConvertFiberToThread -- 00024190
173 -- ConvertNLSDayOfWeekToWin32DayOfWeek -- 00046780
174 -- ConvertSystemTimeToCalDateTime -- 000467B0
175 -- ConvertThreadToFiber -- 000241A0
176 -- ConvertThreadToFiberEx -- 000241B0
177 -- CopyContext -- 00036C50
178 -- CopyFile2 -- 00036C70
179 -- CopyFileA -- 0001F620
180 -- CopyFileExA -- 0005AE80
181 -- CopyFileExW -- 00036C90
182 -- CopyFileTransactedA -- 0005AEF0
183 -- CopyFileTransactedW -- 0005AF90
184 -- CopyFileW -- 00024180
185 -- CopyLZFile -- 00033EB0
186 -- CreateActCtxA -- 0005B600
187 -- CreateActCtxW -- 00022970
188 -- CreateActCtxWWorker -- 00012140
189 -- CreateBoundaryDescriptorA -- 0005AD30
190 -- CreateBoundaryDescriptorW -- 0001A070
191 -- CreateConsoleScreenBuffer -- 00024340
192 -- CreateDirectoryA -- 00023A50
193 -- CreateDirectoryExA -- 0005B940
194 -- CreateDirectoryExW -- 00036CB0
195 -- CreateDirectoryTransactedA -- 00034110
196 -- CreateDirectoryTransactedW -- 0005B9A0
197 -- CreateDirectoryW -- 00023A60
198 -- CreateEnclave -- 00097E04 (forwarder -> api-ms-win-core-enclave-l1-1-0.CreateEnclave)
199 -- CreateEventA -- 00023880
200 -- CreateEventExA -- 00023890
201 -- CreateEventExW -- 000238A0
202 -- CreateEventW -- 000238B0
203 -- CreateFiber -- 000241C0
204 -- CreateFiberEx -- 000241D0
205 -- CreateFile2 -- 00023A70
206 -- CreateFileA -- 00023A80
207 -- CreateFileMappingA -- 00016D80
208 -- CreateFileMappingFromApp -- 00097EC7 (forwarder -> api-ms-win-core-memory-l1-1-1.CreateFileMappingFromApp)
209 -- CreateFileMappingNumaA -- 0005BAD0
210 -- CreateFileMappingNumaW -- 00036CD0
211 -- CreateFileMappingW -- 000188A0
212 -- CreateFileTransactedA -- 0005B030
213 -- CreateFileTransactedW -- 0005B090
214 -- CreateFileW -- 00023A90
215 -- CreateHardLinkA -- 00036CF0
216 -- CreateHardLinkTransactedA -- 0003E420
217 -- CreateHardLinkTransactedW -- 0005BB30
218 -- CreateHardLinkW -- 00036D10
219 -- CreateIoCompletionPort -- 00022C60
220 -- CreateJobObjectA -- 000561F0
221 -- CreateJobObjectW -- 00056240
222 -- CreateJobSet -- 000562C0
223 -- CreateMailslotA -- 0005BBC0
224 -- CreateMailslotW -- 0005BC10
225 -- CreateMemoryResourceNotification -- 0001A120
226 -- CreateMutexA -- 000238C0
227 -- CreateMutexExA -- 000238D0
228 -- CreateMutexExW -- 000238E0
229 -- CreateMutexW -- 000238F0
230 -- CreateNamedPipeA -- 00022B10
231 -- CreateNamedPipeW -- 00036D30
232 -- CreatePackageVirtualizationContext -- 00024920
233 -- CreatePipe -- 000107C0
234 -- CreatePrivateNamespaceA -- 0005AD90
235 -- CreatePrivateNamespaceW -- 0001A050
236 -- CreateProcessA -- 00036D50
237 -- CreateProcessAsUserA -- 00036D70
238 -- CreateProcessAsUserW -- 00036D90
239 -- CreateProcessInternalA -- 00036DB0
240 -- CreateProcessInternalW -- 00036DD0
241 -- CreateProcessW -- 00019AA0
242 -- CreatePseudoConsole -- 00024230
243 -- CreateRemoteThread -- 00036DF0
244 -- CreateRemoteThreadEx -- 000981BC (forwarder -> api-ms-win-core-processthreads-l1-1-0.CreateRemoteThreadEx)
245 -- CreateSemaphoreA -- 0001DE60
246 -- CreateSemaphoreExA -- 0001DE90
247 -- CreateSemaphoreExW -- 00023900
248 -- CreateSemaphoreW -- 00023910
249 -- CreateSocketHandle -- 0003E4A0
250 -- CreateSymbolicLinkA -- 0005C060
251 -- CreateSymbolicLinkTransactedA -- 0005C0E0
252 -- CreateSymbolicLinkTransactedW -- 0005C170
253 -- CreateSymbolicLinkW -- 00036E30
254 -- CreateTapePartition -- 0003E1B0
255 -- CreateThread -- 00019390
256 -- CreateThreadpool -- 00036EA0
257 -- CreateThreadpoolCleanupGroup -- 00036E50
258 -- CreateThreadpoolIo -- 00036E80
259 -- CreateThreadpoolTimer -- 00019FC0
260 -- CreateThreadpoolWait -- 000195B0
261 -- CreateThreadpoolWork -- 00019C10
262 -- CreateTimerQueue -- 00036ED0
263 -- CreateTimerQueueTimer -- 00036EE0
264 -- CreateToolhelp32Snapshot -- 00028650
265 -- CreateWaitableTimerA -- 0001E8F0
266 -- CreateWaitableTimerExA -- 0001E920
267 -- CreateWaitableTimerExW -- 00023920
268 -- CreateWaitableTimerW -- 0005BE50
269 -- CtrlRoutine -- 000983FC (forwarder -> kernelbase.CtrlRoutine)
270 -- DeactivateActCtx -- 00021AA0
271 -- DeactivateActCtxWorker -- 00016E20
272 -- DeactivatePackageVirtualizationContext -- 00024940
273 -- DebugActiveProcess -- 00036F20
274 -- DebugActiveProcessStop -- 00036F00
275 -- DebugBreak -- 00036F40
276 -- DebugBreakProcess -- 00034070
277 -- DebugSetProcessKillOnExit -- 000340A0
278 -- DecodePointer -- 000984D1 (forwarder -> NTDLL.RtlDecodePointer)
279 -- DecodeSystemPointer -- 000984FC (forwarder -> NTDLL.RtlDecodeSystemPointer)
280 -- DefineDosDeviceA -- 0005D510
281 -- DefineDosDeviceW -- 00023AA0
282 -- DelayLoadFailureHook -- 00036F50
283 -- DeleteAtom -- 0001D290
284 -- DeleteBoundaryDescriptor -- 0001A0D0
285 -- DeleteCriticalSection -- 0009858A (forwarder -> NTDLL.RtlDeleteCriticalSection)
286 -- DeleteFiber -- 000241E0
287 -- DeleteFileA -- 00023AB0
288 -- DeleteFileTransactedA -- 0005C200
289 -- DeleteFileTransactedW -- 0005C240
290 -- DeleteFileW -- 00023AC0
291 -- DeleteProcThreadAttributeList -- 00098617 (forwarder -> api-ms-win-core-processthreads-l1-1-0.DeleteProcThreadAttributeList)
292 -- DeleteSynchronizationBarrier -- 00036F70
293 -- DeleteTimerQueue -- 00023F50
294 -- DeleteTimerQueueEx -- 00036F90
295 -- DeleteTimerQueueTimer -- 00036FB0
296 -- DeleteVolumeMountPointA -- 0005D750
297 -- DeleteVolumeMountPointW -- 00023AD0
298 -- DeviceIoControl -- 000179E0
299 -- DisableThreadLibraryCalls -- 00019A70
300 -- DisableThreadProfiling -- 0003EE00
301 -- DisassociateCurrentThreadFromCallback -- 00098749 (forwarder -> NTDLL.TpDisassociateCallback)
302 -- DiscardVirtualMemory -- 0009877B (forwarder -> api-ms-win-core-memory-l1-1-2.DiscardVirtualMemory)
303 -- DisconnectNamedPipe -- 00036FD0
304 -- DnsHostnameToComputerNameA -- 000554D0
305 -- DnsHostnameToComputerNameExW -- 00036FF0
306 -- DnsHostnameToComputerNameW -- 00055590
307 -- DosDateTimeToFileTime -- 00021670
308 -- DosPathToSessionPathA -- 0005EEC0
309 -- DosPathToSessionPathW -- 0005F040
310 -- DuplicateConsoleHandle -- 00062540
311 -- DuplicateEncryptionInfoFileExt -- 000348F0
312 -- DuplicateHandle -- 00023840
313 -- DuplicatePackageVirtualizationContext -- 00024960
314 -- EnableProcessOptionalXStateFeatures -- 00024B80
315 -- EnableThreadProfiling -- 0003EE30
316 -- EncodePointer -- 0009890B (forwarder -> NTDLL.RtlEncodePointer)
317 -- EncodeSystemPointer -- 00098936 (forwarder -> NTDLL.RtlEncodeSystemPointer)
318 -- EndUpdateResourceA -- 00044690
319 -- EndUpdateResourceW -- 000446B0
320 -- EnterCriticalSection -- 0009898E (forwarder -> NTDLL.RtlEnterCriticalSection)
321 -- EnterSynchronizationBarrier -- 00037010
322 -- EnumCalendarInfoA -- 0001FF00
323 -- EnumCalendarInfoExA -- 00047600
324 -- EnumCalendarInfoExEx -- 00010730
325 -- EnumCalendarInfoExW -- 00020450
326 -- EnumCalendarInfoW -- 00037030
327 -- EnumDateFormatsA -- 00047660
328 -- EnumDateFormatsExA -- 00047690
329 -- EnumDateFormatsExEx -- 00037050
330 -- EnumDateFormatsExW -- 00037070
331 -- EnumDateFormatsW -- 000200E0
332 -- EnumLanguageGroupLocalesA -- 000476C0
333 -- EnumLanguageGroupLocalesW -- 00037090
334 -- EnumResourceLanguagesA -- 00035530
335 -- EnumResourceLanguagesExA -- 000370B0
336 -- EnumResourceLanguagesExW -- 000370D0
337 -- EnumResourceLanguagesW -- 00035560
338 -- EnumResourceNamesA -- 00023F60
339 -- EnumResourceNamesExA -- 000370F0
340 -- EnumResourceNamesExW -- 00037110
341 -- EnumResourceNamesW -- 00023F70
342 -- EnumResourceTypesA -- 00035590
343 -- EnumResourceTypesExA -- 00037130
344 -- EnumResourceTypesExW -- 00037150
345 -- EnumResourceTypesW -- 000355C0
346 -- EnumSystemCodePagesA -- 000476F0
347 -- EnumSystemCodePagesW -- 00037170
348 -- EnumSystemFirmwareTables -- 00034A60
349 -- EnumSystemGeoID -- 0004E0E0
350 -- EnumSystemGeoNames -- 0004E1A0
351 -- EnumSystemLanguageGroupsA -- 00047710
352 -- EnumSystemLanguageGroupsW -- 00037190
353 -- EnumSystemLocalesA -- 000371B0
354 -- EnumSystemLocalesEx -- 00020130
355 -- EnumSystemLocalesW -- 00020250
356 -- EnumTimeFormatsA -- 00047730
357 -- EnumTimeFormatsEx -- 000107A0
358 -- EnumTimeFormatsW -- 00020270
359 -- EnumUILanguagesA -- 00047780
360 -- EnumUILanguagesW -- 000201D0
361 -- EnumerateLocalComputerNamesA -- 00055650
362 -- EnumerateLocalComputerNamesW -- 00055700
363 -- EraseTape -- 0003E200
364 -- EscapeCommFunction -- 00024050
365 -- ExitProcess -- 00028630
366 -- ExitThread -- 00098D51 (forwarder -> NTDLL.RtlExitUserThread)
367 -- ExitVDM -- 0003C1A0
368 -- ExpandEnvironmentStringsA -- 00022BE0
369 -- ExpandEnvironmentStringsW -- 0001E790
370 -- ExpungeConsoleCommandHistoryA -- 000245D0
371 -- ExpungeConsoleCommandHistoryW -- 000245E0
372 -- FatalAppExitA -- 000371D0
373 -- FatalAppExitW -- 000371E0
374 -- FatalExit -- 00036420
375 -- FileTimeToDosDateTime -- 00021560
376 -- FileTimeToLocalFileTime -- 00023AE0
377 -- FileTimeToSystemTime -- 00023FB0
378 -- FillConsoleOutputAttribute -- 00024350
379 -- FillConsoleOutputCharacterA -- 00024360
380 -- FillConsoleOutputCharacterW -- 00024370
381 -- FindActCtxSectionGuid -- 00016310
382 -- FindActCtxSectionGuidWorker -- 00014240
383 -- FindActCtxSectionStringA -- 0005F190
384 -- FindActCtxSectionStringW -- 0001F310
385 -- FindActCtxSectionStringWWorker -- 0001DF00
386 -- FindAtomA -- 0001F070
387 -- FindAtomW -- 000140A0
388 -- FindClose -- 00023AF0
389 -- FindCloseChangeNotification -- 00023B00
390 -- FindFirstChangeNotificationA -- 00023B10
391 -- FindFirstChangeNotificationW -- 00023B20
392 -- FindFirstFileA -- 00023B30
393 -- FindFirstFileExA -- 00023B40
394 -- FindFirstFileExW -- 00023B50
395 -- FindFirstFileNameTransactedW -- 000341D0
396 -- FindFirstFileNameW -- 00023B60
397 -- FindFirstFileTransactedA -- 00034270
398 -- FindFirstFileTransactedW -- 0005F1F0
399 -- FindFirstFileW -- 00023B70
400 -- FindFirstStreamTransactedW -- 00034310
401 -- FindFirstStreamW -- 00099062 (forwarder -> api-ms-win-core-file-l1-2-2.FindFirstStreamW)
402 -- FindFirstVolumeA -- 0005D790
403 -- FindFirstVolumeMountPointA -- 0005D8B0
404 -- FindFirstVolumeMountPointW -- 0005D9F0
405 -- FindFirstVolumeW -- 00023B80
406 -- FindNLSString -- 000371F0
407 -- FindNLSStringEx -- 00019830
408 -- FindNextChangeNotification -- 00023B90
409 -- FindNextFileA -- 00023BA0
410 -- FindNextFileNameW -- 00023BB0
411 -- FindNextFileW -- 00023BC0
412 -- FindNextStreamW -- 0009915E (forwarder -> api-ms-win-core-file-l1-2-2.FindNextStreamW)
413 -- FindNextVolumeA -- 0005DBB0
414 -- FindNextVolumeMountPointA -- 0005DCD0
415 -- FindNextVolumeMountPointW -- 0005E240
416 -- FindNextVolumeW -- 00023BD0
417 -- FindPackagesByPackageFamily -- 000991FA (forwarder -> kernelbase.FindPackagesByPackageFamily)
418 -- FindResourceA -- 0001D270
419 -- FindResourceExA -- 00021330
420 -- FindResourceExW -- 00016900
421 -- FindResourceW -- 0001C000
422 -- FindStringOrdinal -- 00037210
423 -- FindVolumeClose -- 00023BE0
424 -- FindVolumeMountPointClose -- 0005E260
425 -- FlsAlloc -- 00019C80
426 -- FlsFree -- 00022A20
427 -- FlsGetValue -- 00015600
428 -- FlsSetValue -- 00019410
429 -- FlushConsoleInputBuffer -- 00024380
430 -- FlushFileBuffers -- 00023BF0
431 -- FlushInstructionCache -- 00018860
432 -- FlushProcessWriteBuffers -- 0009931A (forwarder -> NTDLL.NtFlushProcessWriteBuffers)
433 -- FlushViewOfFile -- 000204F0
434 -- FoldStringA -- 000477A0
435 -- FoldStringW -- 0001F930
436 -- FormatApplicationUserModelId -- 00099380 (forwarder -> kernelbase.FormatApplicationUserModelId)
437 -- FormatMessageA -- 0001F970
438 -- FormatMessageW -- 00019B90
439 -- FreeConsole -- 00024240
440 -- FreeEnvironmentStringsA -- 00037230
441 -- FreeEnvironmentStringsW -- 00019BB0
442 -- FreeLibrary -- 000189C0
443 -- FreeLibraryAndExitThread -- 00019BD0
444 -- FreeLibraryWhenCallbackReturns -- 00099446 (forwarder -> NTDLL.TpCallbackUnloadDllOnCompletion)
445 -- FreeMemoryJobObject -- 00024AD0
446 -- FreeResource -- 00022370
447 -- FreeUserPhysicalPages -- 00037250
448 -- GenerateConsoleCtrlEvent -- 00024390
449 -- GetACP -- 00016DF0
450 -- GetActiveProcessorCount -- 00019980
451 -- GetActiveProcessorGroupCount -- 0005F2F0
452 -- GetAppContainerAce -- 00037270
453 -- GetAppContainerNamedObjectPath -- 00037290
454 -- GetApplicationRecoveryCallback -- 000372B0
455 -- GetApplicationRecoveryCallbackWorker -- 0003EB70
456 -- GetApplicationRestartSettings -- 000372D0
457 -- GetApplicationRestartSettingsWorker -- 0003EBF0
458 -- GetApplicationUserModelId -- 000995CA (forwarder -> kernelbase.GetApplicationUserModelId)
459 -- GetAtomNameA -- 00054810
460 -- GetAtomNameW -- 0001E750
461 -- GetBinaryType -- 0005A550
462 -- GetBinaryTypeA -- 0005A550
463 -- GetBinaryTypeW -- 0005A590
464 -- GetCPInfo -- 00019650
465 -- GetCPInfoExA -- 00047980
466 -- GetCPInfoExW -- 000372F0
467 -- GetCachedSigningLevel -- 00037310
468 -- GetCalendarDateFormat -- 00046890
469 -- GetCalendarDateFormatEx -- 00046900
470 -- GetCalendarDaysInMonth -- 00046C70
471 -- GetCalendarDifferenceInDays -- 00046E00
472 -- GetCalendarInfoA -- 00047A20
473 -- GetCalendarInfoEx -- 00023FC0
474 -- GetCalendarInfoW -- 00023FD0
475 -- GetCalendarMonthsInYear -- 00046EF0
476 -- GetCalendarSupportedDateRange -- 00046FB0
477 -- GetCalendarWeekNumber -- 00047060
478 -- GetComPlusPackageInstallStatus -- 0003EA90
479 -- GetCommConfig -- 00024060
480 -- GetCommMask -- 00024070
481 -- GetCommModemStatus -- 00024080
482 -- GetCommProperties -- 00024090
483 -- GetCommState -- 000240A0
484 -- GetCommTimeouts -- 000240B0
485 -- GetCommandLineA -- 00019E40
486 -- GetCommandLineW -- 00019D10
487 -- GetCompressedFileSizeA -- 00037330
488 -- GetCompressedFileSizeTransactedA -- 0005C2D0
489 -- GetCompressedFileSizeTransactedW -- 0005C320
490 -- GetCompressedFileSizeW -- 00037350
491 -- GetComputerNameA -- 0001FD00
492 -- GetComputerNameExA -- 00037370
493 -- GetComputerNameExW -- 0001A020
494 -- GetComputerNameW -- 00022070
495 -- GetConsoleAliasA -- 000245F0
496 -- GetConsoleAliasExesA -- 00024600
497 -- GetConsoleAliasExesLengthA -- 00024610
498 -- GetConsoleAliasExesLengthW -- 00024620
499 -- GetConsoleAliasExesW -- 00024630
500 -- GetConsoleAliasW -- 00024640
501 -- GetConsoleAliasesA -- 00024650
502 -- GetConsoleAliasesLengthA -- 00024660
503 -- GetConsoleAliasesLengthW -- 00024670
504 -- GetConsoleAliasesW -- 00024680
505 -- GetConsoleCP -- 00024250
506 -- GetConsoleCharType -- 00062A40
507 -- GetConsoleCommandHistoryA -- 00024690
508 -- GetConsoleCommandHistoryLengthA -- 000246A0
509 -- GetConsoleCommandHistoryLengthW -- 000246B0
510 -- GetConsoleCommandHistoryW -- 000246C0
511 -- GetConsoleCursorInfo -- 000243A0
512 -- GetConsoleCursorMode -- 00062AA0
513 -- GetConsoleDisplayMode -- 000246D0
514 -- GetConsoleFontInfo -- 00062D40
515 -- GetConsoleFontSize -- 000246E0
516 -- GetConsoleHardwareState -- 00062640
517 -- GetConsoleHistoryInfo -- 000246F0
518 -- GetConsoleInputExeNameA -- 00099ABD (forwarder -> kernelbase.GetConsoleInputExeNameA)
519 -- GetConsoleInputExeNameW -- 00099AF8 (forwarder -> kernelbase.GetConsoleInputExeNameW)
520 -- GetConsoleInputWaitHandle -- 00062580
521 -- GetConsoleKeyboardLayoutNameA -- 00062DD0
522 -- GetConsoleKeyboardLayoutNameW -- 00062DF0
523 -- GetConsoleMode -- 00024260
524 -- GetConsoleNlsMode -- 00062B00
525 -- GetConsoleOriginalTitleA -- 000243B0
526 -- GetConsoleOriginalTitleW -- 000243C0
527 -- GetConsoleOutputCP -- 00024270
528 -- GetConsoleProcessList -- 00024700
529 -- GetConsoleScreenBufferInfo -- 000243D0
530 -- GetConsoleScreenBufferInfoEx -- 000243E0
531 -- GetConsoleSelectionInfo -- 00024710
532 -- GetConsoleTitleA -- 000243F0
533 -- GetConsoleTitleW -- 00024400
534 -- GetConsoleWindow -- 00024720
535 -- GetCurrencyFormatA -- 00047BF0
536 -- GetCurrencyFormatEx -- 00037390
537 -- GetCurrencyFormatW -- 000373B0
538 -- GetCurrentActCtx -- 000373E0
539 -- GetCurrentActCtxWorker -- 00022390
540 -- GetCurrentApplicationUserModelId -- 00099CF3 (forwarder -> kernelbase.GetCurrentApplicationUserModelId)
541 -- GetCurrentConsoleFont -- 00024730
542 -- GetCurrentConsoleFontEx -- 00024740
543 -- GetCurrentDirectoryA -- 00037400
544 -- GetCurrentDirectoryW -- 0001F990
545 -- GetCurrentPackageFamilyName -- 00099D93 (forwarder -> kernelbase.GetCurrentPackageFamilyName)
546 -- GetCurrentPackageFullName -- 00099DD4 (forwarder -> kernelbase.GetCurrentPackageFullName)
547 -- GetCurrentPackageId -- 00099E0D (forwarder -> kernelbase.GetCurrentPackageId)
548 -- GetCurrentPackageInfo -- 00099E42 (forwarder -> kernelbase.GetCurrentPackageInfo)
549 -- GetCurrentPackagePath -- 00099E79 (forwarder -> kernelbase.GetCurrentPackagePath)
550 -- GetCurrentPackageVirtualizationContext -- 00024980
551 -- GetCurrentProcess -- 000237B0
552 -- GetCurrentProcessId -- 000237C0
553 -- GetCurrentProcessorNumber -- 00099F01 (forwarder -> NTDLL.RtlGetCurrentProcessorNumber)
554 -- GetCurrentProcessorNumberEx -- 00099F40 (forwarder -> NTDLL.RtlGetCurrentProcessorNumberEx)
555 -- GetCurrentThread -- 000162C0
556 -- GetCurrentThreadId -- 00011CD0
557 -- GetCurrentThreadStackLimits -- 00099FA5 (forwarder -> api-ms-win-core-processthreads-l1-1-0.GetCurrentThreadStackLimits)
558 -- GetDateFormatA -- 00037420
559 -- GetDateFormatAWorker -- 00047F20
560 -- GetDateFormatEx -- 00037440
561 -- GetDateFormatW -- 0001F690
562 -- GetDateFormatWWorker -- 0001C110
563 -- GetDefaultCommConfigA -- 00039E80
564 -- GetDefaultCommConfigW -- 00039F10
565 -- GetDevicePowerState -- 0005F8B0
566 -- GetDiskFreeSpaceA -- 00023C00
567 -- GetDiskFreeSpaceExA -- 00023C10
568 -- GetDiskFreeSpaceExW -- 00023C20
569 -- GetDiskFreeSpaceW -- 00023C30
570 -- GetDiskSpaceInformationA -- 0009A0E4 (forwarder -> api-ms-win-core-file-l1-2-3.GetDiskSpaceInformationA)
571 -- GetDiskSpaceInformationW -- 0009A132 (forwarder -> api-ms-win-core-file-l1-2-3.GetDiskSpaceInformationW)
572 -- GetDllDirectoryA -- 000355F0
573 -- GetDllDirectoryW -- 0005F290
574 -- GetDriveTypeA -- 00023C40
575 -- GetDriveTypeW -- 00023C50
576 -- GetDurationFormat -- 00048DA0
577 -- GetDurationFormatEx -- 00037460
578 -- GetDynamicTimeZoneInformation -- 000107E0
579 -- GetEnabledXStateFeatures -- 0001A140
580 -- GetEncryptedFileVersionExt -- 00034980
581 -- GetEnvironmentStrings -- 00037490
582 -- GetEnvironmentStringsA -- 00024020
583 -- GetEnvironmentStringsW -- 00019E50
584 -- GetEnvironmentVariableA -- 00019E60
585 -- GetEnvironmentVariableW -- 00017B10
586 -- GetEraNameCountedString -- 000374A0
587 -- GetErrorMode -- 000374C0
588 -- GetExitCodeProcess -- 00020100
589 -- GetExitCodeThread -- 00019FA0
590 -- GetExpandedNameA -- 00039360
591 -- GetExpandedNameW -- 00039440
592 -- GetFileAttributesA -- 00023C60
593 -- GetFileAttributesExA -- 00023C70
594 -- GetFileAttributesExW -- 00023C80
595 -- GetFileAttributesTransactedA -- 0005C3B0
596 -- GetFileAttributesTransactedW -- 0005C400
597 -- GetFileAttributesW -- 00023C90
598 -- GetFileBandwidthReservation -- 000343B0
599 -- GetFileInformationByHandle -- 00023CA0
600 -- GetFileInformationByHandleEx -- 00022C20
601 -- GetFileMUIInfo -- 000374D0
602 -- GetFileMUIPath -- 00010750
603 -- GetFileSize -- 00023CB0
604 -- GetFileSizeEx -- 00023CC0
605 -- GetFileTime -- 00023CD0
606 -- GetFileType -- 00023CE0
607 -- GetFinalPathNameByHandleA -- 00023CF0
608 -- GetFinalPathNameByHandleW -- 00023D00
609 -- GetFirmwareEnvironmentVariableA -- 0005FAC0
610 -- GetFirmwareEnvironmentVariableExA -- 0005FAF0
611 -- GetFirmwareEnvironmentVariableExW -- 0005FBB0
612 -- GetFirmwareEnvironmentVariableW -- 0005FC50
613 -- GetFirmwareType -- 0005FE40
614 -- GetFullPathNameA -- 00023D10
615 -- GetFullPathNameTransactedA -- 00033FA0
616 -- GetFullPathNameTransactedW -- 0005FEA0
617 -- GetFullPathNameW -- 00023D20
618 -- GetGeoInfoA -- 000480D0
619 -- GetGeoInfoEx -- 0004E260
620 -- GetGeoInfoW -- 0004E3A0
621 -- GetHandleContext -- 0003E4C0
622 -- GetHandleInformation -- 00023850
623 -- GetLargePageMinimum -- 000374F0
624 -- GetLargestConsoleWindowSize -- 00024410
625 -- GetLastError -- 000149B0
626 -- GetLocalTime -- 00018410
627 -- GetLocaleInfoA -- 0001EB50
628 -- GetLocaleInfoEx -- 00018900
629 -- GetLocaleInfoW -- 000178B0
630 -- GetLogicalDriveStringsA -- 0005FF30
631 -- GetLogicalDriveStringsW -- 00023D30
632 -- GetLogicalDrives -- 0001F430
633 -- GetLogicalProcessorInformation -- 00037500
634 -- GetLogicalProcessorInformationEx -- 0009A68F (forwarder -> api-ms-win-core-sysinfo-l1-1-0.GetLogicalProcessorInformationEx)
635 -- GetLongPathNameA -- 00054030
636 -- GetLongPathNameTransactedA -- 0003C260
637 -- GetLongPathNameTransactedW -- 0005A800
638 -- GetLongPathNameW -- 00014F30
639 -- GetMachineTypeAttributes -- 0009A740 (forwarder -> api-ms-win-core-processthreads-l1-1-7.GetMachineTypeAttributes)
640 -- GetMailslotInfo -- 0005BD20
641 -- GetMaximumProcessorCount -- 0005F330
642 -- GetMaximumProcessorGroupCount -- 0005F3C0
643 -- GetMemoryErrorHandlingCapabilities -- 00037520
644 -- GetModuleFileNameA -- 000190E0
645 -- GetModuleFileNameW -- 000193F0
646 -- GetModuleHandleA -- 000188E0
647 -- GetModuleHandleExA -- 0001F5A0
648 -- GetModuleHandleExW -- 00019770
649 -- GetModuleHandleW -- 000190C0
650 -- GetNLSVersion -- 00020370
651 -- GetNLSVersionEx -- 00037540
652 -- GetNamedPipeAttribute -- 00037560
653 -- GetNamedPipeClientComputerNameA -- 0005A910
654 -- GetNamedPipeClientComputerNameW -- 00037580
655 -- GetNamedPipeClientProcessId -- 0005AA20
656 -- GetNamedPipeClientSessionId -- 000357E0
657 -- GetNamedPipeHandleStateA -- 0005AA50
658 -- GetNamedPipeHandleStateW -- 000375A0
659 -- GetNamedPipeInfo -- 0009A946 (forwarder -> api-ms-win-core-namedpipe-l1-2-1.GetNamedPipeInfo)
660 -- GetNamedPipeServerProcessId -- 0005AB50
661 -- GetNamedPipeServerSessionId -- 00035830
662 -- GetNativeSystemInfo -- 00019D00
663 -- GetNextVDMCommand -- 0003C2F0
664 -- GetNumaAvailableMemoryNode -- 000359D0
665 -- GetNumaAvailableMemoryNodeEx -- 00060360
666 -- GetNumaHighestNodeNumber -- 0001A100
667 -- GetNumaNodeNumberFromHandle -- 000359F0
669 -- GetNumaNodeProcessorMask -- 000603D0
668 -- GetNumaNodeProcessorMask2 -- 0009AA5D (forwarder -> api-ms-win-core-systemtopology-l1-1-2.GetNumaNodeProcessorMask2)
670 -- GetNumaNodeProcessorMaskEx -- 000375C0
671 -- GetNumaProcessorNode -- 00035A40
672 -- GetNumaProcessorNodeEx -- 00060440
673 -- GetNumaProximityNode -- 00035A90
674 -- GetNumaProximityNodeEx -- 000375E0
675 -- GetNumberFormatA -- 00048160
676 -- GetNumberFormatEx -- 00037600
677 -- GetNumberFormatW -- 0001F2B0
678 -- GetNumberOfConsoleFonts -- 00062E80
679 -- GetNumberOfConsoleInputEvents -- 00024280
680 -- GetNumberOfConsoleMouseButtons -- 00024750
681 -- GetOEMCP -- 00022AD0
682 -- GetOverlappedResult -- 0001B7F0
683 -- GetOverlappedResultEx -- 0009ABE5 (forwarder -> api-ms-win-core-io-l1-1-1.GetOverlappedResultEx)
684 -- GetPackageApplicationIds -- 0009AC2E (forwarder -> kernelbase.GetPackageApplicationIds)
685 -- GetPackageFamilyName -- 0009AC67 (forwarder -> kernelbase.GetPackageFamilyName)
686 -- GetPackageFullName -- 0009AC9A (forwarder -> kernelbase.GetPackageFullName)
687 -- GetPackageId -- 0009ACC5 (forwarder -> kernelbase.GetPackageId)
688 -- GetPackageInfo -- 0009ACEC (forwarder -> kernelbase.GetPackageInfo)
689 -- GetPackagePath -- 0009AD15 (forwarder -> kernelbase.GetPackagePath)
690 -- GetPackagePathByFullName -- 0009AD48 (forwarder -> kernelbase.GetPackagePathByFullName)
691 -- GetPackagesByPackageFamily -- 0009AD87 (forwarder -> kernelbase.GetPackagesByPackageFamily)
692 -- GetPhysicallyInstalledSystemMemory -- 00037620
693 -- GetPriorityClass -- 00037640
694 -- GetPrivateProfileIntA -- 00020530
695 -- GetPrivateProfileIntW -- 0001B490
696 -- GetPrivateProfileSectionA -- 00059CD0
697 -- GetPrivateProfileSectionNamesA -- 00059D60
698 -- GetPrivateProfileSectionNamesW -- 0001B560
699 -- GetPrivateProfileSectionW -- 0001E9D0
700 -- GetPrivateProfileStringA -- 00020590
701 -- GetPrivateProfileStringW -- 0001B590
702 -- GetPrivateProfileStructA -- 00059D90
703 -- GetPrivateProfileStructW -- 00059EE0
704 -- GetProcAddress -- 000162D0
705 -- GetProcessAffinityMask -- 00019CA0
706 -- GetProcessDEPPolicy -- 00036440
707 -- GetProcessDefaultCpuSetMasks -- 0009AF3A (forwarder -> api-ms-win-core-processthreads-l1-1-6.GetProcessDefaultCpuSetMasks)
708 -- GetProcessDefaultCpuSets -- 0009AF96 (forwarder -> api-ms-win-core-processthreads-l1-1-3.GetProcessDefaultCpuSets)
709 -- GetProcessGroupAffinity -- 00037660
710 -- GetProcessHandleCount -- 00037680
711 -- GetProcessHeap -- 00016980
712 -- GetProcessHeaps -- 000376A0
713 -- GetProcessId -- 00019100
714 -- GetProcessIdOfThread -- 00022A00
715 -- GetProcessInformation -- 000237D0
716 -- GetProcessIoCounters -- 00022510
717 -- GetProcessMitigationPolicy -- 0009B08A (forwarder -> api-ms-win-core-processthreads-l1-1-1.GetProcessMitigationPolicy)
718 -- GetProcessPreferredUILanguages -- 000376C0
719 -- GetProcessPriorityBoost -- 000376E0
720 -- GetProcessShutdownParameters -- 00037700
721 -- GetProcessTimes -- 000162F0
722 -- GetProcessVersion -- 0001F5E0
723 -- GetProcessWorkingSetSize -- 00023810
724 -- GetProcessWorkingSetSizeEx -- 00037720
725 -- GetProcessesInVirtualizationContext -- 000249A0
726 -- GetProcessorSystemCycleTime -- 0009B1B5 (forwarder -> api-ms-win-core-sysinfo-l1-2-2.GetProcessorSystemCycleTime)
727 -- GetProductInfo -- 00022A90
728 -- GetProfileIntA -- 00020510
729 -- GetProfileIntW -- 0005A060
730 -- GetProfileSectionA -- 0005A080
731 -- GetProfileSectionW -- 0005A0A0
732 -- GetProfileStringA -- 0001FE00
733 -- GetProfileStringW -- 0001A5C0
734 -- GetQueuedCompletionStatus -- 00022C40
735 -- GetQueuedCompletionStatusEx -- 00037740
736 -- GetShortPathNameA -- 0001F0D0
737 -- GetShortPathNameW -- 00014B60
738 -- GetStagedPackagePathByFullName -- 0009B2E0 (forwarder -> kernelbase.GetStagedPackagePathByFullName)
739 -- GetStartupInfoA -- 00021AE0
740 -- GetStartupInfoW -- 000196A0
741 -- GetStateFolder -- 0009B339 (forwarder -> kernelbase.GetStateFolder)
742 -- GetStdHandle -- 000197F0
743 -- GetStringScripts -- 00037760
744 -- GetStringTypeA -- 000203D0
745 -- GetStringTypeExA -- 000203D0
746 -- GetStringTypeExW -- 0001B7D0
747 -- GetStringTypeW -- 00018880
748 -- GetSystemAppDataKey -- 0009B3C5 (forwarder -> kernelbase.GetSystemAppDataKey)
749 -- GetSystemCpuSetInformation -- 0009B3FF (forwarder -> api-ms-win-core-processthreads-l1-1-3.GetSystemCpuSetInformation)
750 -- GetSystemDEPPolicy -- 00036480
751 -- GetSystemDefaultLCID -- 0001EA50
752 -- GetSystemDefaultLangID -- 0001DDF0
753 -- GetSystemDefaultLocaleName -- 00037780
754 -- GetSystemDefaultUILanguage -- 00019F90
755 -- GetSystemDirectoryA -- 0001F4B0
756 -- GetSystemDirectoryW -- 000195E0
757 -- GetSystemFileCacheSize -- 000377A0
758 -- GetSystemFirmwareTable -- 00034A80
759 -- GetSystemInfo -- 00019A90
760 -- GetSystemPowerStatus -- 00022630
761 -- GetSystemPreferredUILanguages -- 000377C0
762 -- GetSystemRegistryQuota -- 000364C0
763 -- GetSystemTime -- 000197B0
764 -- GetSystemTimeAdjustment -- 00019C60
765 -- GetSystemTimeAsFileTime -- 00016210
766 -- GetSystemTimePreciseAsFileTime -- 00023FA0
767 -- GetSystemTimes -- 000377E0
768 -- GetSystemWindowsDirectoryA -- 00037800
769 -- GetSystemWindowsDirectoryW -- 00015670
770 -- GetSystemWow64DirectoryA -- 00024150
771 -- GetSystemWow64DirectoryW -- 00024160
772 -- GetTapeParameters -- 00060540
773 -- GetTapePosition -- 0003E240
774 -- GetTapeStatus -- 0003E2B0
775 -- GetTempFileNameA -- 00023D40
776 -- GetTempFileNameW -- 00023D50
777 -- GetTempPath2A -- 00023D60
778 -- GetTempPath2W -- 00023D70
779 -- GetTempPathA -- 00023D80
780 -- GetTempPathW -- 00023D90
781 -- GetThreadContext -- 00037820
782 -- GetThreadDescription -- 0009B6E5 (forwarder -> api-ms-win-core-processthreads-l1-1-3.GetThreadDescription)
783 -- GetThreadEnabledXStateFeatures -- 00024BA0
784 -- GetThreadErrorMode -- 00037840
785 -- GetThreadGroupAffinity -- 00037850
786 -- GetThreadIOPendingFlag -- 00037870
787 -- GetThreadId -- 00037890
788 -- GetThreadIdealProcessorEx -- 000378B0
789 -- GetThreadInformation -- 000237E0
790 -- GetThreadLocale -- 0001F420
791 -- GetThreadPreferredUILanguages -- 00017AD0
792 -- GetThreadPriority -- 00018970
793 -- GetThreadPriorityBoost -- 000378D0
794 -- GetThreadSelectedCpuSetMasks -- 0009B82F (forwarder -> api-ms-win-core-processthreads-l1-1-6.GetThreadSelectedCpuSetMasks)
795 -- GetThreadSelectedCpuSets -- 0009B88B (forwarder -> api-ms-win-core-processthreads-l1-1-3.GetThreadSelectedCpuSets)
796 -- GetThreadSelectorEntry -- 000605A0
797 -- GetThreadTimes -- 00019B70
798 -- GetThreadUILanguage -- 0001E670
800 -- GetTickCount -- 00022C80
799 -- GetTickCount64 -- 00017850
801 -- GetTimeFormatA -- 000378F0
802 -- GetTimeFormatAWorker -- 00048430
803 -- GetTimeFormatEx -- 00037910
804 -- GetTimeFormatW -- 0001E630
805 -- GetTimeFormatWWorker -- 0001CAE0
806 -- GetTimeZoneInformation -- 00019C40
807 -- GetTimeZoneInformationForYear -- 00037930
808 -- GetUILanguageInfo -- 00037950
809 -- GetUserDefaultGeoName -- 0004E7B0
810 -- GetUserDefaultLCID -- 00014A70
811 -- GetUserDefaultLangID -- 000200D0
812 -- GetUserDefaultLocaleName -- 00019F00
813 -- GetUserDefaultUILanguage -- 00022990
814 -- GetUserGeoID -- 0001C020
815 -- GetUserPreferredUILanguages -- 00019FF0
816 -- GetVDMCurrentDirectories -- 0003CBD0
817 -- GetVersion -- 000227E0
818 -- GetVersionExA -- 00019890
819 -- GetVersionExW -- 00019960
820 -- GetVolumeInformationA -- 00023DA0
821 -- GetVolumeInformationByHandleW -- 00023DB0
822 -- GetVolumeInformationW -- 00023DC0
823 -- GetVolumeNameForVolumeMountPointA -- 0005E2B0
824 -- GetVolumeNameForVolumeMountPointW -- 000237A0
825 -- GetVolumePathNameA -- 0005E3F0
826 -- GetVolumePathNameW -- 00023DD0
827 -- GetVolumePathNamesForVolumeNameA -- 0005E530
828 -- GetVolumePathNamesForVolumeNameW -- 00023DE0
829 -- GetWindowsDirectoryA -- 00022950
830 -- GetWindowsDirectoryW -- 00020430
831 -- GetWriteWatch -- 000168E0
832 -- GetXStateFeaturesMask -- 00037970
833 -- GlobalAddAtomA -- 00013B00
834 -- GlobalAddAtomExA -- 00054830
835 -- GlobalAddAtomExW -- 00013B20
836 -- GlobalAddAtomW -- 00013B40
837 -- GlobalAlloc -- 00017B50
838 -- GlobalCompact -- 00034AA0
839 -- GlobalDeleteAtom -- 00019120
840 -- GlobalFindAtomA -- 0001DE40
841 -- GlobalFindAtomW -- 00014080
842 -- GlobalFix -- 00034AC0
843 -- GlobalFlags -- 0001EE20
844 -- GlobalFree -- 000170B0
845 -- GlobalGetAtomNameA -- 00054850
846 -- GlobalGetAtomNameW -- 00013E10
847 -- GlobalHandle -- 0001EA60
848 -- GlobalLock -- 00014360
849 -- GlobalMemoryStatus -- 00016F10
850 -- GlobalMemoryStatusEx -- 00019E20
851 -- GlobalReAlloc -- 00016040
852 -- GlobalSize -- 000178F0
853 -- GlobalUnWire -- 00034AE0
854 -- GlobalUnfix -- 00034B00
855 -- GlobalUnlock -- 00014A80
856 -- GlobalWire -- 00034B20
857 -- Heap32First -- 0005CAA0
858 -- Heap32ListFirst -- 0005CCE0
859 -- Heap32ListNext -- 0005CD90
860 -- Heap32Next -- 0005CE30
861 -- HeapAlloc -- 0009BD78 (forwarder -> NTDLL.RtlAllocateHeap)
862 -- HeapCompact -- 00037990
863 -- HeapCreate -- 00019540
864 -- HeapDestroy -- 000224F0
865 -- HeapFree -- 00011CE0
866 -- HeapLock -- 000379B0
867 -- HeapQueryInformation -- 000379D0
868 -- HeapReAlloc -- 0009BDE4 (forwarder -> NTDLL.RtlReAllocateHeap)
869 -- HeapSetInformation -- 000198D0
870 -- HeapSize -- 0009BE18 (forwarder -> NTDLL.RtlSizeHeap)
871 -- HeapSummary -- 000379F0
872 -- HeapUnlock -- 00037A10
873 -- HeapValidate -- 0001DE00
874 -- HeapWalk -- 00037A30
875 -- IdnToAscii -- 00037A50
876 -- IdnToNameprepUnicode -- 00037A70
877 -- IdnToUnicode -- 00037A90
878 -- InitAtomTable -- 00054870
879 -- InitOnceBeginInitialize -- 0009BEAA (forwarder -> api-ms-win-core-synch-l1-2-0.InitOnceBeginInitialize)
880 -- InitOnceComplete -- 0009BEF0 (forwarder -> api-ms-win-core-synch-l1-2-0.InitOnceComplete)
881 -- InitOnceExecuteOnce -- 0009BF32 (forwarder -> api-ms-win-core-synch-l1-2-0.InitOnceExecuteOnce)
882 -- InitOnceInitialize -- 0009BF76 (forwarder -> NTDLL.RtlRunOnceInitialize)
883 -- InitializeConditionVariable -- 0009BFAD (forwarder -> NTDLL.RtlInitializeConditionVariable)
885 -- InitializeContext -- 00037AE0
884 -- InitializeContext2 -- 00037AB0
886 -- InitializeCriticalSection -- 0009C011 (forwarder -> NTDLL.RtlInitializeCriticalSection)
887 -- InitializeCriticalSectionAndSpinCount -- 00023930
888 -- InitializeCriticalSectionEx -- 00023940
889 -- InitializeEnclave -- 0009C088 (forwarder -> api-ms-win-core-enclave-l1-1-0.InitializeEnclave)
890 -- InitializeProcThreadAttributeList -- 0009C0DB (forwarder -> api-ms-win-core-processthreads-l1-1-0.InitializeProcThreadAttributeList)
891 -- InitializeSListHead -- 0009C137 (forwarder -> NTDLL.RtlInitializeSListHead)
892 -- InitializeSRWLock -- 0009C166 (forwarder -> NTDLL.RtlInitializeSRWLock)
893 -- InitializeSynchronizationBarrier -- 00037B00
894 -- InstallELAMCertificateInfo -- 0009C1BD (forwarder -> api-ms-win-core-sysinfo-l1-2-1.InstallELAMCertificateInfo)
896 -- InterlockedCompareExchange -- 0001D350
895 -- InterlockedCompareExchange64 -- 0009C214 (forwarder -> NTDLL.RtlInterlockedCompareExchange64)
897 -- InterlockedDecrement -- 00021630
898 -- InterlockedExchange -- 000161F0
899 -- InterlockedExchangeAdd -- 0001DE20
900 -- InterlockedFlushSList -- 0009C2AB (forwarder -> NTDLL.RtlInterlockedFlushSList)
901 -- InterlockedIncrement -- 00021540
902 -- InterlockedPopEntrySList -- 0009C2F8 (forwarder -> NTDLL.RtlInterlockedPopEntrySList)
903 -- InterlockedPushEntrySList -- 0009C334 (forwarder -> NTDLL.RtlInterlockedPushEntrySList)
2 -- InterlockedPushListSList -- 00096884 (forwarder -> NTDLL.RtlInterlockedPushListSList)
904 -- InterlockedPushListSListEx -- 0009C372 (forwarder -> NTDLL.RtlInterlockedPushListSListEx)
905 -- InvalidateConsoleDIBits -- 00062F70
906 -- IsBadCodePtr -- 0001E650
907 -- IsBadHugeReadPtr -- 00036530
908 -- IsBadHugeWritePtr -- 00036550
909 -- IsBadReadPtr -- 00014490
910 -- IsBadStringPtrA -- 0001D1F0
911 -- IsBadStringPtrW -- 0001D2E0
912 -- IsBadWritePtr -- 0001A210
913 -- IsCalendarLeapDay -- 00047280
914 -- IsCalendarLeapMonth -- 00047350
915 -- IsCalendarLeapYear -- 00047410
916 -- IsDBCSLeadByte -- 0001D250
917 -- IsDBCSLeadByteEx -- 00037B20
918 -- IsDebuggerPresent -- 0001A040
919 -- IsEnclaveTypeSupported -- 0009C49B (forwarder -> api-ms-win-core-enclave-l1-1-0.IsEnclaveTypeSupported)
920 -- IsNLSDefinedString -- 00037B40
921 -- IsNativeVhdBoot -- 00033DF0
922 -- IsNormalizedString -- 00037B60
923 -- IsProcessCritical -- 0009C519 (forwarder -> api-ms-win-core-processthreads-l1-1-2.IsProcessCritical)
924 -- IsProcessInJob -- 0001A090
925 -- IsProcessorFeaturePresent -- 000183F0
926 -- IsSystemResumeAutomatic -- 0005F900
927 -- IsThreadAFiber -- 00037B80
928 -- IsThreadpoolTimerSet -- 0009C5B6 (forwarder -> NTDLL.TpIsTimerSet)
929 -- IsUserCetAvailableInEnvironment -- 0009C5E9 (forwarder -> api-ms-win-core-sysinfo-l1-2-6.IsUserCetAvailableInEnvironment)
930 -- IsValidCalDateTime -- 000474B0
931 -- IsValidCodePage -- 00019810
932 -- IsValidLanguageGroup -- 00037BA0
933 -- IsValidLocale -- 00019940
934 -- IsValidLocaleName -- 00037BC0
935 -- IsValidNLSVersion -- 00037BE0
936 -- IsWow64GuestMachineSupported -- 0009C6AF (forwarder -> api-ms-win-core-wow64-l1-1-2.IsWow64GuestMachineSupported)
938 -- IsWow64Process -- 00019430
937 -- IsWow64Process2 -- 0009C6F9 (forwarder -> api-ms-win-core-wow64-l1-1-1.IsWow64Process2)
939 -- K32EmptyWorkingSet -- 00037C00
940 -- K32EnumDeviceDrivers -- 00037C20
941 -- K32EnumPageFilesA -- 00037C40
942 -- K32EnumPageFilesW -- 00037C60
943 -- K32EnumProcessModules -- 00037CA0
944 -- K32EnumProcessModulesEx -- 00037C80
945 -- K32EnumProcesses -- 00037CC0
946 -- K32GetDeviceDriverBaseNameA -- 00037CE0
947 -- K32GetDeviceDriverBaseNameW -- 00037D00
948 -- K32GetDeviceDriverFileNameA -- 00037D20
949 -- K32GetDeviceDriverFileNameW -- 00037D40
950 -- K32GetMappedFileNameA -- 00037D60
951 -- K32GetMappedFileNameW -- 00037D80
952 -- K32GetModuleBaseNameA -- 00037DA0
953 -- K32GetModuleBaseNameW -- 00037DC0
954 -- K32GetModuleFileNameExA -- 00037DE0
955 -- K32GetModuleFileNameExW -- 00037E00
956 -- K32GetModuleInformation -- 000202C0
957 -- K32GetPerformanceInfo -- 00037E20
958 -- K32GetProcessImageFileNameA -- 00037E40
959 -- K32GetProcessImageFileNameW -- 00037E60
960 -- K32GetProcessMemoryInfo -- 00037E80
961 -- K32GetWsChanges -- 00037EC0
962 -- K32GetWsChangesEx -- 00037EA0
963 -- K32InitializeProcessForWsWatch -- 00037EE0
964 -- K32QueryWorkingSet -- 00037F20
965 -- K32QueryWorkingSetEx -- 00037F00
966 -- LCIDToLocaleName -- 00019E80
967 -- LCMapStringA -- 000203B0
968 -- LCMapStringEx -- 000196B0
969 -- LCMapStringW -- 00019180
970 -- LZClose -- 00039530
971 -- LZCloseFile -- 00039530
972 -- LZCopy -- 00033ED0
973 -- LZCreateFileW -- 000395C0
974 -- LZDone -- 000235C0
975 -- LZInit -- 00039690
976 -- LZOpenFileA -- 000397E0
977 -- LZOpenFileW -- 000398B0
978 -- LZRead -- 00039940
979 -- LZSeek -- 00039B60
980 -- LZStart -- 0001A1C0
981 -- LeaveCriticalSection -- 0009CA52 (forwarder -> NTDLL.RtlLeaveCriticalSection)
982 -- LeaveCriticalSectionWhenCallbackReturns -- 0009CA98 (forwarder -> NTDLL.TpCallbackLeaveCriticalSectionOnCompletion)
983 -- LoadAppInitDlls -- 00016990
984 -- LoadEnclaveData -- 0009CAE9 (forwarder -> api-ms-win-core-enclave-l1-1-0.LoadEnclaveData)
985 -- LoadLibraryA -- 000224A0
986 -- LoadLibraryExA -- 00019790
987 -- LoadLibraryExW -- 00016220
988 -- LoadLibraryW -- 000197D0
989 -- LoadModule -- 0005F400
990 -- LoadPackagedLibrary -- 00023F80
991 -- LoadResource -- 00015650
992 -- LoadStringBaseExW -- 00037F40
993 -- LoadStringBaseW -- 00035500
994 -- LocalAlloc -- 00017B70
995 -- LocalCompact -- 00034AA0
996 -- LocalFileTimeToFileTime -- 00023DF0
997 -- LocalFileTimeToLocalSystemTime -- 0009CBED (forwarder -> api-ms-win-core-timezone-l1-1-1.LocalFileTimeToLocalSystemTime)
998 -- LocalFlags -- 00060610
999 -- LocalFree -- 00016920
1000 -- LocalHandle -- 00035420
1001 -- LocalLock -- 00017A40
1002 -- LocalReAlloc -- 0001F600
1003 -- LocalShrink -- 000354E0
1004 -- LocalSize -- 00016C90
1005 -- LocalSystemTimeToLocalFileTime -- 0009CC99 (forwarder -> api-ms-win-core-timezone-l1-1-1.LocalSystemTimeToLocalFileTime)
1006 -- LocalUnlock -- 00017A60
1007 -- LocaleNameToLCID -- 00019D20
1008 -- LocateXStateFeature -- 00037F60
1009 -- LockFile -- 00023E00
1010 -- LockFileEx -- 00023E10
1011 -- LockResource -- 00021650
1012 -- MapUserPhysicalPages -- 00037F80
1013 -- MapUserPhysicalPagesScatter -- 0003ED30
1014 -- MapViewOfFile -- 00017B30
1015 -- MapViewOfFileEx -- 00019590
1016 -- MapViewOfFileExNuma -- 00037FA0
1017 -- MapViewOfFileFromApp -- 0009CDA2 (forwarder -> api-ms-win-core-memory-l1-1-1.MapViewOfFileFromApp)
1018 -- Module32First -- 0005D040
1019 -- Module32FirstW -- 0005D130
1020 -- Module32Next -- 0005D1F0
1021 -- Module32NextW -- 0005D2E0
1022 -- MoveFileA -- 00021760
1023 -- MoveFileExA -- 0005C490
1024 -- MoveFileExW -- 00020470
1025 -- MoveFileTransactedA -- 0005C4C0
1026 -- MoveFileTransactedW -- 0005C560
1027 -- MoveFileW -- 00022A50
1028 -- MoveFileWithProgressA -- 0005C600
1029 -- MoveFileWithProgressW -- 00037FC0
1030 -- MulDiv -- 00023660
1031 -- MultiByteToWideChar -- 00014340
1032 -- NeedCurrentDirectoryForExePathA -- 00037FE0
1033 -- NeedCurrentDirectoryForExePathW -- 00038000
1034 -- NlsCheckPolicy -- 00023FE0
1035 -- NlsGetCacheUpdateCount -- 00023FF0
1036 -- NlsUpdateLocale -- 00024000
1037 -- NlsUpdateSystemLocale -- 00024010
1038 -- NormalizeString -- 00038020
1039 -- NotifyMountMgr -- 00038040
1040 -- NotifyUILanguageChange -- 000491C0
1041 -- NtVdm64CreateProcessInternalW -- 00036570
1042 -- OOBEComplete -- 000608D0
1043 -- OfferVirtualMemory -- 0009CFA8 (forwarder -> api-ms-win-core-memory-l1-1-2.OfferVirtualMemory)
1044 -- OpenConsoleW -- 00062590
1045 -- OpenConsoleWStub -- 00038050
1046 -- OpenEventA -- 00023950
1047 -- OpenEventW -- 00023960
1048 -- OpenFile -- 0005B180
1049 -- OpenFileById -- 00038070
1050 -- OpenFileMappingA -- 0001F010
1051 -- OpenFileMappingW -- 00019EA0
1052 -- OpenJobObjectA -- 000562F0
1053 -- OpenJobObjectW -- 00056350
1054 -- OpenMutexA -- 0001DDA0
1055 -- OpenMutexW -- 00023970
1056 -- OpenPackageInfoByFullName -- 0009D093 (forwarder -> kernelbase.OpenPackageInfoByFullName)
1057 -- OpenPrivateNamespaceA -- 0005ADF0
1058 -- OpenPrivateNamespaceW -- 00020490
1059 -- OpenProcess -- 00017AF0
1060 -- OpenProcessToken -- 0009D101 (forwarder -> api-ms-win-core-processthreads-l1-1-0.OpenProcessToken)
1061 -- OpenProfileUserMapping -- 0001A1C0
1062 -- OpenSemaphoreA -- 00022B70
1063 -- OpenSemaphoreW -- 00023980
1064 -- OpenState -- 0009D177 (forwarder -> kernelbase.OpenState)
1065 -- OpenStateExplicit -- 0009D19E (forwarder -> kernelbase.OpenStateExplicit)
1066 -- OpenThread -- 00016EB0
1067 -- OpenThreadToken -- 0009D1D6 (forwarder -> api-ms-win-core-processthreads-l1-1-0.OpenThreadToken)
1068 -- OpenWaitableTimerA -- 0005BE80
1069 -- OpenWaitableTimerW -- 00023990
1070 -- OutputDebugStringA -- 0001FF90
1071 -- OutputDebugStringW -- 00038090
1072 -- PackageFamilyNameFromFullName -- 0009D276 (forwarder -> kernelbase.PackageFamilyNameFromFullName)
1073 -- PackageFamilyNameFromId -- 0009D2B7 (forwarder -> kernelbase.PackageFamilyNameFromId)
1074 -- PackageFullNameFromId -- 0009D2F0 (forwarder -> kernelbase.PackageFullNameFromId)
1075 -- PackageIdFromFullName -- 0009D327 (forwarder -> kernelbase.PackageIdFromFullName)
1076 -- PackageNameAndPublisherIdFromFamilyName -- 0009D370 (forwarder -> kernelbase.PackageNameAndPublisherIdFromFamilyName)
1077 -- ParseApplicationUserModelId -- 0009D3BF (forwarder -> kernelbase.ParseApplicationUserModelId)
1078 -- PeekConsoleInputA -- 00024290
1079 -- PeekConsoleInputW -- 000242A0
1080 -- PeekNamedPipe -- 000380A0
1081 -- PostQueuedCompletionStatus -- 00022BC0
1082 -- PowerClearRequest -- 0005F910
1083 -- PowerCreateRequest -- 0005F990
1084 -- PowerSetRequest -- 0005F9F0
1085 -- PrefetchVirtualMemory -- 0009D47E (forwarder -> api-ms-win-core-memory-l1-1-1.PrefetchVirtualMemory)
1086 -- PrepareTape -- 0003E2E0
1087 -- PrivCopyFileExW -- 000380C0
1088 -- PrivMoveFileIdentityW -- 0005C630
1089 -- Process32First -- 0001FE30
1090 -- Process32FirstW -- 00022840
1091 -- Process32Next -- 0001E680
1092 -- Process32NextW -- 000218D0
1093 -- ProcessIdToSessionId -- 00018CD0
1094 -- PssCaptureSnapshot -- 000380E0
1095 -- PssDuplicateSnapshot -- 00038100
1096 -- PssFreeSnapshot -- 00038120
1097 -- PssQuerySnapshot -- 00038140
1098 -- PssWalkMarkerCreate -- 00038160
1099 -- PssWalkMarkerFree -- 00038180
1100 -- PssWalkMarkerGetPosition -- 000381A0
1101 -- PssWalkMarkerRewind -- 000381C0
1102 -- PssWalkMarkerSeek -- 000381E0
1103 -- PssWalkMarkerSeekToBeginning -- 000381C0
1104 -- PssWalkMarkerSetPosition -- 000381E0
1105 -- PssWalkMarkerTell -- 000381A0
1106 -- PssWalkSnapshot -- 00038200
1107 -- PulseEvent -- 00038220
1108 -- PurgeComm -- 000240C0
1109 -- QueryActCtxSettingsW -- 00038240
1110 -- QueryActCtxSettingsWWorker -- 00013F60
1111 -- QueryActCtxW -- 000198B0
1112 -- QueryActCtxWWorker -- 00013D10
1113 -- QueryDepthSList -- 0009D6B0 (forwarder -> NTDLL.RtlQueryDepthSList)
1114 -- QueryDosDeviceA -- 0005D590
1115 -- QueryDosDeviceW -- 00023E20
1116 -- QueryFullProcessImageNameA -- 00038260
1117 -- QueryFullProcessImageNameW -- 00038280
1118 -- QueryIdleProcessorCycleTime -- 000382C0
1119 -- QueryIdleProcessorCycleTimeEx -- 000382A0
1120 -- QueryInformationJobObject -- 00019D40
1121 -- QueryIoRateControlInformationJobObject -- 000563E0
1122 -- QueryMemoryResourceNotification -- 000382E0
1123 -- QueryPerformanceCounter -- 00017090
1124 -- QueryPerformanceFrequency -- 00019AE0
1125 -- QueryProcessAffinityUpdateMode -- 00038300
1126 -- QueryProcessCycleTime -- 00038320
1127 -- QueryProtectedPolicy -- 0009D836 (forwarder -> api-ms-win-core-processthreads-l1-1-2.QueryProtectedPolicy)
1128 -- QueryThreadCycleTime -- 000228E0
1129 -- QueryThreadProfiling -- 0003EE70
1130 -- QueryThreadpoolStackInformation -- 00038340
1131 -- QueryUnbiasedInterruptTime -- 00038370
1133 -- QueueUserAPC -- 00020300
1132 -- QueueUserAPC2 -- 0009D8E4 (forwarder -> api-ms-win-core-processthreads-l1-1-5.QueueUserAPC2)
1134 -- QueueUserWorkItem -- 0001F5C0
1135 -- QuirkGetData2Worker -- 00045BD0
1136 -- QuirkGetDataWorker -- 00045C80
1137 -- QuirkIsEnabled2Worker -- 00045D20
1138 -- QuirkIsEnabled3Worker -- 00018920
1139 -- QuirkIsEnabledForPackage2Worker -- 00045E50
1140 -- QuirkIsEnabledForPackage3Worker -- 00045E80
1141 -- QuirkIsEnabledForPackage4Worker -- 00045EC0
1142 -- QuirkIsEnabledForPackageWorker -- 00045F30
1143 -- QuirkIsEnabledForProcessWorker -- 00045FA0
1144 -- QuirkIsEnabledWorker -- 00014590
1145 -- RaiseException -- 00016EF0
1146 -- RaiseFailFastException -- 0009DA63 (forwarder -> kernelbase.RaiseFailFastException)
1147 -- RaiseInvalid16BitExeError -- 00036710
1148 -- ReOpenFile -- 00038390
1149 -- ReadConsoleA -- 000242B0
1150 -- ReadConsoleInputA -- 000242C0
1151 -- ReadConsoleInputExA -- 0009DADD (forwarder -> kernelbase.ReadConsoleInputExA)
1152 -- ReadConsoleInputExW -- 0009DB10 (forwarder -> kernelbase.ReadConsoleInputExW)
1153 -- ReadConsoleInputW -- 000242D0
1154 -- ReadConsoleOutputA -- 00024420
1155 -- ReadConsoleOutputAttribute -- 00024430
1156 -- ReadConsoleOutputCharacterA -- 00024440
1157 -- ReadConsoleOutputCharacterW -- 00024450
1158 -- ReadConsoleOutputW -- 00024460
1159 -- ReadConsoleW -- 000242E0
1160 -- ReadDirectoryChangesExW -- 000383B0
1161 -- ReadDirectoryChangesW -- 000383D0
1162 -- ReadFile -- 00023E30
1163 -- ReadFileEx -- 00023E40
1164 -- ReadFileScatter -- 00023E50
1165 -- ReadProcessMemory -- 000383F0
1166 -- ReadThreadProfilingData -- 0003EEA0
1167 -- ReclaimVirtualMemory -- 0009DC58 (forwarder -> api-ms-win-core-memory-l1-1-2.ReclaimVirtualMemory)
1168 -- RegCloseKey -- 00038410
1169 -- RegCopyTreeW -- 00038430
1170 -- RegCreateKeyExA -- 00038450
1171 -- RegCreateKeyExW -- 00038470
1172 -- RegDeleteKeyExA -- 00038490
1173 -- RegDeleteKeyExW -- 000384B0
1174 -- RegDeleteTreeA -- 000384D0
1175 -- RegDeleteTreeW -- 000384F0
1176 -- RegDeleteValueA -- 00038510
1177 -- RegDeleteValueW -- 00038530
1178 -- RegDisablePredefinedCacheEx -- 00038550
1179 -- RegEnumKeyExA -- 00038560
1180 -- RegEnumKeyExW -- 00038580
1181 -- RegEnumValueA -- 000385A0
1182 -- RegEnumValueW -- 000385C0
1183 -- RegFlushKey -- 000385E0
1184 -- RegGetKeySecurity -- 00038600
1185 -- RegGetValueA -- 00038620
1186 -- RegGetValueW -- 00038640
1187 -- RegLoadKeyA -- 00038660
1188 -- RegLoadKeyW -- 00038680
1189 -- RegLoadMUIStringA -- 000386A0
1190 -- RegLoadMUIStringW -- 000386C0
1191 -- RegNotifyChangeKeyValue -- 000386E0
1192 -- RegOpenCurrentUser -- 00038700
1193 -- RegOpenKeyExA -- 00038720
1194 -- RegOpenKeyExW -- 00020410
1195 -- RegOpenUserClassesRoot -- 00038740
1196 -- RegQueryInfoKeyA -- 00038760
1197 -- RegQueryInfoKeyW -- 00038780
1198 -- RegQueryValueExA -- 000387A0
1199 -- RegQueryValueExW -- 000387C0
1200 -- RegRestoreKeyA -- 000387E0
1201 -- RegRestoreKeyW -- 00038800
1202 -- RegSaveKeyExA -- 00038820
1203 -- RegSaveKeyExW -- 00038840
1204 -- RegSetKeySecurity -- 00038860
1205 -- RegSetValueExA -- 00038880
1206 -- RegSetValueExW -- 000388A0
1207 -- RegUnLoadKeyA -- 000388C0
1208 -- RegUnLoadKeyW -- 000388E0
1209 -- RegisterApplicationRecoveryCallback -- 0001F6D0
1210 -- RegisterApplicationRestart -- 00024BB0
1211 -- RegisterBadMemoryNotification -- 00038900
1212 -- RegisterConsoleIME -- 0003E4E0
1213 -- RegisterConsoleOS2 -- 00062B50
1214 -- RegisterConsoleVDM -- 000626A0
1215 -- RegisterWaitForInputIdle -- 00019870
1216 -- RegisterWaitForSingleObject -- 00019490
1217 -- RegisterWaitForSingleObjectEx -- 00038920
1218 -- RegisterWaitUntilOOBECompleted -- 00060A80
1219 -- RegisterWowBaseHandlers -- 00034B40
1220 -- RegisterWowExec -- 0003D0E0
1221 -- ReleaseActCtx -- 0001A010
1222 -- ReleaseActCtxWorker -- 000197C0
1223 -- ReleaseMutex -- 000239A0
1224 -- ReleaseMutexWhenCallbackReturns -- 0009E091 (forwarder -> NTDLL.TpCallbackReleaseMutexOnCompletion)
1225 -- ReleasePackageVirtualizationContext -- 000249C0
1226 -- ReleaseSRWLockExclusive -- 0009E0F6 (forwarder -> NTDLL.RtlReleaseSRWLockExclusive)
1227 -- ReleaseSRWLockShared -- 0009E12C (forwarder -> NTDLL.RtlReleaseSRWLockShared)
1228 -- ReleaseSemaphore -- 000239B0
1229 -- ReleaseSemaphoreWhenCallbackReturns -- 0009E17F (forwarder -> NTDLL.TpCallbackReleaseSemaphoreOnCompletion)
1230 -- RemoveDirectoryA -- 00023E60
1231 -- RemoveDirectoryTransactedA -- 00034190
1232 -- RemoveDirectoryTransactedW -- 0005BA40
1233 -- RemoveDirectoryW -- 00023E70
1234 -- RemoveDllDirectory -- 0009E217 (forwarder -> api-ms-win-core-libraryloader-l1-1-0.RemoveDllDirectory)
1235 -- RemoveLocalAlternateComputerNameA -- 000559F0
1236 -- RemoveLocalAlternateComputerNameW -- 00055A50
1237 -- RemoveSecureMemoryCacheCallback -- 00034B60
1238 -- RemoveVectoredContinueHandler -- 0009E2D1 (forwarder -> NTDLL.RtlRemoveVectoredContinueHandler)
1239 -- RemoveVectoredExceptionHandler -- 0009E317 (forwarder -> NTDLL.RtlRemoveVectoredExceptionHandler)
1240 -- ReplaceFile -- 00022AB0
1241 -- ReplaceFileA -- 0005B530
1242 -- ReplaceFileW -- 00022AB0
1243 -- ReplacePartitionUnit -- 00036960
1244 -- RequestDeviceWakeup -- 00035C30
1245 -- RequestWakeupLatency -- 00035C30
1246 -- ResetEvent -- 000239C0
1247 -- ResetWriteWatch -- 00016E90
1248 -- ResizePseudoConsole -- 000242F0
1249 -- ResolveDelayLoadedAPI -- 0009E3E8 (forwarder -> NTDLL.LdrResolveDelayLoadedAPI)
1250 -- ResolveDelayLoadsFromDll -- 0009E420 (forwarder -> NTDLL.LdrResolveDelayLoadsFromDll)
1251 -- ResolveLocaleName -- 00038940
1252 -- RestoreLastError -- 0009E465 (forwarder -> NTDLL.RtlRestoreLastWin32Error)
1253 -- ResumeThread -- 00019AC0
1254 -- RtlCaptureContext -- 00022FD0
1255 -- RtlCaptureStackBackTrace -- 0009E4BC (forwarder -> NTDLL.RtlCaptureStackBackTrace)
1256 -- RtlFillMemory -- 00038960
1257 -- RtlMoveMemory -- 0009E4F7 (forwarder -> NTDLL.RtlMoveMemory)
1258 -- RtlPcToFileHeader -- 00038980
1259 -- RtlUnwind -- 00016F00
1260 -- RtlZeroMemory -- 0009E535 (forwarder -> NTDLL.RtlZeroMemory)
1261 -- ScrollConsoleScreenBufferA -- 00024470
1262 -- ScrollConsoleScreenBufferW -- 00024480
1263 -- SearchPathA -- 00020390
1264 -- SearchPathW -- 0001F6B0
1265 -- SetCachedSigningLevel -- 000389A0
1266 -- SetCalendarInfoA -- 00048750
1267 -- SetCalendarInfoW -- 000389C0
1268 -- SetComPlusPackageInstallStatus -- 0003EAD0
1269 -- SetCommBreak -- 000240D0
1270 -- SetCommConfig -- 000240E0
1271 -- SetCommMask -- 000240F0
1272 -- SetCommState -- 00024100
1273 -- SetCommTimeouts -- 00024110
1274 -- SetComputerNameA -- 000389E0
1275 -- SetComputerNameEx2W -- 00038A00
1276 -- SetComputerNameExA -- 00038A20
1277 -- SetComputerNameExW -- 00038A40
1278 -- SetComputerNameW -- 00038A60
1279 -- SetConsoleActiveScreenBuffer -- 00024490
1280 -- SetConsoleCP -- 000244A0
1281 -- SetConsoleCtrlHandler -- 00024300
1282 -- SetConsoleCursor -- 00062740
1283 -- SetConsoleCursorInfo -- 000244B0
1284 -- SetConsoleCursorMode -- 00062BA0
1285 -- SetConsoleCursorPosition -- 000244C0
1286 -- SetConsoleDisplayMode -- 00024760
1287 -- SetConsoleFont -- 00062EC0
1288 -- SetConsoleHardwareState -- 00062790
1289 -- SetConsoleHistoryInfo -- 00024770
1290 -- SetConsoleIcon -- 00062F10
1291 -- SetConsoleInputExeNameA -- 0009E79C (forwarder -> kernelbase.SetConsoleInputExeNameA)
1292 -- SetConsoleInputExeNameW -- 0009E7D7 (forwarder -> kernelbase.SetConsoleInputExeNameW)
1293 -- SetConsoleKeyShortcuts -- 000627E0
1294 -- SetConsoleLocalEUDC -- 00062C00
1295 -- SetConsoleMaximumWindowSize -- 00062F60
1296 -- SetConsoleMenuClose -- 00062860
1297 -- SetConsoleMode -- 00024310
1298 -- SetConsoleNlsMode -- 00062CA0
1299 -- SetConsoleNumberOfCommandsA -- 00024780
1300 -- SetConsoleNumberOfCommandsW -- 00024790
1301 -- SetConsoleOS2OemFormat -- 00062CF0
1302 -- SetConsoleOutputCP -- 000244D0
1303 -- SetConsolePalette -- 000628B0
1304 -- SetConsoleScreenBufferInfoEx -- 000244E0
1305 -- SetConsoleScreenBufferSize -- 000244F0
1306 -- SetConsoleTextAttribute -- 00024500
1307 -- SetConsoleTitleA -- 00024510
1308 -- SetConsoleTitleW -- 00024520
1309 -- SetConsoleWindowInfo -- 00024530
1310 -- SetCriticalSectionSpinCount -- 0009E98D (forwarder -> NTDLL.RtlSetCriticalSectionSpinCount)
1311 -- SetCurrentConsoleFontEx -- 000247A0
1312 -- SetCurrentDirectoryA -- 00038A80
1313 -- SetCurrentDirectoryW -- 0001FC90
1314 -- SetDefaultCommConfigA -- 0003A370
1315 -- SetDefaultCommConfigW -- 0003A400
1316 -- SetDefaultDllDirectories -- 0009EA39 (forwarder -> api-ms-win-core-libraryloader-l1-1-0.SetDefaultDllDirectories)
1317 -- SetDllDirectoryA -- 0001FFB0
1318 -- SetDllDirectoryW -- 000229A0
1319 -- SetDynamicTimeZoneInformation -- 00038AA0
1320 -- SetEndOfFile -- 00023E80
1321 -- SetEnvironmentStringsA -- 00060BF0
1322 -- SetEnvironmentStringsW -- 00038AC0
1323 -- SetEnvironmentVariableA -- 00038AE0
1324 -- SetEnvironmentVariableW -- 0001F950
1325 -- SetErrorMode -- 00017070
1326 -- SetEvent -- 000239D0
1327 -- SetEventWhenCallbackReturns -- 0009EB54 (forwarder -> NTDLL.TpCallbackSetEventOnCompletion)
1328 -- SetFileApisToANSI -- 00038B00
1329 -- SetFileApisToOEM -- 00038B10
1330 -- SetFileAttributesA -- 00023E90
1331 -- SetFileAttributesTransactedA -- 0005C9D0
1332 -- SetFileAttributesTransactedW -- 0005CA10
1333 -- SetFileAttributesW -- 00023EA0
1334 -- SetFileBandwidthReservation -- 00034440
1335 -- SetFileCompletionNotificationModes -- 000227F0
1336 -- SetFileInformationByHandle -- 00023EB0
1337 -- SetFileIoOverlappedRange -- 00038B20
1338 -- SetFilePointer -- 00023EC0
1339 -- SetFilePointerEx -- 00023ED0
1340 -- SetFileShortNameA -- 00034540
1341 -- SetFileShortNameW -- 00034580
1342 -- SetFileTime -- 00023EE0
1343 -- SetFileValidData -- 00023EF0
1344 -- SetFirmwareEnvironmentVariableA -- 0005FC80
1345 -- SetFirmwareEnvironmentVariableExA -- 0005FCB0
1346 -- SetFirmwareEnvironmentVariableExW -- 0005FD70
1347 -- SetFirmwareEnvironmentVariableW -- 0005FE10
1348 -- SetHandleContext -- 0003E4E0
1349 -- SetHandleCount -- 000229E0
1350 -- SetHandleInformation -- 00023860
1351 -- SetInformationJobObject -- 000566A0
1352 -- SetIoRateControlInformationJobObject -- 00056800
1353 -- SetLastConsoleEventActive -- 0009EDE0 (forwarder -> kernelbase.SetLastConsoleEventActive)
1354 -- SetLastError -- 00014560
1355 -- SetLocalPrimaryComputerNameA -- 00055B50
1356 -- SetLocalPrimaryComputerNameW -- 00055BB0
1357 -- SetLocalTime -- 00038B40
1358 -- SetLocaleInfoA -- 00048800
1359 -- SetLocaleInfoW -- 00038B60
1360 -- SetMailslotInfo -- 0005BDE0
1361 -- SetMessageWaitingIndicator -- 00035C50
1362 -- SetNamedPipeAttribute -- 00035860
1363 -- SetNamedPipeHandleState -- 00038B80
1364 -- SetPriorityClass -- 0001E770
1365 -- SetProcessAffinityMask -- 0005F700
1366 -- SetProcessAffinityUpdateMode -- 00038BA0
1367 -- SetProcessDEPPolicy -- 0001F280
1368 -- SetProcessDefaultCpuSetMasks -- 0009EF46 (forwarder -> api-ms-win-core-processthreads-l1-1-6.SetProcessDefaultCpuSetMasks)
1369 -- SetProcessDefaultCpuSets -- 0009EFA2 (forwarder -> api-ms-win-core-processthreads-l1-1-3.SetProcessDefaultCpuSets)
1370 -- SetProcessDynamicEHContinuationTargets -- 0009F008 (forwarder -> api-ms-win-core-processthreads-l1-1-4.SetProcessDynamicEHContinuationTargets)
1371 -- SetProcessDynamicEnforcedCetCompatibleRanges -- 0009F082 (forwarder -> api-ms-win-core-processthreads-l1-1-6.SetProcessDynamicEnforcedCetCompatibleRanges)
1372 -- SetProcessInformation -- 000237F0
1373 -- SetProcessMitigationPolicy -- 0009F106 (forwarder -> api-ms-win-core-processthreads-l1-1-1.SetProcessMitigationPolicy)
1374 -- SetProcessPreferredUILanguages -- 00038BC0
1375 -- SetProcessPriorityBoost -- 00038BE0
1376 -- SetProcessShutdownParameters -- 00020150
1377 -- SetProcessWorkingSetSize -- 00023820
1378 -- SetProcessWorkingSetSizeEx -- 00038C00
1379 -- SetProtectedPolicy -- 0009F1E2 (forwarder -> api-ms-win-core-processthreads-l1-1-2.SetProtectedPolicy)
1380 -- SetSearchPathMode -- 00035740
1381 -- SetStdHandle -- 00038C40
1382 -- SetStdHandleEx -- 00038C20
1383 -- SetSystemFileCacheSize -- 00038C60
1384 -- SetSystemPowerState -- 0005FA70
1385 -- SetSystemTime -- 00038C80
1386 -- SetSystemTimeAdjustment -- 00034030
1387 -- SetTapeParameters -- 0003E320
1388 -- SetTapePosition -- 0003E370
1389 -- SetTermsrvAppInstallMode -- 00061010
1390 -- SetThreadAffinityMask -- 00061E50
1391 -- SetThreadContext -- 00038CA0
1392 -- SetThreadDescription -- 0009F311 (forwarder -> api-ms-win-core-processthreads-l1-1-3.SetThreadDescription)
1393 -- SetThreadErrorMode -- 00020230
1394 -- SetThreadExecutionState -- 00020290
1395 -- SetThreadGroupAffinity -- 00038CC0
1396 -- SetThreadIdealProcessor -- 00020170
1397 -- SetThreadIdealProcessorEx -- 00038CE0
1398 -- SetThreadInformation -- 00023800
1399 -- SetThreadLocale -- 0001F260
1400 -- SetThreadPreferredUILanguages -- 000188C0
1401 -- SetThreadPriority -- 000183D0
1402 -- SetThreadPriorityBoost -- 00038D00
1403 -- SetThreadSelectedCpuSetMasks -- 0009F449 (forwarder -> api-ms-win-core-processthreads-l1-1-6.SetThreadSelectedCpuSetMasks)
1404 -- SetThreadSelectedCpuSets -- 0009F4A5 (forwarder -> api-ms-win-core-processthreads-l1-1-3.SetThreadSelectedCpuSets)
1405 -- SetThreadStackGuarantee -- 00019F70
1406 -- SetThreadToken -- 0009F50B (forwarder -> api-ms-win-core-processthreads-l1-1-0.SetThreadToken)
1407 -- SetThreadUILanguage -- 00038D20
1408 -- SetThreadpoolStackInformation -- 00038D40
1409 -- SetThreadpoolThreadMaximum -- 0009F58D (forwarder -> NTDLL.TpSetPoolMaxThreads)
1410 -- SetThreadpoolThreadMinimum -- 00038D70
1411 -- SetThreadpoolTimer -- 0009F5D5 (forwarder -> NTDLL.TpSetTimer)
1412 -- SetThreadpoolTimerEx -- 0009F5FB (forwarder -> NTDLL.TpSetTimerEx)
1413 -- SetThreadpoolWait -- 0009F620 (forwarder -> NTDLL.TpSetWait)
1414 -- SetThreadpoolWaitEx -- 0009F644 (forwarder -> NTDLL.TpSetWaitEx)
1415 -- SetTimeZoneInformation -- 00038DA0
1416 -- SetTimerQueueTimer -- 0003ED90
1417 -- SetUnhandledExceptionFilter -- 00019850
1418 -- SetUserGeoID -- 0004E870
1419 -- SetUserGeoName -- 0004E890
1420 -- SetVDMCurrentDirectories -- 0003D140
1421 -- SetVolumeLabelA -- 0005FFD0
1422 -- SetVolumeLabelW -- 00060050
1423 -- SetVolumeMountPointA -- 0005E7F0
1424 -- SetVolumeMountPointW -- 0005E860
1425 -- SetVolumeMountPointWStub -- 00038DC0
1426 -- SetWaitableTimer -- 000239E0
1427 -- SetWaitableTimerEx -- 0009F758 (forwarder -> api-ms-win-core-synch-l1-1-0.SetWaitableTimerEx)
1428 -- SetXStateFeaturesMask -- 00038DE0
1429 -- SetupComm -- 00024120
1430 -- ShowConsoleCursor -- 00062920
1431 -- SignalObjectAndWait -- 00038E00
1432 -- SizeofResource -- 00021850
1433 -- Sleep -- 00018990
1434 -- SleepConditionVariableCS -- 0009F7FC (forwarder -> api-ms-win-core-synch-l1-2-0.SleepConditionVariableCS)
1435 -- SleepConditionVariableSRW -- 0009F84C (forwarder -> api-ms-win-core-synch-l1-2-0.SleepConditionVariableSRW)
1436 -- SleepEx -- 000239F0
1437 -- SortCloseHandle -- 00020320
1438 -- SortGetHandle -- 00015690
1439 -- StartThreadpoolIo -- 0009F8BB (forwarder -> NTDLL.TpStartAsyncIoOperation)
1440 -- SubmitThreadpoolWork -- 0009F8EE (forwarder -> NTDLL.TpPostWork)
1441 -- SuspendThread -- 00038E20
1442 -- SwitchToFiber -- 000241F0
1443 -- SwitchToThread -- 0001E8C0
1444 -- SystemTimeToFileTime -- 000194F0
1445 -- SystemTimeToTzSpecificLocalTime -- 0001FA10
1446 -- SystemTimeToTzSpecificLocalTimeEx -- 0009F981 (forwarder -> api-ms-win-core-timezone-l1-1-0.SystemTimeToTzSpecificLocalTimeEx)
1447 -- TerminateJobObject -- 00056A00
1448 -- TerminateProcess -- 000203F0
1449 -- TerminateThread -- 00038E40
1450 -- TermsrvAppInstallMode -- 0001AD70
1451 -- TermsrvConvertSysRootToUserDir -- 0001AB70
1452 -- TermsrvCreateRegEntry -- 000223D0
1453 -- TermsrvDeleteKey -- 000224C0
1454 -- TermsrvDeleteValue -- 00019560
1455 -- TermsrvGetPreSetValue -- 00021870
1456 -- TermsrvGetWindowsDirectoryA -- 00022900
1457 -- TermsrvGetWindowsDirectoryW -- 00019510
1458 -- TermsrvOpenRegEntry -- 00015620
1459 -- TermsrvOpenUserClasses -- 00019B00
1460 -- TermsrvRestoreKey -- 00061DD0
1461 -- TermsrvSetKeySecurity -- 00061E10
1462 -- TermsrvSetValueKey -- 000218A0
1463 -- TermsrvSyncUserIniFileExt -- 0001ACB0
1464 -- Thread32First -- 0005D3C0
1465 -- Thread32Next -- 0005D470
1466 -- TlsAlloc -- 00019480
1467 -- TlsFree -- 000193D0
1468 -- TlsGetValue -- 00011CB0
1469 -- TlsSetValue -- 00014540
1470 -- Toolhelp32ReadProcessMemory -- 000392B0
1471 -- TransactNamedPipe -- 00038E60
1472 -- TransmitCommChar -- 00024130
1473 -- TryAcquireSRWLockExclusive -- 0009FBD2 (forwarder -> NTDLL.RtlTryAcquireSRWLockExclusive)
1474 -- TryAcquireSRWLockShared -- 0009FC0E (forwarder -> NTDLL.RtlTryAcquireSRWLockShared)
1475 -- TryEnterCriticalSection -- 0009FC47 (forwarder -> NTDLL.RtlTryEnterCriticalSection)
1476 -- TrySubmitThreadpoolCallback -- 00038E80
1477 -- TzSpecificLocalTimeToSystemTime -- 0001F9F0
1478 -- TzSpecificLocalTimeToSystemTimeEx -- 0009FCC6 (forwarder -> api-ms-win-core-timezone-l1-1-0.TzSpecificLocalTimeToSystemTimeEx)
1479 -- UTRegister -- 00035770
1480 -- UTUnRegister -- 000357D0
1481 -- UnhandledExceptionFilter -- 00038EB0
1482 -- UnlockFile -- 00023F00
1483 -- UnlockFileEx -- 00023F10
1484 -- UnmapViewOfFile -- 000189A0
1485 -- UnmapViewOfFileEx -- 0009FD73 (forwarder -> api-ms-win-core-memory-l1-1-1.UnmapViewOfFileEx)
1486 -- UnregisterApplicationRecoveryCallback -- 0003EC10
1487 -- UnregisterApplicationRestart -- 00024BD0
1488 -- UnregisterBadMemoryNotification -- 00038ED0
1489 -- UnregisterConsoleIME -- 0003E4A0
1490 -- UnregisterWait -- 0001F470
1491 -- UnregisterWaitEx -- 000204D0
1492 -- UnregisterWaitUntilOOBECompleted -- 00060BA0
1493 -- UpdateCalendarDayOfWeek -- 00047570
1494 -- UpdateProcThreadAttribute -- 0009FE8E (forwarder -> api-ms-win-core-processthreads-l1-1-0.UpdateProcThreadAttribute)
1495 -- UpdateResourceA -- 00044980
1496 -- UpdateResourceW -- 00044AA0
1497 -- VDMConsoleOperation -- 00062FD0
1498 -- VDMOperationStarted -- 0003D3E0
1499 -- VerLanguageNameA -- 00038EF0
1500 -- VerLanguageNameW -- 00038F10
1501 -- VerSetConditionMask -- 0009FF4C (forwarder -> NTDLL.VerSetConditionMask)
1502 -- VerifyConsoleIoHandle -- 000625D0
1503 -- VerifyScripts -- 00038F30
1504 -- VerifyVersionInfoA -- 0001DCF0
1505 -- VerifyVersionInfoW -- 00017A80
1506 -- VirtualAlloc -- 00016280
1507 -- VirtualAllocEx -- 00038F70
1508 -- VirtualAllocExNuma -- 00038F50
1509 -- VirtualFree -- 000162A0
1510 -- VirtualFreeEx -- 00038F90
1511 -- VirtualLock -- 00038FB0
1512 -- VirtualProtect -- 00016E00
1513 -- VirtualProtectEx -- 00038FD0
1514 -- VirtualQuery -- 00016960
1515 -- VirtualQueryEx -- 00038FF0
1516 -- VirtualUnlock -- 000202E0
1517 -- WTSGetActiveConsoleSessionId -- 00019EC0
1518 -- WaitCommEvent -- 00024140
1519 -- WaitForDebugEvent -- 00039010
1520 -- WaitForDebugEventEx -- 000A00A0 (forwarder -> api-ms-win-core-debug-l1-1-2.WaitForDebugEventEx)
1521 -- WaitForMultipleObjects -- 00023A00
1522 -- WaitForMultipleObjectsEx -- 00023A10
1523 -- WaitForSingleObject -- 00023A20
1524 -- WaitForSingleObjectEx -- 00023A30
1525 -- WaitForThreadpoolIoCallbacks -- 000A0148 (forwarder -> NTDLL.TpWaitForIoCompletion)
1526 -- WaitForThreadpoolTimerCallbacks -- 000A0184 (forwarder -> NTDLL.TpWaitForTimer)
1527 -- WaitForThreadpoolWaitCallbacks -- 000A01B8 (forwarder -> NTDLL.TpWaitForWait)
1528 -- WaitForThreadpoolWorkCallbacks -- 000A01EB (forwarder -> NTDLL.TpWaitForWork)
1529 -- WaitNamedPipeA -- 0005AB80
1530 -- WaitNamedPipeW -- 00039030
1531 -- WakeAllConditionVariable -- 000A0236 (forwarder -> NTDLL.RtlWakeAllConditionVariable)
1532 -- WakeConditionVariable -- 000A026E (forwarder -> NTDLL.RtlWakeConditionVariable)
1533 -- WerGetFlags -- 0003EC30
1534 -- WerGetFlagsWorker -- 0003EC30
1535 -- WerRegisterAdditionalProcess -- 00039050
1536 -- WerRegisterAppLocalDump -- 00039070
1537 -- WerRegisterCustomMetadata -- 00039090
1538 -- WerRegisterExcludedMemoryBlock -- 000390B0
1539 -- WerRegisterFile -- 00022930
1540 -- WerRegisterFileWorker -- 0003EC50
1541 -- WerRegisterMemoryBlock -- 000390D0
1542 -- WerRegisterMemoryBlockWorker -- 0003EC70
1543 -- WerRegisterRuntimeExceptionModule -- 0001A0B0
1544 -- WerRegisterRuntimeExceptionModuleWorker -- 0003EC90
1545 -- WerSetFlags -- 000189E0
1546 -- WerSetFlagsWorker -- 000189E0
1547 -- WerUnregisterAdditionalProcess -- 000390F0
1548 -- WerUnregisterAppLocalDump -- 00039110
1549 -- WerUnregisterCustomMetadata -- 00039120
1550 -- WerUnregisterExcludedMemoryBlock -- 00039140
1551 -- WerUnregisterFile -- 00039160
1552 -- WerUnregisterFileWorker -- 0003ECB0
1553 -- WerUnregisterMemoryBlock -- 00039180
1554 -- WerUnregisterMemoryBlockWorker -- 0003ECD0
1555 -- WerUnregisterRuntimeExceptionModule -- 000391A0
1556 -- WerUnregisterRuntimeExceptionModuleWorker -- 0003ECF0
1557 -- WerpGetDebugger -- 000644E0
1558 -- WerpInitiateRemoteRecovery -- 0003ED10
1559 -- WerpLaunchAeDebug -- 00064CB0
1560 -- WerpNotifyLoadStringResourceWorker -- 00018420
1561 -- WerpNotifyUseStringResourceWorker -- 0003ED20
1562 -- WideCharToMultiByte -- 00014990
1563 -- WinExec -- 0005F740
1564 -- Wow64DisableWow64FsRedirection -- 0001F0B0
1565 -- Wow64EnableWow64FsRedirection -- 00024170
1566 -- Wow64GetThreadContext -- 00024BE0
1567 -- Wow64GetThreadSelectorEntry -- 000340F0
1568 -- Wow64RevertWow64FsRedirection -- 0001F090
1569 -- Wow64SetThreadContext -- 00024C00
1570 -- Wow64SuspendThread -- 00024C20
3 -- Wow64Transition -- 0008209C
1571 -- WriteConsoleA -- 00024320
1572 -- WriteConsoleInputA -- 00024540
1573 -- WriteConsoleInputVDMA -- 00062960
1574 -- WriteConsoleInputVDMW -- 000629D0
1575 -- WriteConsoleInputW -- 00024550
1576 -- WriteConsoleOutputA -- 00024560
1577 -- WriteConsoleOutputAttribute -- 00024570
1578 -- WriteConsoleOutputCharacterA -- 00024580
1579 -- WriteConsoleOutputCharacterW -- 00024590
1580 -- WriteConsoleOutputW -- 000245A0
1581 -- WriteConsoleW -- 00024330
1582 -- WriteFile -- 00023F20
1583 -- WriteFileEx -- 00023F30
1584 -- WriteFileGather -- 00023F40
1585 -- WritePrivateProfileSectionA -- 0005A0C0
1586 -- WritePrivateProfileSectionW -- 0005A110
1587 -- WritePrivateProfileStringA -- 0001FA30
1588 -- WritePrivateProfileStringW -- 000201F0
1589 -- WritePrivateProfileStructA -- 0005A160
1590 -- WritePrivateProfileStructW -- 0005A2B0
1591 -- WriteProcessMemory -- 000391C0
1592 -- WriteProfileSectionA -- 0005A410
1593 -- WriteProfileSectionW -- 0005A430
1594 -- WriteProfileStringA -- 0005A450
1595 -- WriteProfileStringW -- 0005A470
1596 -- WriteTapemark -- 0003E3D0
1597 -- ZombifyActCtx -- 000391E0
1598 -- ZombifyActCtxWorker -- 0003EA60
1599 -- _hread -- 00014A10
1600 -- _hwrite -- 00061F50
1601 -- _lclose -- 000170E0
1602 -- _lcreat -- 00061EC0
1603 -- _llseek -- 000149C0
1604 -- _lopen -- 00061F00
1605 -- _lread -- 00014A10
1606 -- _lwrite -- 00061F50
1607 -- lstrcat -- 0001F540
1608 -- lstrcatA -- 0001F540
1609 -- lstrcatW -- 00061FA0
1610 -- lstrcmp -- 00039200
1611 -- lstrcmpA -- 00021720
1612 -- lstrcmpW -- 00016940
1613 -- lstrcmpi -- 00039220
1614 -- lstrcmpiA -- 00016240
1615 -- lstrcmpiW -- 000168C0
1616 -- lstrcpy -- 0001EB00
1617 -- lstrcpyA -- 0001EB00
1618 -- lstrcpyW -- 00062120
1619 -- lstrcpyn -- 0001FF60
1620 -- lstrcpynA -- 0001FF60
1621 -- lstrcpynW -- 00020350
1622 -- lstrlen -- 00017100
1623 -- lstrlenA -- 00017100
1624 -- lstrlenW -- 00014A50
1625 -- timeBeginPeriod -- 0001FB90
1626 -- timeEndPeriod -- 0001FA90
1627 -- timeGetDevCaps -- 00062190
1628 -- timeGetSystemTime -- 000621E0
1629 -- timeGetTime -- 0001D370
PS Z:\win\le-format-pe\Debug>
```

Les src du projet sont dispo sur : {{< newtabref href="https://gitlab.com/sysc4ll/le-format-pe/" title="https://gitlab.com/sysc4ll/le-format-pe/" >}}
