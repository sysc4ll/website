---
title: "irc-socket-buffering-parsing"
slug: "irc-socket-buffering-parsing"
description: "Gestion des sockets Windows && UNIX, parseur IRC et buffering"
tags: ["c", "linux", "windows"]
categories: ["coding", "networking"]
layout: "post-without-toc"
---

# irc-socket-buffering-parsing

Un bot IRC tout simple.

Le code se compose de la gestion des sockets Windows && UNIX avec l'utilisation de la fonction select() pour ne pas rester bloqué sur les fonctions recv() : 
{{< newtabref href="https://gitlab.com/sysc4ll/irc-socket-buffering-parsing/-/blob/main/socket.c" title="socket.c" >}}

D'un parseur {{< newtabref href="http://abcdrfc.free.fr/rfc-vf/rfc1459.html" title="de lignes IRC" >}} :
{{< newtabref href="https://gitlab.com/sysc4ll/irc-socket-buffering-parsing/-/blob/main/parser.c" title="parser.c" >}}

Et d'une certaine technique de buffering (buffer circulaire) :
{{< newtabref href="https://gitlab.com/sysc4ll/irc-socket-buffering-parsing/-/blob/main/parser.c#L3" title="set_irc_buffer()" >}}

Ici : {{< newtabref href="https://gitlab.com/sysc4ll/irc-socket-buffering-parsing/-/blob/main/main.c#L103" title="https://gitlab.com/sysc4ll/irc-socket-buffering-parsing/-/blob/main/main.c#L103" >}}
se trouve la partie où nous écoutons les commandes, dans l'exemple en commentaire : On regarde si la commande **"!join"** est exécutée en privé :

```c {linenos=false}
if (!strncmp(pline.trailing, "!join ", 6))
```

Comme expliqué dans le commentaire, je laisse le choix pour la gestion des commandes :
* directement dans le même code : une recompilation est donc nécessaire à chaque ajout de commande
* dans un programme externe avec {{< newtabref href="https://man7.org/linux/man-pages/man3/popen.3.html" title="popen()" >}}
* dans une lib chargée dynamiquement avec {{< newtabref href="https://man7.org/linux/man-pages/man3/dlsym.3.html" title="dlsym()" >}}
