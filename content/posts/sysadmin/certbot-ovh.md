---
title: "générer mes certificats ssl sur linux avec certbot et ovh"
slug: "generate-ssl-certbot-ovh"
description: "comment générer mes certificats ssl sur linux avec certbot et ovh"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-without-toc"
---

## generate ssl

Pour générer mes certificats SSL je vais utiliser
{{< newtabref href="https://certbot-dns-ovh.readthedocs.io/" title="l'API d'OVH" >}} :

```sh {linenos=false}
sudo apt-get install certbot python3-certbot-dns-ovh
```

Pour obtenir les identifiants OVH, je me rends sur
{{< newtabref href="https://eu.api.ovh.com/createToken/" title="https://eu.api.ovh.com/createToken/" >}}.

Je prépare le fichier d'identification OVH **/root/.certbot/ovh-freec0ding.ini** :

```sh {linenos=false}
sudo mkdir -p /root/.certbot
```

```sh
export OVH_APPLICATION_KEY='XXXXXXXXXXXXXXXX'
export OVH_APPLICATION_SECRET='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
export OVH_CONSUMER_KEY='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

```sh
sudo sh -c "cat > /root/.certbot/ovh-freec0ding.ini << EOF
# OVH API credentials used by certbot
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = ${OVH_APPLICATION_KEY}
dns_ovh_application_secret = ${OVH_APPLICATION_SECRET}
dns_ovh_consumer_key = ${OVH_CONSUMER_KEY}
EOF"
```

Je définis les droits du fichier **/root/.certbot/ovh-freec0ding.ini** :

```sh {linenos=false}
sudo chmod 600 /root/.certbot/ovh-freec0ding.ini
```

Je génère les certificats SSL :

```sh
sudo certbot certonly \
--dns-ovh \
--dns-ovh-credentials /root/.certbot/ovh-freec0ding.ini \
-d freec0ding.dev \
-d *.freec0ding.dev \
-d *.pages.freec0ding.dev \
--non-interactive \
--agree-tos \
--email contact@freec0ding.dev
```
