---
title: "installer dpaste avec nginx et docker sur linux"
slug: "install-dpaste-nginx-docker"
description: "article sur comment installer dpaste avec nginx et docker sur debian"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-with-toc"
---

## intro

Dans cet article, je vais détailler comment installer
{{< newtabref href="https://hub.docker.com/r/darrenofficial/dpaste" title="dpaste" >}}
avec {{< newtabref href="https://hub.docker.com/_/nginx" title="nginx" >}}
en utilisant {{< newtabref href="https://www.docker.com/" title="docker" >}} sur linux.

## postgresql

Pour installer la base de données, voir : [comment installer mysql et postgresql avec docker sur linux](/install-mysql-postgresql-docker).

Pour **dpaste** je vais utiliser la base de données {{< newtabref href="https://hub.docker.com/_/postgres/" title="PostgreSQL" >}} : \
Je crée la base de données pour **dpaste** :

```sh {linenos=false}
sudo docker exec -it postgresql-container psql -U postgres
```

```sql
CREATE USER dpaste WITH ENCRYPTED PASSWORD 'example-password';
CREATE DATABASE dpastedb OWNER dpaste;
```

## dpaste

Le fichier **docker-compose** pour **dpaste** : **dpaste-docker-compose.yml** :

```yml
networks:
  dpaste_network:
    name: dpaste-network
  sql_network:
    name: sql-network
    external: true

services:
  dpaste:
    image: darrenofficial/dpaste:latest
    container_name: dpaste-container
    restart: always
    networks:
      - dpaste_network
      - sql_network
    environment:
      DATABASE_URL: postgres://dpaste:example-password@postgresql/dpastedb
```

Je crée le container **dpaste-container** :

```sh {linenos=false}
sudo docker-compose -p dpaste -f ./dpaste-docker-compose.yml up -d
```

## nginx

Pour installer nginx, voir : [comment installer nginx avec docker sur linux](/install-nginx-docker).

Le fichier de config **dpaste.conf** dans le container web **/srv/web/nginx/conf/conf.d/** :

```nginx
# dpaste

server {
  listen 80;
  listen [::]:80;

  server_name paste.freec0ding.dev;

  access_log /var/log/nginx/paste_access.log;
  error_log /var/log/nginx/paste_error.log;

  server_tokens off;

  return 301 https://$http_host$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name paste.freec0ding.dev;

  ssl_certificate /etc/letsencrypt/live/freec0ding.dev/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/freec0ding.dev/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/freec0ding.dev/chain.pem;

  access_log /var/log/nginx/paste_access.log;
  error_log /var/log/nginx/paste_error.log;

  server_tokens off;

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    proxy_pass http://dpaste:8000;
  }
}
```

Je restart le container nginx :

```sh {linenos=false}
sudo docker exec nginx-container service nginx restart
```

Le résulta :

{{< image-popup src="/dpaste-empty.png" style="max-height=200px" class="img-fluid" title="dpaste-empty" >}}
\
\
{{< image-popup src="/dpaste-hello-world.png" style="max-height=200px" class="img-fluid" title="dpaste-hello-world" >}}
