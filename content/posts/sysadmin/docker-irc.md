---
title: "installer inspircd et anope avec docker sur linux"
slug: "install-inspircd-anope-docker"
description: "comment installer inspircd et anope avec docker sur linux"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-with-toc"
---

## intro

Je vais détailler ici comment installer
{{< newtabref href="https://hub.docker.com/r/inspircd/inspircd-docker" title="inspircd" >}}
et {{< newtabref href="https://hub.docker.com/r/anope/anope" title="anope" >}} avec
{{< newtabref href="https://www.docker.com/" title="docker" >}} sur linux.

## ssl

voir : [comment générer mes certificats ssl sur linux avec certbot et ovh](/generate-ssl-certbot-ovh).

## anope database

Pour installer la base de données, voir : [comment installer mysql et postgresql avec docker sur linux](/install-mysql-postgresql-docker).

Je crée la base de données pour anope :

```sh {linenos=false}
sudo docker exec -it mysql-container bash
```

```sh {linenos=false}
mysql -u root -p
```

```sql
CREATE DATABASE IF NOT EXISTS anopedb;
CREATE USER 'anope'@'%' IDENTIFIED BY 'example-password';
GRANT ALL PRIVILEGES ON anopedb.* TO 'anope'@'%';
EXIT;
```

## irc-docker-compose

Je crée les dossiers utilisés pour les containers
**inspircd-container** et **anope-container** :

```sh
sudo mkdir -p /srv/irc/inspircd/conf /srv/irc/inspircd/log \
    /srv/irc/anope/conf /srv/irc/anope/data
```

Le fichier **irc-docker-compose.yml** :

```yml
volumes:
  inspircd_conf:
    name: inspircd-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/irc/inspircd/conf

  inspircd_log:
    name: inspircd-log
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/irc/inspircd/log

  anope_conf:
    name: anope-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/irc/anope/conf

  anope_data:
    name: anope-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/irc/anope/data
  
  irc_ssl:
    name: irc-ssl
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /etc/letsencrypt

networks:
  irc_network:
    name: irc-network

  sql_network:
    name: sql-network
    external: true

services:
  inspircd:
    image: inspircd/inspircd-docker:latest
    container_name: inspircd-container
    restart: always
    volumes:
      - inspircd_conf:/inspircd/conf
      - inspircd_log:/inspircd/logs
      - irc_ssl:/etc/letsencrypt
    networks:
      - irc_network
    ports:
      - '6697:6697'
    env_file:
      - inspircd.env

  anope:
    image: anope/anope:latest
    container_name: anope-container
    restart: always
    volumes:
      - anope_conf:/anope/conf
      - anope_data:/data
    networks:
      - irc_network
      - sql_network
    env_file:
      - anope.env
```

Mon fichier **inspircd.env** :

```sh
INSP_NET_SUFFIX=".freec0ding.dev"
INSP_NET_NAME="freec0ding.dev"
INSP_SERVER_NAME="irc.freec0ding.dev"
INSP_ADMIN_NAME="sysc4ll"
INSP_ADMIN_NICK="sysc4ll"
INSP_ADMIN_EMAIL="hello@sysc4ll.sh"
INSP_ENABLE_DNSBL="yes"

INSP_OPER_NAME="sysc4ll"
INSP_OPER_PASSWORD_HASH="DRY66hP/GJUy+6bxU2LYcQfLHZAclfbpHA2x81Zk14s\$r0TfEsLnCmTcFOS6Dbw1ZB4xXsF1mb/Sy/Of+bPJIqY"
INSP_OPER_HOST="*@*"
INSP_OPER_HASH="hmac-sha256"
INSP_OPER_SSLONLY="yes"

INSP_SERVICES_NAME="services.freec0ding.dev"
INSP_SERVICES_IPADDR="anope"
INSP_SERVICES_HIDDEN="no"
INSP_SERVICES_PASSWORD="example-password"

INSP_TLS_CN="irc.freec0ding.dev"
INSP_TLS_MAIL="contact@freec0ding.dev"
INSP_TLS_UNIT="freec0ding.dev"
INSP_TLS_ORG="freec0ding.dev"
INSP_TLS_LOC=""
INSP_TLS_STATE="france"
INSP_TLS_COUNTRY="fr"
```

Mon fichier **anope.env** :

```sh
ANOPE_SERVICES_NAME="services.freec0ding.dev"
ANOPE_SERVICES_VHOST="services.freec0ding.dev"
ANOPE_UPLINK_IP="inspircd"
ANOPE_UPLINK_PORT="7000"
ANOPE_UPLINK_PASSWORD="example-password"

ANOPE_SQL_ENGINE="mysql"
ANOPE_MYSQL_DB="anopedb"
ANOPE_MYSQL_HOST="mysql"
ANOPE_MYSQL_PORT="3306"
ANOPE_MYSQL_USER="anope"
ANOPE_MYSQL_PASSWORD="example-password"
ANOPE_SQL_LIVE="yes"
```

## docker up

Je crée les containers **inspircd-container** et **anope-container** :

```sh {linenos=false}
sudo docker-compose -p irc -f ./irc-docker-compose.yml up -d
```

## config

Je sauvegarde les fichiers de config **/srv/irc/inspircd/conf/inspircd.conf**
et **/srv/irc/inspircd/conf/modules.conf**

```sh
sudo cp /srv/irc/inspircd/conf/inspircd.conf \
/srv/irc/inspircd/conf/inspircd.conf.orig

sudo cp /srv/irc/inspircd/conf/modules.conf \
/srv/irc/inspircd/conf/modules.conf.orig
```

Je configure inspircd en éditant le fichier **/srv/irc/inspircd/conf/modules.conf** :

```txt
<module name="md5">
<module name="cloaking">
<cloak mode="full"
       key="changeme" # openssl rand -base64 30
       prefix="net-"
       ignorecase="no">

<module name="conn_join">
<autojoin channel="#home">
```

Je restart inspircd :

```sh {linenos=false}
sudo docker restart inspircd-container
```

J'enregistre mon pseudo :

```sh {linenos=false}
/MSG nickserv register example-password hello@sysc4ll.sh
```

J'enregistre le salon **#home** :

```sh {linenos=false}
/MSG chanserv register #home
```

## end

Le 15 avril 2024, le container anope n'attend pas le container mysql pour démarrer.\
Il démarre sans base de données.

J'ai essayé plusieurs outils genre {{< newtabref href="https://wait4x.dev/" title="wait4x" >}}
mais sans succès.

Au reboot du serveur et après le démarrage du container mysql, je suis donc obligé de restart
le container anope avec la commande \
"**sudo docker restart anope-container**" pour qu'il prenne en charge la base de données.
