---
title: "installer matrix / synapse avec docker sur linux"
slug: "install-matrix-synapse-docker"
description: "comment installer le serveur matrix : synapse avec docker sur linux"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-with-toc"
---

## intro

Ici, je vais détailler l'installation de
{{< newtabref href="https://github.com/element-hq/synapse" title="synapse" >}},
le serveur {{< newtabref href="https://matrix.org/" title="matrix" >}} sur linux.

## database

Pour installer la base de données, voir : [comment installer mysql et postgresql avec docker sur linux](/install-mysql-postgresql-docker).

Je crée la base de données pour **synapse** :
{{< newtabref href="https://matrix-org.github.io/synapse/latest/postgres.html" title="Set up database" >}} :

```sh {linenos=false}
sudo docker exec -it postgresql-container bash
```

```sh {linenos=false}
su - postgres
```

```sh
createuser --pwprompt synapse_user
createdb --encoding=UTF8 --locale=C --template=template0 --owner=synapse_user synapse
```

## matrix

src : {{< newtabref href="https://hub.docker.com/r/matrixdotorg/synapse" title="https://hub.docker.com/r/matrixdotorg/synapse" >}} :

Je crée le dossier utilisé pour le serveur synapse :

```sh {linenos=false}
sudo mkdir -p /srv/matrix/data
```

Le fichier **docker-compose** pour matrix : **matrix-docker-compose.yml** :

```yml
volumes:
  matrix_data:
    name: matrix-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/matrix/data

networks:
  matrix_network:
    name: matrix-network
  sql_network:
    name: sql-network
    external: true

services:
  synapse:
    image: matrixdotorg/synapse:latest
    container_name: synapse-container
    restart: always
    volumes:
      - matrix_data:/data
    networks:
      - matrix_network
      - sql_network
    environment:
      - SYNAPSE_SERVER_NAME=matrix.freec0ding.dev
      - SYNAPSE_REPORT_STATS=no
```

Je génère le fichier de configuration pour synapse :

```sh {linenos=false}
sudo docker-compose -p matrix -f ./matrix-docker-compose.yml run --rm synapse generate
```

Je backup le fichier de config pour synapse :

```sh {linenos=false}
sudo cp /srv/matrix/data/homeserver.yaml /srv/matrix/data/homeserver.yaml.orig
```

J'édite le fichier de config **/srv/matrix/data/homeserver.yaml**
pour utiliser **postgresql** :

```yml
database:
  name: psycopg2
  args:
    user: "synapse_user"
    password: "example-password"
    dbname: "synapse"
    host: "postgresql"
    cp_min: 5
    cp_max: 10
```

Je crée le container **synapse-container** :

```sh {linenos=false}
sudo docker-compose -p matrix -f ./matrix-docker-compose.yml up -d
```

Je crée un utilisateur admin pour le serveur synapse :

```sh {linenos=false}
sudo docker exec -it synapse-container bash
```

```sh {linenos=false}
register_new_matrix_user http://localhost:8008 -c /data/homeserver.yaml --user sysc4ll --password example-password
```

## nginx

Pour installer nginx, voir : [comment installer nginx avec docker sur linux](/install-nginx-docker).

Le fichier de config **synapse.conf** dans le container web :
**/srv/web/nginx/conf/conf.d/synapse.conf** :

src : {{< newtabref href="https://element-hq.github.io/synapse/latest/reverse_proxy.html#nginx" title="synapse reverse_proxy.html#nginx" >}}

```nginx
server {
    listen 443 ssl;
    listen [::]:443 ssl;

    # For the federation port
    listen 8448 ssl default_server;
    listen [::]:8448 ssl default_server;

    server_name matrix.freec0ding.dev;

    ssl_certificate /etc/letsencrypt/live/freec0ding.dev/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/freec0ding.dev/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/freec0ding.dev/chain.pem;

    location ~ ^(/_matrix|/_synapse/client) {
        # note: do not add a path (even a single /) after the port in `proxy_pass`,
        # otherwise nginx will canonicalise the URI and cause signature verification
        # errors.
        proxy_pass http://synapse:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;
    
        # Synapse responses may be chunked, which is an HTTP/1.1 feature.
        proxy_http_version 1.1;
    }
}
```

Je restart le container nginx :

```sh {linenos=false}
sudo docker exec nginx-container service nginx restart
```

Je peux maintenant me connecter avec un client comme :
{{< newtabref href="https://element.io/" title="https://element.io/" >}}
sur **matrix.freec0ding.dev** en utilisant **sysc4ll** comme username et **example-password** comme mot de passe.
