---
title: "installer nginx avec docker sur linux"
slug: "install-nginx-docker"
description: "comment installer nginx avec docker sur linux"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-without-toc"
---

## generate ssl

voir : [comment générer mes certificats ssl sur linux avec certbot et ovh](/generate-ssl-certbot-ovh).

## web container

Je **crée les dossiers** pour le **container web** :

```sh {linenos=false}
sudo mkdir -p /srv/web/nginx/conf /srv/web/nginx/log /srv/web/www-data
```

Le fichier **docker-compose** pour le **container web** : **web-docker-compose.yml** :

```yml
volumes:
  nginx_conf:
    name: nginx-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/web/nginx/conf
  nginx_log:
    name: nginx-log
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/web/nginx/log
  www_data:
    name: www-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/web/www-data
  www_ssl:
    name: www-ssl
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /etc/letsencrypt

networks:
  web_network:
    name: web-network
  dpaste_network:
    name: dpaste-network
    external: true
  matrix_network:
    name: matrix-network
    external: true
  gitlab_network:
    name: gitlab-network
    external: true

services:
  nginx:
    image: nginx:latest
    container_name: nginx-container
    restart: always
    volumes:
      - nginx_conf:/etc/nginx
      - nginx_log:/var/log/nginx
      - www_data:/usr/share/nginx/html
      - www_ssl:/etc/letsencrypt
    networks:
      - web_network
      - dpaste_network
      - matrix_network
      - gitlab_network
    ports:
      - '80:80'
      - '443:443'
```

Je crée le **container web** :

```sh {linenos=false}
sudo docker-compose -p web -f ./web-docker-compose.yml up -d
```
