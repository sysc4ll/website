---
title: "installer mysql et postgresql avec docker sur linux"
slug: "install-mysql-postgresql-docker"
description: "comment installer mysql et postgresql avec docker sur linux"
tags: ["linux", "docker", "freec0ding"]
categories: ["sysadmin"]
layout: "post-without-toc"
---

## docker-compose

Mon fichier **sql-docker-compose.yml** :

```yml
volumes:
  mysql_conf:
    name: mysql-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/sql/mysql/conf

  mysql_data:
    name: mysql-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/sql/mysql/data

  postgresql_data:
    name: postgresql-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/sql/postgresql/data

networks:
  sql_network:
    name: sql-network

services:
  mysql:
    image: mysql:latest
    container_name: mysql-container
    restart: always
    volumes:
      - mysql_conf:/etc/mysql/conf.d
      - mysql_data:/var/lib/mysql
    networks:
      - sql_network
    environment:
      MYSQL_ROOT_PASSWORD: example-password

  postgresql:
    image: postgres:latest
    container_name: postgresql-container
    restart: always
    volumes:
      - postgresql_data:/var/lib/postgresql/data
    networks:
      - sql_network
    environment:
      POSTGRES_PASSWORD: example-password
```

Je crée les dossiers utilisés par les containers **mysql-container** et **postgresql-container** :

```sh {linenos=false}
sudo mkdir -p /srv/sql/mysql/conf /srv/sql/mysql/data /srv/sql/postgresql/data
```

Je crée les containers **mysql-container** et **postgresql-container** :

```sh {linenos=false}
sudo docker-compose -p sql -f ./sql-docker-compose.yml up -d
```
