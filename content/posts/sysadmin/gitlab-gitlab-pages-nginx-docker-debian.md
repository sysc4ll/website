---
title: "gitlab et gitlab-pages avec nginx et docker sur debian"
slug: "gitlab-gitlab-pages-nginx-docker-debian"
description: "article sur comment installer gitlab et gitlab-pages avec nginx et docker sur debian"
tags: ["linux", "debian", "docker"]
categories: ["sysadmin"]
layout: "post-with-toc"
---

## intro

Dans cet article, je vais détailler comment installer
{{< newtabref href="https://hub.docker.com/r/gitlab/gitlab-ce" title="gitlab" >}}
et {{< newtabref href="https://docs.gitlab.com/ee/user/project/pages/" title="gitlab-pages" >}}
avec {{< newtabref href="https://hub.docker.com/_/nginx" title="nginx" >}}
en utilisant {{< newtabref href="https://www.docker.com/" title="docker" >}} sur
{{< newtabref href="https://www.debian.org/" title="debian" >}}

## ssh

Je définis le **port SSH du serveur sur 2222** :

```sh
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
sudo sed -i 's/^#\?Port 22/Port 2222/' /etc/ssh/sshd_config
```

Pour **appliquer cette configuration**, je **reboot** :

```sh {linenos=false}
sudo reboot
```

Pour la suite,
je me reconnecte au ssh du serveur freec0ding.dev sur le port **2222**.

## docker

Installation de **docker** sur **debian** :

```sh {linenos=false}
sudo apt-get install docker docker-compose
```

## ssl

voir : [comment générer mes certificats ssl sur linux avec certbot et ovh](/generate-ssl-certbot-ovh).

## gitlab

je **crée les dossiers** pour les containers **gitlab et gitlab-runner** :

```sh
sudo mkdir -p \
/srv/gitlab/conf \
/srv/gitlab/log \
/srv/gitlab/data \
/srv/gitlab-runner/conf
```

Le fichier **docker-compose** pour
**gitlab et gitlab-runner** : **gitlab-docker-compose.yml** :

```yml
volumes:
  gitlab_conf:
    name: gitlab-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/gitlab/conf
  gitlab_log:
    name: gitlab-log
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/gitlab/log
  gitlab_data:
    name: gitlab-data
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/gitlab/data
  gitlab_runner_conf:
    name: gitlab-runner-conf
    driver: local
    driver_opts:
      type: none
      o: bind
      device: /srv/gitlab-runner/conf

networks:
  gitlab_network:
    name: gitlab-network

services:
  gitlab:
    image: gitlab/gitlab-ce:latest
    container_name: gitlab-container
    restart: always
    volumes:
      - gitlab_conf:/etc/gitlab
      - gitlab_log:/var/log/gitlab
      - gitlab_data:/var/opt/gitlab
    networks:
      - gitlab_network
    ports:
      - '22:22'
  gitlab_runner:
    container_name: gitlab-runner-container
    image: gitlab/gitlab-runner:latest
    restart: always
    volumes:
      - gitlab_runner_conf:/etc/gitlab-runner
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - gitlab_network
```

Je **crée les containers gitlab et gitlab-runner** :

```sh {linenos=false}
sudo docker-compose -p gitlab -f ./gitlab-docker-compose.yml up -d
```

Je **sauvegarde le fichier de config gitlab.rb** :

```sh {linenos=false}
sudo cp /srv/gitlab/conf/gitlab.rb /srv/gitlab/conf/gitlab.rb.orig
```

J'édite le fichier de config **/srv/gitlab/conf/gitlab.rb** :

```rb
external_url 'https://gitlab.freec0ding.dev'

gitlab_rails['gitlab_ssh_host'] = 'freec0ding.dev'

gitlab_rails['incoming_email_enabled'] = false

nginx['enable'] = true
nginx['listen_port'] = 18080

pages_external_url 'http://pages.freec0ding.dev'

gitlab_pages['enable'] = true
gitlab_pages['external_http'] = [':28080']

pages_nginx['enable'] = false
```

Je reconfig gitlab :

```sh {linenos=false}
sudo docker exec gitlab-container gitlab-ctl reconfigure
```

Pour pouvoir utiliser ssh
{{< newtabref href="https://docs.gitlab.com/ee/user/ssh.html" title="Use SSH keys to communicate with GitLab" >}}

## web

Pour installer nginx, voir : [comment installer nginx avec docker sur linux](/install-nginx-docker).

Le **fichier de config nginx** pour **/srv/web/nginx/conf/conf.d/gitlab.conf** :

```nginx
# GITLAB

server {
  listen 80;
  listen [::]:80;

  server_name gitlab.freec0ding.dev;
  
  access_log /var/log/nginx/gitlab_access.log;
  error_log /var/log/nginx/gitlab_error.log;

  server_tokens off;

  return 301 https://$http_host$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name gitlab.freec0ding.dev;

  ssl_certificate /etc/letsencrypt/live/freec0ding.dev/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/freec0ding.dev/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/freec0ding.dev/chain.pem;

  access_log /var/log/nginx/gitlab_access.log;
  error_log /var/log/nginx/gitlab_error.log;

  server_tokens off;

  client_max_body_size 250m; # https://docs.gitlab.com/omnibus/settings/nginx.html

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    proxy_pass https://gitlab:18080;
  }
}
```

Le fichier de config nginx pour **/srv/web/nginx/conf/conf.d/gitlab-pages.conf** :

```nginx
# GITLAB-PAGES

server {
  listen 80;
  listen [::]:80;

  server_name *.pages.freec0ding.dev;
  
  access_log /var/log/nginx/gitlab_pages_access.log;
  error_log /var/log/nginx/gitlab_pages_error.log;

  server_tokens off;

  return 301 https://$http_host$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name *.pages.freec0ding.dev;

  ssl_certificate /etc/letsencrypt/live/freec0ding.dev/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/freec0ding.dev/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/freec0ding.dev/chain.pem;

  access_log /var/log/nginx/gitlab_pages_access.log;
  error_log /var/log/nginx/gitlab_pages_error.log;

  server_tokens off;

  client_max_body_size 250m; # https://docs.gitlab.com/omnibus/settings/nginx.html

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    proxy_pass http://gitlab:28080;
  }
}
```

Je **restart nginx** :

```sh {linenos=false}
sudo docker exec nginx-container service nginx restart
```

## gitlab-runner

Après avoir créé le project **Pages/Hugo** depuis le template fournit par gitlab,
J'enregistre un
{{< newtabref href="https://docs.gitlab.com/runner/" title="runner" >}} depuis le menu "Settings" -> "CI/CD" -> "New project runner" :

{{< image-popup src="/gitlab-runner.png" style="max-height=200px" class="img-fluid" title="gitlab-runner" >}}

Après avoir obtenu le "token" :

```sh {linenos=false}
export RUNNER_TOKEN='XXXXXXXXXXXXXXXXXXXXXXXXX'
```

```sh
sudo docker exec -it gitlab-runner-container gitlab-runner register \
--non-interactive \
--url https://gitlab.freec0ding.dev \
--token ${RUNNER_TOKEN} \
--executor "docker" \
--docker-image alpine:latest \
--description "runner for sysc4ll's page"
```

## custom domain

Je prépare le fichier d'identification OVH **/root/.certbot/ovh-sysc4ll.ini** :

```sh
export OVH_APPLICATION_KEY='XXXXXXXXXXXXXXXX'
export OVH_APPLICATION_SECRET='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
export OVH_CONSUMER_KEY='XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
```

```sh
sudo sh -c "cat > /root/.certbot/ovh-sysc4ll.ini << EOF
# OVH API credentials used by Certbot
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = ${OVH_APPLICATION_KEY}
dns_ovh_application_secret = ${OVH_APPLICATION_SECRET}
dns_ovh_consumer_key = ${OVH_CONSUMER_KEY}
EOF"
```

Je **définis les droits** du fichier **/root/.certbot/ovh-sysc4ll.ini** :

```sh {linenos=false}
sudo chmod 600 /root/.certbot/ovh-sysc4ll.ini
```

Je **génère les certificats SSL** :

```sh
sudo certbot certonly \
--dns-ovh \
--dns-ovh-credentials /root/.certbot/ovh-sysc4ll.ini \
-d sysc4ll.sh \
-d *.sysc4ll.sh \
--non-interactive \
--agree-tos \
--email contact@sysc4ll.sh
```

Le fichier de config nginx **/srv/web/nginx/conf/conf.d/sysc4ll-website.conf** :

```nginx
# SYSC4LL'S WEBSITE

server {
  listen 80;
  listen [::]:80;

  server_name www.sysc4ll.sh;

  access_log /var/log/nginx/sysc4ll_page_access.log;
  error_log /var/log/nginx/sysc4ll_page_error.log;

  server_tokens off;

  return 301 https://$http_host$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;

  server_name www.sysc4ll.sh;

  ssl_certificate /etc/letsencrypt/live/sysc4ll.sh/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/sysc4ll.sh/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/sysc4ll.sh/chain.pem;

  access_log /var/log/nginx/sysc4ll_page_access.log;
  error_log /var/log/nginx/sysc4ll_page_error.log;

  server_tokens off;
  
  client_max_body_size 250m; # https://docs.gitlab.com/omnibus/settings/nginx.html

  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;

    proxy_pass http://gitlab:28080;
  }
}
```

Je **restart nginx** :

```sh {linenos=false}
sudo docker exec nginx-container service nginx restart
```

{{< image-popup src="/gitlab-pages-hugo.png" style="max-height=200px" class="img-fluid" title="gitlab-pages-hugo" >}}
