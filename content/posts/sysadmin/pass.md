---
title: "pass: the standard unix password manager"
slug: "passwordstore"
description: "Utilisation de pass, the standard unix password manager"
tags: ["linux"]
categories: ["sysadmin"]
layout: "post-with-toc"
---

# pass : the standard unix password manager


{{< newtabref href="https://www.passwordstore.org/" title="pass" >}} utilise {{< newtabref href="https://gnupg.org/" title="gpg" >}} avec
{{< newtabref href="https://en.wikipedia.org/wiki/GNU_Privacy_Guard" title="GNU Privacy Guard" >}} pour stocker de façon chiffré les passwords.

## Générer les clés gpg

Je génère les clés gpg avec la commande suivante:

```sh {linenos=false}
gpg --full-generate-key
```

Exemple:

```text
sysc4ll@l4ptop ~ $ gpg --full-generate-key
gpg (GnuPG) 2.2.20; Copyright (C) 2020 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
  (14) Existing key from card
Your selection? 
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (3072) 
Requested keysize is 3072 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name: passwordstore
Email address: hello@sysc4ll.sh
Comment: 
You selected this USER-ID:
    "passwordstore <hello@sysc4ll.sh>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key 979C110E825FEFDE marked as ultimately trusted
gpg: revocation certificate stored as '/home/sysc4ll/.gnupg/openpgp-revocs.d/6ED80C6091546ABB9BD7A1A8979C110E825FEFDE.rev'
public and secret key created and signed.

pub   rsa3072 2021-12-24 [SC]
      6ED80C6091546ABB9BD7A1A8979C110E825FEFDE
uid                      passwordstore <hello@sysc4ll.sh>
sub   rsa3072 2021-12-24 [E]

sysc4ll@l4ptop ~ $
```

## Sauvegarder la clé privée

Je vais maintenant faire une backup de la "private key" sur un conteneur chiffré avec
{{< newtabref href="https://veracrypt.fr/en/Home.html" title="veracrypt" >}}

Durant la création du conteneur une string de **320** chars sera demandée, je la génère de cette façon:

```sh {linenos=false}
cat /dev/urandom | tr -cd "[:print:]" | head -c 320; echo
```

Je crée le conteneur:

```sh {linenos=false}
veracrypt --text --create
```

Exemple:

```text
sysc4ll@l4ptop ~ $ veracrypt --text --create
Volume type:
 1) Normal
 2) Hidden
Select [1]: 

Enter volume path: /media/sysc4ll/data/vc-container.hc

Enter volume size (sizeK/size[M]/sizeG.sizeT/max): 4G

Encryption Algorithm:
 1) AES
 2) Serpent
 3) Twofish
 4) Camellia
 5) Kuznyechik
 6) AES(Twofish)
 7) AES(Twofish(Serpent))
 8) Camellia(Kuznyechik)
 9) Camellia(Serpent)
 10) Kuznyechik(AES)
 11) Kuznyechik(Serpent(Camellia))
 12) Kuznyechik(Twofish)
 13) Serpent(AES)
 14) Serpent(Twofish(AES))
 15) Twofish(Serpent)
Select [1]: 

Hash algorithm:
 1) SHA-512
 2) Whirlpool
 3) SHA-256
 4) Streebog
Select [1]: 

Filesystem:
 1) None
 2) FAT
 3) Linux Ext2
 4) Linux Ext3
 5) Linux Ext4
 6) NTFS
 7) exFAT
 8) Btrfs
Select [2]: 

Enter password: 
Re-enter password: 

Enter PIM: 

Enter keyfile path [none]: 

Please type at least 320 randomly chosen characters and then press Enter:


Done: 100,000%  Speed: 117 MiB/s  Left: 0 s               

The VeraCrypt volume has been successfully created.
sysc4ll@l4ptop ~ $ 
```

Je mount le volume veracrypt:

```sh
mkdir /mnt/vault && \
sudo veracrypt --text --mount /media/sysc4ll/data/vc-container.hc /mnt/vault
```

Exemple:

```text
sysc4ll@l4ptop ~ $ mkdir /mnt/vault && \
sudo veracrypt --text --mount /media/sysc4ll/data/vc-container.hc /mnt/vault
[sudo] password for sysc4ll: 
Enter password for /media/sysc4ll/data/vc-container.hc: 
Enter PIM for /media/sysc4ll/data/vc-container.hc: 
Enter keyfile [none]: 
Protect hidden volume (if any)? (y=Yes/n=No) [No]: 
sysc4ll@l4ptop ~ $
```

J'exporte la clé gpg (remplacer **979C110E825FEFDE** par l'id de votre clé gpg):

```sh {linenos=false}
gpg --export-secret-keys --armor 979C110E825FEFDE > ~/hello@sysc4ll.sh-passwordstore-privkey.asc
```

Je copie la clé dans le conteneur chiffré:

```sh
sudo cp ~/hello@sysc4ll.sh-passwordstore-privkey.asc /mnt/vault/ && \
rm ~/hello@sysc4ll.sh-passwordstore-privkey.asc
```

J'unmount:

```sh {linenos=false}
sudo veracrypt --text --dismount /mnt/vault
```

Si l'header d'un conteneur chiffré est détruit ou corrompu, le conteneur sera inutilisable.
Je vais donc sauvegarder l'header du conteneur:

```sh {linenos=false}
veracrypt --text --backup-headers /media/sysc4ll/data/vc-container.hc
```

Exemple:

```text
sysc4ll@l4ptop ~ $ veracrypt --text --backup-headers /media/sysc4ll/data/vc-container.hc
For security reasons, you will have to enter the correct password (and/or supply the correct keyfiles) for the volume.

Note: If the volume contains a hidden volume, you will have to enter the correct password (and/or supply the correct keyfiles) for the outer volume first. Afterwards, if you choose to back up the header of the hidden volume, you will have to enter the correct password (and/or supply the correct keyfiles) for the hidden volume.

Enter password for the normal/outer volume: 
Enter PIM for the normal/outer volume: 
Enter keyfile [none]: 

Does the volume contain a hidden volume? (y=Yes/n=No) [No]: 

Are you sure you want to create volume header backup for /media/sysc4ll/data/vc-container.hc?

After you click Yes, you will prompted for a filename for the header backup.

Note: Both the standard and the hidden volume headers will be re-encrypted using a new salt and stored in the backup file. If there is no hidden volume within this volume, the area reserved for the hidden volume header in the backup file will be filled with random data (to preserve plausible deniability). When restoring a volume header from the backup file, you will need to enter the correct password (and/or to supply the correct keyfiles) that was/were valid when the volume header backup was created. The password (and/or keyfiles) will also automatically determine the type of the volume header to restore, i.e. standard or hidden (note that VeraCrypt determines the type through the process of trial and error).
 (y=Yes/n=No) [Yes]: 

Enter filename: /media/sysc4ll/data/backups/vc-container-headers-backup.txt

Please type at least 320 randomly chosen characters and then press Enter:


Volume header backup has been successfully created.

IMPORTANT: Restoring the volume header using this backup will also restore the current volume password. Moreover, if keyfile(s) are/is necessary to mount the volume, the same keyfile(s) will be necessary to mount the volume again when the volume header is restored.

WARNING: This volume header backup may be used to restore the header ONLY of this particular volume. If you use this header backup to restore a header of a different volume, you will be able to mount the volume, but you will NOT be able to decrypt any data stored in the volume (because you will change its master key).
sysc4ll@l4ptop ~ $ 
```

Pour restaurer l'header de la partition:

```sh {linenos=false}
veracrypt --text --restore-headers /media/sysc4ll/data/vc-container.hc
```

Exemple:

```text
sysc4ll@l4ptop ~ $ veracrypt --text --restore-headers /media/sysc4ll/data/vc-container.hc
Please select the type of volume header backup you want to use:

1) Restore the volume header from the backup embedded in the volume
2) Restore the volume header from an external backup file

Select: 2


Are you sure you want to restore volume header of /media/sysc4ll/data/vc-container.hc?

WARNING: Restoring a volume header also restores the volume password that was valid when the backup was created. Moreover, if keyfile(s) were/was necessary to mount the volume when the backup was created, the same keyfile(s) will be necessary to mount the volume again after the volume header is restored.

After you click Yes, you will select the header backup file. (y=Yes/n=No) [Yes]: 

Enter filename: /media/sysc4ll/data/backups/vc-container-headers-backup.txt

Enter password for the header stored in backup file: 
Enter PIM: 
Enter keyfile [none]: 

Please type at least 320 randomly chosen characters and then press Enter:


The volume header has been successfully restored.

IMPORTANT: Please note that an old password may have been restored as well. Moreover, if keyfile(s) were/was necessary to mount the volume when the backup was created, the same keyfile(s) are now necessary to mount the volume again.
sysc4ll@l4ptop ~ $ 
```

## Utiliser pass

Je souhaite stocker les passwords sur ma partition de données **/media/sysc4ll/data**.
J'utilise le shell **zsh**. J'ajoute donc la variable d'environnement **PASSWORD_STORE_DIR** au fichier **.zshrc**.

```sh
export PASSWORD_STORE_DIR=/media/sysc4ll/data/.password-store && \
echo "" >> ~/.zshrc && \
echo "export PASSWORD_STORE_DIR=/media/sysc4ll/data/.password-store" >> ~/.zshrc
```

```sh {linenos=false}
pass init 979C110E825FEFDE
```

{{< newtabref href="https://git.zx2c4.com/password-store/about/" title="https://git.zx2c4.com/password-store/about/" >}}

## Restaurer la clé privée gpg

Je mount le volume veracrypt:

```sh {linenos=false}
sudo veracrypt --text --mount /media/sysc4ll/data/vc-container.hc /mnt/vault
```

J'importe la clé privée:

```sh {linenos=false}
gpg --import /mnt/vault/hello@sysc4ll.sh-passwordstore-privkey.asc
```

J'unmount:

```sh {linenos=false}
sudo veracrypt --text --dismount /mnt/vault
```

Je précise que je fais confiance à cette "key" (remplacer **979C110E825FEFDE** par l'id de votre clé gpg):

```text
gpg --edit-key 979C110E825FEFDE
gpg> trust
  1 = I don't know or won't say
  2 = I do NOT trust
  3 = I trust marginally
  4 = I trust fully
  5 = I trust ultimately
  m = back to the main menu
Your decision? 5
Do you really want to set this key to ultimate trust? (y/N) y
gpg> quit
```

