$(function() {
    $(window).on('resize', function() {
        var navbarHeight = $('#navbar').outerHeight(true);
        var height = $('body').height() - (navbarHeight + 30);
        $('#terminal').height(height);
    }).resize();

    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        image: { verticalFit: true }
    });
});
