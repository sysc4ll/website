// https://hugomodo.github.io/extensions/fusejs/
// https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae

var summaryInclude = 60;

var fuseOptions = {
    shouldSort: true,
    includeMatches: true,
    threshold: 0.2,
    tokenize: true,
    matchAllTokens: true,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
        { name: 'title',        weight: 0.8 },
        { name: 'description',  weight: 0.8 },
        { name: 'tags',         weight: 0.4 },
        { name: 'categories',   weight: 0.4 }
    ]
};

var searchQuery = param('s');

if (searchQuery) {
    jQuery('#search-query').val(searchQuery);

    executeSearch(searchQuery);
}

//else jQuery('#search-results').html('<p>please enter a word or phrase above and press enter</p>');

function executeSearch(searchQuery) {
    jQuery.ajax({
        cache: false,
        dataType: 'json',
        url: '/index.json',
        success: function(data) {
            var pages = data;
            var fuse = new Fuse(pages, fuseOptions);
            var result = fuse.search(searchQuery);

            jQuery('#search-results').html('');

            if (result.length > 0) populateResults(result);

            else jQuery('#search-results').html('<p>no matches found</p>');
        }
    });
}

function populateResults(result) {
    jQuery('#search-results').html('');

    jQuery.each(result, function(key, value) {
        var description = value.item.description;
        var snippet = '';
        var snippetHighlights = [];
        var tags = [];

        if (fuseOptions.tokenize) snippetHighlights.push(searchQuery);

        else {
            jQuery.each(value.matches, function(matchKey, mvalue) {
                if (mvalue.key == 'tags' || mvalue.key == 'categories') snippetHighlights.push(mvalue.value); 

                else if (mvalue.key == 'description') {
                    start = mvalue.indices[0][0] - summaryInclude > 0 ? mvalue.indices[0][0] - summaryInclude : 0;
                    end = mvalue.indices[0][1] + summaryInclude < description.length ? mvalue.indices[0][1] + summaryInclude : description.length;
                    snippet += description.substring(start, end);
                    snippetHighlights.push(mvalue.value.substring(mvalue.indices[0][0], mvalue.indices[0][1] - mvalue.indices[0][0] + 1));
                }
            });
        }

        if (snippet.length < 1) snippet += description.substring(0, summaryInclude * 2);

        // pull template from hugo template definition
        var templateDefinition = jQuery('#search-result-template').html();

        // replace values
        var output = render(templateDefinition, { key: key, title: value.item.title, link: value.item.permalink,
            tags: value.item.tags, categories: value.item.categories, description: value.item.description, snippet: snippet });

        jQuery('#search-results').append(output);

        jQuery.each(snippetHighlights, function(snipkey, snipvalue) {
            jQuery('#summary-' + key).mark(snipvalue);
        });

    });
}

function param(name) {
    return decodeURIComponent((location.search.split(name + '=')[1] || '').split('&')[0]).replace(/\+/g, ' ');
}

function render(templateString, data) {
    var conditionalMatches, conditionalPattern, copy;

    conditionalPattern = /\$\{\s*isset ([a-zA-Z]*) \s*\}(.*)\$\{\s*end\s*}/g;

    // since loop below depends on re.lastIndex, we use a copy to capture any manipulations whilst inside the loop
    copy = templateString;

    while ((conditionalMatches = conditionalPattern.exec(templateString)) !== null) {
        if (data[conditionalMatches[1]]) {
            // valid key, remove conditionals, leave contents.
            copy = copy.replace(conditionalMatches[0], conditionalMatches[2]);
        }

        else {
            // not valid, remove entire section
            copy = copy.replace(conditionalMatches[0], '');
        }
    }

    templateString = copy;

    // now any conditionals removed we can do simple substitution
    var key, find, re;

    for (key in data) {
        find = '\\$\\{\\s*' + key + '\\s*\\}';
        re = new RegExp(find, 'g');
        templateString = templateString.replace(re, data[key]);
    }

    return templateString;
}

