function TerminalCommands(parser) {
    var term;

    this.set_term = function(terminal) { term = terminal; }

    this.help = function() {
        term.echo('sysc4ll\'s terminal built with https://terminal.jcubic.pl');
        term.echo('arguments between [] are optionals.\n');
        term.echo('[[;yellow;]help]         - display this help message');
        term.echo('[[;yellow;]ls] [<dir>]   - list directory contents');
        term.echo('[[;yellow;]cd] [<dir>]   - change current directory');
        term.echo('[[;yellow;]desc] <file>  - display file description');
        term.echo('[[;yellow;]open] <file>  - open file in browser');
        term.echo('[[;yellow;]search] <str> - search string and open search page');
        term.echo('[[;yellow;]about]        - open about page');
        term.echo('[[;yellow;]greetings]    - display the greetings message');
        term.echo('[[;yellow;]clear]        - clear the terminal screen');
    }

    this.ls = function(...args) {
        var items = [];

        if (args.length > 1) return this.help(); // trop d'arguments

        if ((path == '') && (args.length != 1))
            items = items.concat(parser.list_main_items(true));

        if ((path == '') && (args.length == 1))
            items = items.concat(parser.list_items_in_item(args[0].replace('/', ''), true));

        if (path != '') items = items.concat(parser.list_items_in_item(path, true));

        if (items.length != 0) term.echo(items, {raw: true});

        else term.error('Directory "' + args[0] + '" Not Found!');
    }

    this.cd = function(...args) {
        if (args.length > 1) return this.help(); // trop d'arguments

        if ((args.length == 0) || (args[0] == '..')) {
            path = '';
            term.set_prompt('[[b;green;]' + '~' + '][[b;red;]$] ');
            return;
        }

        if (args[0].charAt(args[0].length - 1) != '/') args[0] = args[0].concat('/');

        if ($.inArray(args[0], parser.list_main_items()) != -1) {
            path = args[0].replace('/', '');
            term.set_prompt('[[b;green;]' + '~' + '/' + path + '][[b;red;]$] ');
        }

        else term.error('Directory "' + args[0] + '" Not Found!');
    }

    this.desc = function(...args) {
        if (args.length != 1) return this.help(); // manque l'argument

        var description = parser.get_item_description(args[0]);

        if (description != '') term.echo(description, {raw: true});

        else term.error('File "' + args[0] + '" Not Found!');
    }

    this.open = function(...args) {
        if (args.length != 1) return this.help(); // manque l'argument

        var permalink = parser.get_item_permalink(args[0]);

        if (permalink != '') window.location = permalink;

        else term.error('File "' + args[0] + '" Not Found!');
    }

    this.greetings = function(...args) {
        if (args.length > 0) return this.help(); // il ne faut pas d'argument

        term.echo(greets);
    }

    this.search = function(...args) {
        if (args.length < 1) return this.help(); // pas d'arguments

        window.location = '/search/?s=' + args.join(' ');

    }

    this.about = function(...args) {
        if (args.length > 0) return this.help(); // il ne faut pas d'argument

        window.location = '/about/';
    }
};

