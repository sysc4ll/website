function TerminalCompletions(parser) {

    this.complete = function(cmd) {
        cmd = cmd.replace(/ +(?= )/g, ''); // supprime les espaces superflux

        if (cmd.match(/^help /)) return [];

        if (cmd.match(/^ls /)) return complete_ls(cmd);

        if (cmd.match(/^cd /)) return complete_cd(cmd);
        
        if (cmd.match(/^desc /)) return complete_desc(cmd);
        
        if (cmd.match(/^open /)) return complete_open(cmd);

        if (cmd.match(/^search /)) return [];

        if (cmd.match(/^about /)) return [];
        
        if (cmd.match(/^greetings /)) return [];
        
        if (cmd.match(/^clear /)) return [];
        
        if (cmd.split(' ').length > 1) return [];

        return ['help', 'ls', 'cd', 'desc', 'open', 'search', 'about', 'greetings', 'clear'];
    };

    complete_ls = function(cmd) {
        var items = [];

        if (cmd.split(' ').length > 2) return []; // il y a au maximum la commande + l'argument

        if (path == '') items = items.concat(parser.list_main_items());

        return items;
    }

    complete_cd = function(cmd) {
        var items = [];

        if (cmd.split(' ').length > 2) return []; // il y a au maximum la commande + l'argument

        if (path == '') items = items.concat(parser.list_main_items());

        return items;
    }

    complete_desc = function(cmd) {
        var items = [];

        if (cmd.split(' ').length > 2) return []; // il y a au maximum la commande + l'argument

        if (path == '') {
            if (cmd.indexOf('/') == -1) items = items.concat(parser.list_main_items());

            else {
                var item = cmd.substr(cmd.indexOf(" "));
                item = item.replace(' ', '');

                var item = item.substr(0, item.indexOf("/"));
                item = item.replace('/', '');

                jQuery.grep(parser.paths(), function(n, i) {
                    if (n.indexOf(item) != -1) items.push(n);
                });
            }
        }

        else items = items.concat(parser.list_items_in_item(path));

        return items;
    }

    complete_open = function(cmd) {
        var items = [];

        if (cmd.split(' ').length > 2) return []; // il y a au maximum la commande + l'argument

        if (path == '') {
            if (cmd.indexOf('/') == -1) items = items.concat(parser.list_main_items());

            else {
                var item = cmd.substr(cmd.indexOf(" "));
                item = item.replace(' ', '');

                var item = item.substr(0, item.indexOf("/"));
                item = item.replace('/', '');

                jQuery.grep(parser.paths(), function(n, i) {
                    if (n.indexOf(item) != -1) items.push(n);
                });
            }
        }

        else items = items.concat(parser.list_items_in_item(path));

        return items;
    }
}

