function TerminalParser() {
    var items = [];
    var categories = {};
    var tags = {};
    var paths = [];

    /* parsing du fichier /index.json */
    this.parse = function(data) {
        $.each(data, function(index, item) {
            if (items.includes(item) == false) items.push(item);

            if (item.categories != null) {
                $.each(item.categories, function(index, category) {
                    if (!categories.hasOwnProperty(category)) categories[category] = [];

                    if (!categories[category].includes(item)) categories[category].push(item);

                    if ($.inArray(category + '/' + item.name, paths))
                        paths.push(category + '/' + item.name);
                });
            }

            if (item.tags != null) {
                $.each(item.tags, function(index, tag) {
                    if (!tags.hasOwnProperty(tag)) tags[tag] = [];

                    if (!tags[tag].includes(item)) tags[tag].push(item);

                    if ($.inArray(tag + '/' + item.name, paths))
                        paths.push(tag + '/' + item.name);
                });
            }
        });
    }

    this.list_main_items = function(link = false) {
        var items = [];

        $.each(Object.entries(categories), function(index, category) {
            if (link == false) items.push(category[0] + '/');
            
            else items.push('<a href="/categories/' + category[0] +
                '" class="category-link">' + category[0] + '</a>');
        });

        $.each(Object.entries(tags), function(index, tag) {
            if (link == false) items.push(tag[0] + '/');
            
            else items.push('<a href="/tags/' + tag[0] +
                '" class="tag-link">' + tag[0] + '</a>');
        });

        return items;
    }

    this.list_items_in_item = function(argument, link = false) {
        var items = [];
        var found = [];

        if (categories.hasOwnProperty(argument)) {
            $.each(categories[argument], function(index, item) {
                if ($.inArray(item, found) == -1) {
                    if (link == false) items.push(item.name);

                    else items.push('<a href="' + item.permalink +
                        '" class="post-link">' + item.name + '</a>');
                    
                    found.push(item);
                }
            });
        }

        if (tags.hasOwnProperty(argument)) {
            $.each(tags[argument], function(index, item) {
                if ($.inArray(item, found) == -1) {
                    if (link == false) items.push(item.name);
                    
                    else items.push('<a href="' + item.permalink +
                        '" class="post-link">' + item.name + '</a>');
                    
                    found.push(item);
                }
            });
        }

        return items;
    }

    this.paths = function() { return paths; }

    this.get_item_description = function(argument) {
        var description = '';
        var name = argument.split('/').pop();

        $.each(items, function(index, item) {
            if (item.name == name) description = item.description;
        });

        return description;
    }

    this.get_item_permalink = function(argument) {
        var permalink = '';
        var name = argument.split('/').pop();

        $.each(items, function(index, item) {
            if (item.name == name) permalink = item.permalink;
        });

        return permalink;
    }
}

