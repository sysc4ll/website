$(function() {
    // https://terminal.jcubic.pl/api_reference.php

    $.ajax({
        cache: false,
        dataType: 'json',
        url: '/index.json',
        success: on_success,
    });

    greets = 'Welcome :)\n' +
        'Entrez la commande [[;yellow;]help] ' +
        'pour obtenir la liste des commandes disponibles.\n' +
        'Utilisez la touche [[;DeepPink;]tab] pour la complétion.\n' +
        'Enjoy !\n';

    path = '';

    function on_success(data) {
        var parser = new TerminalParser();
        var completions = new TerminalCompletions(parser);
        var commands = new TerminalCommands(parser);

        parser.parse(data);

        var term = $('#terminal').terminal({
            help: function(...args) { commands.help(...args); },

            ls: function(...args) { commands.ls(...args); },

            cd: function(...args) { commands.cd(...args); },

            desc: function(...args) { commands.desc(...args); },

            open: function(...args) { commands.open(...args); },

            search: function(...args) { commands.search(...args); },

            about: function(...args) { commands.about(...args); },

            greetings: function(...args) { commands.greetings(...args); },
        },{
            prompt: '[[b;green;]~][[b;red;]$] ',

            greetings: greets,

            checkArity: false,

            onInit: function(term) { commands.set_term(term); },

            onblur: function() { return false; },

            completion: function(string, callback) {
                callback(completions.complete(term.get_command()));
            },
        });
    };
});

